<?php
/**
 * Nextcloud - cospend
 *
 * This file is licensed under the Affero General Public License version 3 or
 * later. See the COPYING file.
 *
 * @author Julien Veyssier <eneiluj@posteo.net>
 * @copyright Julien Veyssier 2018
 */

return [
    'routes' => [
        ['name' => 'multiple#index', 'url' => '/', 'verb' => 'GET'],

        // api for client using guest access (password)
        [
            'name'         => 'multiple#preflighted_cors',
            'url'          => '/api/{path}',
            'verb'         => 'OPTIONS',
            'requirements' => ['path' => '.+']
        ],
        [
            'name'         => 'multiple#preflighted_cors',
            'url'          => '/apiv2/{path}',
            'verb'         => 'OPTIONS',
            'requirements' => ['path' => '.+']
        ],
		// api for logged in clients
		[
			'name'         => 'multiple#preflighted_cors',
			'url'          => '/api-priv/{path}',
			'verb'         => 'OPTIONS',
			'requirements' => ['path' => '.+']
		],


		// Project Web
		['name' => 'multiple#webGetProjects', 'url' => 'getProjects', 'verb' => 'POST'], // list
		['name' => 'multiple#webCreateProject', 'url' => 'createProject', 'verb' => 'POST'], // add
		['name' => 'multiple#webGetProjectInfo', 'url' => 'getProjectInfo', 'verb' => 'POST'], // get
		['name' => 'multiple#webEditProject', 'url' => 'editProject', 'verb' => 'POST'], // edit
		['name' => 'multiple#webDeleteProject', 'url' => 'deleteProject', 'verb' => 'POST'], // delete

		['name' => 'multiple#webAddExternalProject', 'url' => 'addExternalProject', 'verb' => 'POST'], // add external
		['name' => 'multiple#webEditExternalProject', 'url' => 'editExternalProject', 'verb' => 'POST'], // edit external
		['name' => 'multiple#webDeleteExternalProject', 'url' => 'deleteExternalProject', 'verb' => 'POST'], // delete external

		// Project API
		['name' => 'multiple#apiCreateProject', 'url' => '/api/projects', 'verb' => 'POST'], // add
		['name' => 'multiple#apiGetProjectInfo', 'url' => '/api/projects/{projectid}/{password}', 'verb' => 'GET'], // get
		['name' => 'multiple#apiSetProjectInfo', 'url' => '/api/projects/{projectid}/{passwd}', 'verb' => 'PUT'], // edit
		['name' => 'multiple#apiDeleteProject', 'url' => '/api/projects/{projectid}/{password}', 'verb' => 'DELETE'], // delete

		// Project Private
		['name' => 'multiple#apiPrivCreateProject', 'url' => '/api-priv/projects', 'verb' => 'POST'], // add
		['name' => 'multiple#apiPrivGetProjectInfo', 'url' => '/api-priv/projects/{projectid}', 'verb' => 'GET'], // get
		['name' => 'multiple#apiPrivSetProjectInfo', 'url' => '/api-priv/projects/{projectid}', 'verb' => 'PUT'], // edit
		['name' => 'multiple#apiPrivDeleteProject', 'url' => '/api-priv/projects/{projectid}', 'verb' => 'DELETE'], // delete


		// Bill Web
		['name' => 'multiple#webAddBill', 'url' => 'addBill', 'verb' => 'POST'], // add
		['name' => 'multiple#webGetBills', 'url' => 'getBills', 'verb' => 'POST'], // get
		['name' => 'multiple#webEditBill', 'url' => 'editBill', 'verb' => 'POST'], // edit
		['name' => 'multiple#webDeleteBill', 'url' => 'deleteBill', 'verb' => 'POST'], // delete

		// Bill API
		['name' => 'multiple#apiv2GetBills', 'url' => '/apiv2/projects/{projectid}/{password}/bills', 'verb' => 'GET'], // list
		['name' => 'multiple#apiAddBill', 'url' => '/api/projects/{projectid}/{password}/bills', 'verb' => 'POST'], // add
		['name' => 'multiple#apiGetBills', 'url' => '/api/projects/{projectid}/{password}/bills', 'verb' => 'GET'], // get
		['name' => 'multiple#apiEditBill', 'url' => '/api/projects/{projectid}/{password}/bills/{billid}', 'verb' => 'PUT'], // edit
		['name' => 'multiple#apiDeleteBill', 'url' => '/api/projects/{projectid}/{password}/bills/{billid}', 'verb' => 'DELETE'], // delete

		// Bill Private
		['name' => 'multiple#apiPrivGetBills', 'url' => '/api-priv/projects/{projectid}/bills', 'verb' => 'GET'], // list
		['name' => 'multiple#apiPrivAddBill', 'url' => '/api-priv/projects/{projectid}/bills', 'verb' => 'POST'], // add
		['name' => 'multiple#apiPrivEditBill', 'url' => '/api-priv/projects/{projectid}/bills/{billid}', 'verb' => 'PUT'], // edit
		['name' => 'multiple#apiPrivDeleteBill', 'url' => '/api-priv/projects/{projectid}/bills/{billid}', 'verb' => 'DELETE'], // delete


		// Member Web
		['name' => 'multiple#getUserList', 'url' => '/getUserList', 'verb' => 'POST'], // list
		['name' => 'multiple#webAddMember', 'url' => 'addMember', 'verb' => 'POST'], // add
		['name' => 'multiple#webEditMember', 'url' => 'editMember', 'verb' => 'POST'], // edit

		// Member API
		['name' => 'multiple#apiGetMembers', 'url' => '/api/projects/{projectid}/{password}/members', 'verb' => 'GET'], // list
		['name' => 'multiple#apiAddMember', 'url' => '/api/projects/{projectid}/{password}/members', 'verb' => 'POST'], // add
		['name' => 'multiple#apiEditMember', 'url' => '/api/projects/{projectid}/{password}/members/{memberid}', 'verb' => 'PUT'], // edit
		['name' => 'multiple#apiDeleteMember', 'url' => '/api/projects/{projectid}/{password}/members/{memberid}', 'verb' => 'DELETE'], // delete

		// Member Private
		['name' => 'multiple#apiPrivGetMembers', 'url' => '/api-priv/projects/{projectid}/members', 'verb' => 'GET'], // list
		['name' => 'multiple#apiPrivAddMember', 'url' => '/api-priv/projects/{projectid}/members', 'verb' => 'POST'], // add
		['name' => 'multiple#apiPrivEditMember', 'url' => '/api-priv/projects/{projectid}/members/{memberid}', 'verb' => 'PUT'], // edit
		['name' => 'multiple#apiPrivDeleteMember', 'url' => '/api-priv/projects/{projectid}/members/{memberid}', 'verb' => 'DELETE'], // delete


		// Currency Web
		['name' => 'multiple#addCurrency', 'url' => '/addCurrency', 'verb' => 'POST'], // add
		['name' => 'multiple#editCurrency', 'url' => '/editCurrency', 'verb' => 'POST'], // edit
		['name' => 'multiple#deleteCurrency', 'url' => '/deleteCurrency', 'verb' => 'POST'], // delete

		// Currency API
		['name' => 'multiple#apiAddCurrency', 'url' => '/api/projects/{projectid}/{password}/currency', 'verb' => 'POST'], // add
		['name' => 'multiple#apiEditCurrency', 'url' => '/api/projects/{projectid}/{password}/currency/{currencyid}', 'verb' => 'PUT'], // edit
		['name' => 'multiple#apiDeleteCurrency', 'url' => '/api/projects/{projectid}/{password}/currency/{currencyid}', 'verb' => 'DELETE'], // delete

		// Currency Private
		['name' => 'multiple#apiPrivAddCurrency', 'url' => '/api-priv/projects/{projectid}/currency', 'verb' => 'POST'], // add
		['name' => 'multiple#apiPrivEditCurrency', 'url' => '/api-priv/projects/{projectid}/currency/{currencyid}', 'verb' => 'PUT'], // edit
		['name' => 'multiple#apiPrivDeleteCurrency', 'url' => '/api-priv/projects/{projectid}/currency/{currencyid}', 'verb' => 'DELETE'], // delete


		// Category Web
		['name' => 'multiple#addCategory', 'url' => '/addCategory', 'verb' => 'POST'], // add
		['name' => 'multiple#editCategory', 'url' => '/editCategory', 'verb' => 'POST'], // edit
		['name' => 'multiple#deleteCategory', 'url' => '/deleteCategory', 'verb' => 'POST'], // delete

		// Category API
		['name' => 'multiple#apiAddCategory', 'url' => '/api/projects/{projectid}/{password}/category', 'verb' => 'POST'], // add
		['name' => 'multiple#apiEditCategory', 'url' => '/api/projects/{projectid}/{password}/category/{categoryid}', 'verb' => 'PUT'], // edit
		['name' => 'multiple#apiDeleteCategory', 'url' => '/api/projects/{projectid}/{password}/category/{categoryid}', 'verb' => 'DELETE'], // delete

		// Category Private
		['name' => 'multiple#apiPrivAddCategory', 'url' => '/api-priv/projects/{projectid}/category', 'verb' => 'POST'], // add
		['name' => 'multiple#apiPrivEditCategory', 'url' => '/api-priv/projects/{projectid}/category/{categoryid}', 'verb' => 'PUT'], // edit
		['name' => 'multiple#apiPrivDeleteCategory', 'url' => '/api-priv/projects/{projectid}/category/{categoryid}', 'verb' => 'DELETE'], // delete


		// Statistics
		['name' => 'multiple#webGetProjectStatistics', 'url' => 'getStatistics', 'verb' => 'POST'],
		['name' => 'multiple#apiGetProjectStatistics', 'url' => '/api/projects/{projectid}/{password}/statistics', 'verb' => 'GET'],
		['name' => 'multiple#apiPrivGetProjectStatistics', 'url' => '/api-priv/projects/{projectid}/statistics', 'verb' => 'GET'],


		// Settlement Web
		['name' => 'multiple#webGetProjectSettlement', 'url' => 'getSettlement', 'verb' => 'POST'],
		['name' => 'multiple#webAutoSettlement', 'url' => 'autoSettlement', 'verb' => 'POST'],

		// Settlement API
		['name' => 'multiple#apiGetProjectSettlement', 'url' => '/api/projects/{projectid}/{password}/settle', 'verb' => 'GET'],
		['name' => 'multiple#apiAutoSettlement', 'url' => '/api/projects/{projectid}/{password}/autosettlement', 'verb' => 'GET'],

		// Settlement Private
		['name' => 'multiple#apiPrivGetProjectSettlement', 'url' => '/api-priv/projects/{projectid}/settle', 'verb' => 'GET'],
		['name' => 'multiple#apiPrivAutoSettlement', 'url' => '/api-priv/projects/{projectid}/autosettlement', 'verb' => 'GET'],


		// Share
		['name' => 'multiple#addUserShare', 'url' => '/addUserShare', 'verb' => 'POST'], // add
		['name' => 'multiple#deleteUserShare', 'url' => '/deleteUserShare', 'verb' => 'POST'], // delete

		['name' => 'multiple#addGroupShare', 'url' => '/addGroupShare', 'verb' => 'POST'], // add
		['name' => 'multiple#deleteGroupShare', 'url' => '/deleteGroupShare', 'verb' => 'POST'], // delete

		['name' => 'multiple#addCircleShare', 'url' => '/addCircleShare', 'verb' => 'POST'], // add
		['name' => 'multiple#deleteCircleShare', 'url' => '/deleteCircleShare', 'verb' => 'POST'], // delete

		['name' => 'multiple#editSharePermissions', 'url' => '/editSharePermissions', 'verb' => 'POST'], // edit share
		['name' => 'multiple#editGuestPermissions', 'url' => '/editGuestPermissions', 'verb' => 'POST'], // edit guest
		['name' => 'multiple#getPublicFileShare', 'url' => '/getPublicFileShare', 'verb' => 'POST'], // edit public


		// Import/Export
		['name' => 'multiple#importCsvProject', 'url' => '/importCsvProject', 'verb' => 'POST'],
		['name' => 'multiple#importSWProject', 'url' => '/importSWProject', 'verb' => 'POST'],
		['name' => 'multiple#exportCsvProject', 'url' => '/exportCsvProject', 'verb' => 'POST'],
		['name' => 'multiple#exportCsvStatistics', 'url' => '/exportCsvStatistics', 'verb' => 'POST'],
		['name' => 'multiple#exportCsvSettlement', 'url' => '/exportCsvSettlement', 'verb' => 'POST'],


		// Utils
		['name' => 'utils#getOptionsValues', 'url' => '/getOptionsValues', 'verb' => 'POST'],
		['name' => 'utils#saveOptionValue', 'url' => '/saveOptionValue', 'verb' => 'POST'],
		['name' => 'utils#setAllowAnonymousCreation', 'url' => '/setAllowAnonymousCreation', 'verb' => 'POST'],
		['name' => 'utils#getAvatar', 'url' => 'getAvatar', 'verb' => 'GET'],


		// Misc
		['name' => 'multiple#pubLoginProjectPassword', 'url' => 'loginproject/{projectid}/{password}', 'verb' => 'GET'],
		['name' => 'multiple#pubLoginProject', 'url' => 'loginproject/{projectid}', 'verb' => 'GET'],
		['name' => 'multiple#pubLogin', 'url' => 'login', 'verb' => 'GET'],

		['name' => 'multiple#pubProject', 'url' => 'project', 'verb' => 'POST'],

		['name' => 'multiple#apiPing', 'url' => '/api/ping', 'verb' => 'GET'],

    ]
];
