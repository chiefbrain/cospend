# Nextcloud Cospend 💰

A Próxima Cospend é um gerenciador de orçamento compartilhado/de grupo. Foi inspirado pelo ótimo [IHateMoney](https://github.com/spiral-project/ihatemoney/).

Você pode usá-lo quando você compartilha uma casa, quando você vai de férias com amigos, ou sempre que você compartilha despesas com outras pessoas.

Ele permite criar projetos com membros e despesas. Cada membro tem um saldo calculado a partir das faturas do projeto. Desta forma você pode ver quem deve ao grupo e a quem o grupo deve. Em última análise pode pedir um plano de quitação que lhe diga quais os pagamentos a fazer para saldar as dívidas dos membros.

Os membros do projeto são independentes dos usuários do Nextcloud. Projetos podem ser acessados e modificados por pessoas sem uma conta do Nextcloud. Cada projeto tem um ID e uma senha para acesso de convidados.

[MoneyBuster](https://gitlab.com/eneiluj/moneybuster) é um cliente Android que está [disponível no F-Droid](https://f-droid.org/packages/net.eneiluj.moneybuster/) e na [Play store](https://play.google.com/store/apps/details?id=net.eneiluj.moneybuster).

## Funcionalidades

* ✎ criar/editar/excluir projetos, membros e despesas
* ⚖ verificar os saldos dos membros
* 🗠 exibir estatísticas do projeto
* ♻ exibir plano de quitação
* 🎇 criar automaticamente reembolsos a partir do plano de quitação
* 🗓 criar despesas recorrentes (diárias/semanais/mensais/anuais)
* 📊 fornecer, opcionalmente, um valor personalizado para cada membro em despesas novas
* 🔗 inserir link público para um arquivo pessoal na descrição da despesa (imagem da fatura física, por exemplo)
* 👩 acesso de convidado para pessoas fora do Nextcloud
* 👫 compartilhar projetos com o os usuários/grupos do Nextcloud
* 🖫 importar/exportar projetos no formato CSV (compatível com aquivos CSV do IHateMoney)
* 🖧 adicionar projetos externos (hospedados por outra instância de Nextcloud)
* 🔗 gerar link/QRCode para importar facilmente projetos no MoneyBuster
* 🗲 implementar notificações e fluxo de atividades do Nextcloud

Este aplicativo está testado no Nextcloud 17 com Firefox 57+ e Chromium.

Este aplicativo está em desenvolvimento.

🌍 Ajude-nos a traduzir esta aplicação no [projeto Crowdin do Nextcloud-Cospend/MoneyBuster](https://crowdin.com/project/moneybuster).

⚒ Veja outras formas de ajudar nos [guias de contribuição](https://gitlab.com/eneiluj/cospend-nc/blob/master/CONTRIBUTING.md).

## Instalar

Consulte o [AdminDoc](https://gitlab.com/eneiluj/cospend-nc/wikis/admindoc) para obter detalhes de instalação.

Consulte o arquivo [CHANGELOG](https://gitlab.com/eneiluj/cospend-nc/blob/master/CHANGELOG.md#change-log) para ver o que há de novo e o que está por vir na próxima versão.

Consulte o arquivo [AUTHORS](https://gitlab.com/eneiluj/cospend-nc/blob/master/AUTHORS.md#authors) para ver a lista completa dos autores.

## Problemas conhecidos

* ele não te torna rico

Qualquer comentário será apreciado.