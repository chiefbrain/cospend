# Authors

* Julien Veyssier <eneiluj@posteo.net> @eneiluj (Developer)
* @poVoq (issue master)
* @MrCustomizer
* @swestersund
* @redplanet
* @Questlog
* @MoathZ
* @klonfish
* @derpeter1
* @werner.schiller
* @nerdoc
* @llucax
* @PL5bTStMZLduri
* @quizilkend
* @archit3kt (test master)
* @mr-manuel
* @eldiep
* @denics (splitwise master)
* @yward
* @xsus95
* @Helloha
* @puerki
* @deepbluev7
* @Allirion
* @jaroslaw.gerin
