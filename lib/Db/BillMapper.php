<?php

/**
 * Nextcloud - cospend
 *
 * This file is licensed under the Affero General Public License version 3 or
 * later. See the COPYING file.
 *
 * @author Julien Veyssier <eneiluj@posteo.net
 * @copyright Julien Veyssier 2019
 */

 namespace OCA\Cospend\Db;

use OCA\Cospend\Exception\CospendFatalException;
use OCP\AppFramework\Db\DoesNotExistException;
use OCP\AppFramework\Db\Entity;
use OCP\AppFramework\Db\MultipleObjectsReturnedException;
use OCP\AppFramework\Db\QBMapper;
use OCP\DB\QueryBuilder\IQueryBuilder;
use OCP\IDBConnection;

class BillMapper extends QBMapper {

    public function __construct(IDBConnection $db) {
        parent::__construct($db, 'cospend_bills');
    }

	/***
	 * @param string $projectId
	 * @param int|null $dateMin
	 * @param int|null $dateMax
	 * @param string|null $paymentMode
	 * @param int|null $categoryId
	 * @param string|null $amountMin
	 * @param string|null $amountMax
	 * @param int|null $lastChanged
	 * @return Bill[]|Entity[]
	 */
    public function find($projectId, $dateMin=null, $dateMax=null, $paymentMode=null, $categoryId=null,
						 $amountMin=null, $amountMax=null, $lastChanged=null)
	{
		$qb = $this->db->getQueryBuilder();
		$qb->select('id')
			->from($this->tableName)
			->where(
				$qb->expr()->eq('projectid', $qb->createNamedParameter($projectId, IQueryBuilder::PARAM_STR))
			);

		if(!is_null($dateMin)) {
			$qb->andWhere(
				$qb->expr()->gte('timestamp', $qb->createNamedParameter($dateMin, IQueryBuilder::PARAM_INT))
			);
		}
		if(!is_null($dateMax)) {
			$qb->andWhere(
				$qb->expr()->lte('timestamp', $qb->createNamedParameter($dateMax, IQueryBuilder::PARAM_INT))
			);
		}
		if(!is_null($paymentMode)) {
			$qb->andWhere(
				$qb->expr()->eq('paymentmode', $qb->createNamedParameter($paymentMode, IQueryBuilder::PARAM_STR))
			);
		}
		if(!is_null($categoryId)) {
			if ($categoryId === -100) {
				$or = $qb->expr()->orx();
				$or->add($qb->expr()->isNull('categoryid'));
				$or->add($qb->expr()->neq('categoryid', $qb->createNamedParameter(CAT_REIMBURSEMENT, IQueryBuilder::PARAM_INT)));
				$qb->andWhere($or);
			}
			else {
				$qb->andWhere(
					$qb->expr()->eq('categoryid', $qb->createNamedParameter($categoryId), IQueryBuilder::PARAM_INT)
				);
			}
		}
		if(!is_null($amountMin)) {
			$qb->andWhere(
				$qb->expr()->gte('amount', $qb->createNamedParameter($amountMin, IQueryBuilder::PARAM_STR))
			);
		}
		if(!is_null($amountMax)) {
			$qb->andWhere(
				$qb->expr()->lte('amount', $qb->createNamedParameter($amountMax, IQueryBuilder::PARAM_STR))
			);
		}
		if(!is_null($lastChanged)) {
			$qb->andWhere(
				$qb->expr()->gt('lastchanged', $qb->createNamedParameter(intval($lastChanged), IQueryBuilder::PARAM_INT))
			);
		}

		return $this->findEntities($qb);
	}

	/***
	 * @param string $projectId
	 * @param string $what
	 * @param string $timestamp
	 * @param string $amount
	 * @param int $payerId
	 * @param string $repeat
	 * @param int $repeatAllActive
	 * @param string $repeatUntil
	 * @param int $categoryId
	 * @param string $paymentMode
	 * @param int $lastChanged
	 * @return Bill|Entity
	 * @throws CospendFatalException
	 * @throws DoesNotExistException
	 */
	public function add($projectId, $what, $timestamp, $amount, $payerId, $repeat, $repeatAllActive, $repeatUntil, $categoryId, $paymentMode, $lastChanged) {
		$qb = $this->db->getQueryBuilder();

		$qb->insert($this->tableName)
			->values([
				'projectid' => $qb->createNamedParameter($projectId, IQueryBuilder::PARAM_STR),
				'what' => $qb->createNamedParameter($what, IQueryBuilder::PARAM_STR),
				'timestamp' => $qb->createNamedParameter($timestamp, IQueryBuilder::PARAM_STR),
				'amount' => $qb->createNamedParameter($amount, IQueryBuilder::PARAM_STR),
				'payerid' => $qb->createNamedParameter($payerId, IQueryBuilder::PARAM_INT),
				'repeat' => $qb->createNamedParameter($repeat, IQueryBuilder::PARAM_STR),
				'repeatallactive' => $qb->createNamedParameter($repeatAllActive, IQueryBuilder::PARAM_INT),
				'repeatuntil' => $qb->createNamedParameter($repeatUntil, IQueryBuilder::PARAM_STR),
				'categoryid' => $qb->createNamedParameter($categoryId, IQueryBuilder::PARAM_INT),
				'paymentmode' => $qb->createNamedParameter($paymentMode, IQueryBuilder::PARAM_STR),
				'lastchanged' => $qb->createNamedParameter($lastChanged, IQueryBuilder::PARAM_INT)
			]);
		$qb->execute();

		return $this->get($qb->getLastInsertId());
	}

	/**
	 * @param int $id
	 * @return Bill|Entity
	 * @throws DoesNotExistException
	 * @throws CospendFatalException
	 */
	public function get($id) {
		$qb = $this->db->getQueryBuilder();

		$qb->select('*')
			->from($this->tableName)
			->Where(
				$qb->expr()->eq('id', $qb->createNamedParameter($id, IQueryBuilder::PARAM_INT))
			);

		try {
			return $this->findEntity($qb);
		} catch (MultipleObjectsReturnedException $e) {
			throw new CospendFatalException("DB error: bill id not unique.");
		}
	}

	/***
	 * @param int $id
	 * @param string $what
	 * @param int $timestamp
	 * @param int $amount
	 * @param int $payerId
	 * @param string $repeat
	 * @param int $repeatAllActive
	 * @param string|null $repeatUntil
	 * @param int $categoryId
	 * @param string $paymentMode
	 * @param int $lastChanged
	 * @return bool
	 */
	public function updateById($id, $what, $timestamp, $amount, $payerId, $repeat, $repeatAllActive, $repeatUntil, $categoryId, $paymentMode, $lastChanged) {
		$qb = $this->db->getQueryBuilder();
		$qb->update('cospend_bills');

		$qb->set('what', $qb->createNamedParameter($what, IQueryBuilder::PARAM_STR))

			->set('amount', $qb->createNamedParameter($amount, IQueryBuilder::PARAM_STR))
			->set('repeat', $qb->createNamedParameter($repeat, IQueryBuilder::PARAM_STR))
			->set('repeatallactive', $qb->createNamedParameter($repeatAllActive, IQueryBuilder::PARAM_INT))
			->set('repeatuntil', $qb->createNamedParameter($repeatUntil, IQueryBuilder::PARAM_STR))
			->set('categoryid', $qb->createNamedParameter($categoryId, IQueryBuilder::PARAM_INT))
			->set('paymentmode', $qb->createNamedParameter($paymentMode, IQueryBuilder::PARAM_STR))
			->set('lastchanged', $qb->createNamedParameter($lastChanged, IQueryBuilder::PARAM_INT))
			->where(
				$qb->expr()->eq('id', $qb->createNamedParameter($id, IQueryBuilder::PARAM_INT))
			);

		return $qb->execute();
	}

	/**
	 * @param int $billId
	 * @return bool success
	 */
	public function deleteById($billId) {
		$qb = $this->db->getQueryBuilder();
		$qb->delete($this->tableName)
			->where(
				$qb->expr()->eq('id', $qb->createNamedParameter($billId, IQueryBuilder::PARAM_INT))
			);
		$rows = $qb->execute();

		return $rows === 1;
	}
}
