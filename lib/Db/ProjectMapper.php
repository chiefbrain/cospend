<?php

/**
 * Nextcloud - cospend
 *
 * This file is licensed under the Affero General Public License version 3 or
 * later. See the COPYING file.
 *
 * @author Julien Veyssier <eneiluj@posteo.net>
 * @copyright Julien Veyssier 2019
 */

namespace OCA\Cospend\Db;

use OCA\Cospend\Exception\CospendFatalException;
use OCP\AppFramework\Db\DoesNotExistException;
use OCP\AppFramework\Db\Entity;
use OCP\AppFramework\Db\MultipleObjectsReturnedException;
use OCP\AppFramework\Db\QBMapper;
use OCP\DB\QueryBuilder\IQueryBuilder;
use OCP\IDBConnection;

class ProjectMapper extends QBMapper {

    public function __construct(IDBConnection $db) {
        parent::__construct($db, 'cospend_projects');
    }

	/***

	 * @param string $name
	 * @param string $id
	 * @param string $password
	 * @param string $contactEmail
	 * @param int $lastChanged
	 * @param string|null $userId
	 * @return Project|Entity
	 * @throws CospendFatalException
	 * @throws DoesNotExistException
	 */
    public function add($name, $id, $password, $contactEmail, $lastChanged, $userId = null) {
		$qb = $this->db->getQueryBuilder();

		$qb->insert($this->tableName)
			->values([
				'name' => $qb->createNamedParameter($name, IQueryBuilder::PARAM_STR),
				'id' => $qb->createNamedParameter($id, IQueryBuilder::PARAM_STR),
				'password' => $qb->createNamedParameter($password, IQueryBuilder::PARAM_STR),
				'email' => $qb->createNamedParameter($contactEmail, IQueryBuilder::PARAM_STR),
				'lastchanged' => $qb->createNamedParameter($lastChanged, IQueryBuilder::PARAM_INT),
				'userid' => $qb->createNamedParameter($userId, IQueryBuilder::PARAM_STR),
			]);
		$qb->execute();

		return $this->get($qb->getLastInsertId());
	}

	/**
	 * @param string $projectId
	 * @return Project|Entity
	 * @throws DoesNotExistException
	 * @throws CospendFatalException
	 */
    public function get($projectId) {
        $qb = $this->db->getQueryBuilder();

        $qb->select('*')
            ->from($this->tableName)
            ->where(
                $qb->expr()->eq('id', $qb->createNamedParameter($projectId, IQueryBuilder::PARAM_STR))
            );

		try {
			return $this->findEntity($qb);
		} catch (MultipleObjectsReturnedException $e) {
			throw new CospendFatalException("DB error: project id not unique.");
		}
	}

	/***
	 * @param string $name
	 * @return Project|Entity
	 * @throws DoesNotExistException
	 * @throws CospendFatalException
	 */
	public function getByName(string $name) {
		$qb = $this->db->getQueryBuilder();

		$qb->select('id')
			->from($this->tableName)
			->where(
				$qb->expr()->eq('id', $qb->createNamedParameter($name, IQueryBuilder::PARAM_STR))
			);

		try {
			return $this->findEntity($qb);
		} catch (MultipleObjectsReturnedException $e) {
			throw new CospendFatalException("DB error: project id not unique.");
		}
	}

	/***
	 * @param string $projectId
	 * @param string $name
	 * @param string $contactEmail
	 * @param string $password
	 * @param string $autoExport
	 * @param string $currencyName
	 * @param string $lastChanged
	 * @return bool success
	 */
	public function updateById($projectId, $name, $contactEmail, $password, $autoExport, $currencyName, $lastChanged) {
		$qb = $this->db->getQueryBuilder();

		$qb->update($this->tableName)
			->set('email', $qb->createNamedParameter($contactEmail, IQueryBuilder::PARAM_STR))
			->set('password', $qb->createNamedParameter($password, IQueryBuilder::PARAM_STR))
			->set('autoexport', $qb->createNamedParameter($autoExport, IQueryBuilder::PARAM_STR))
			->set('lastchanged', $qb->createNamedParameter($lastChanged, IQueryBuilder::PARAM_INT))
			->set('name', $qb->createNamedParameter($name, IQueryBuilder::PARAM_STR))
			->where(
				$qb->expr()->eq('id', $qb->createNamedParameter($projectId, IQueryBuilder::PARAM_STR))
			);

		return $qb->execute();
	}

	/***
	 * @param string $projectId
	 * @return bool success
	 */
	public function deleteById($projectId) {
		$qb = $this->db->getQueryBuilder();

		$qb->delete($this->tableName)
			->where(
				$qb->expr()->eq('id', $qb->createNamedParameter($projectId, IQueryBuilder::PARAM_STR))
			);
		$rows = $qb->execute();

		return $rows === 1;
	}

}
