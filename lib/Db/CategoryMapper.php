<?php

/**
 * Nextcloud - cospend
 *
 * This file is licensed under the Affero General Public License version 3 or
 * later. See the COPYING file.
 *
 * @author Julien Veyssier <eneiluj@posteo.net
 * @copyright Julien Veyssier 2019
 */

namespace OCA\Cospend\Db;

use OCA\Cospend\Exception\CospendFatalException;
use OCP\AppFramework\Db\DoesNotExistException;
use OCP\AppFramework\Db\Entity;
use OCP\AppFramework\Db\MultipleObjectsReturnedException;
use OCP\AppFramework\Db\QBMapper;
use OCP\DB\QueryBuilder\IQueryBuilder;
use OCP\IDBConnection;

class CategoryMapper extends QBMapper {

	public function __construct(IDBConnection $db) {
		parent::__construct($db, 'cospend_project_categories');
	}

	/**
	 * @param string $projectId
	 * @return Category[]|Entity[]
	 */
	public function list($projectId) {
		$qb = $this->db->getQueryBuilder();

		$qb->select('*')
			->from($this->tableName)
			->where(
				$qb->expr()->eq('projectid', $qb->createNamedParameter($projectId, IQueryBuilder::PARAM_STR))
			);

		return $this->findEntities($qb);
	}

	/**
	 * @param string $projectId
	 * @param string $name
	 * @param string $icon
	 * @param string $color
	 * @return Category|Entity
	 * @throws CospendFatalException
	 * @throws DoesNotExistException
	 */
	public function add($projectId, $name, $icon, $color) {
		$qb = $this->db->getQueryBuilder();

		$qb->insert($this->tableName)
			->values([
				'projectid' => $qb->createNamedParameter($projectId, IQueryBuilder::PARAM_STR),
				'icon' => $qb->createNamedParameter($icon, IQueryBuilder::PARAM_STR),
				'color' => $qb->createNamedParameter($color, IQueryBuilder::PARAM_STR),
				'name' => $qb->createNamedParameter($name, IQueryBuilder::PARAM_STR)
			]);
		$qb->execute();

		return $this->get($qb->getLastInsertId());
	}

	/**
	 * @param int $id
	 * @return Category|Entity
	 * @throws DoesNotExistException
	 * @throws CospendFatalException
	 */
	public function get($id) {
		$qb = $this->db->getQueryBuilder();

		$qb->select('*')
			->from($this->tableName)
			->Where(
				$qb->expr()->eq('id', $qb->createNamedParameter($id, IQueryBuilder::PARAM_INT))
			);
		try {
			return $this->findEntity($qb);
		} catch (MultipleObjectsReturnedException $e) {
			throw new CospendFatalException("DB error: category id not unique.");
		}
	}

	/**
	 * @param int $categoryId
	 * @param string $name
	 * @param string $icon
	 * @param string $color
	 * @return bool success
	 */
	public function updateById($categoryId, $name, $icon, $color) {
		$qb = $this->db->getQueryBuilder();
		$qb->update($this->tableName);
		$qb->set('name', $qb->createNamedParameter($name, IQueryBuilder::PARAM_STR));
		$qb->set('icon', $qb->createNamedParameter($icon, IQueryBuilder::PARAM_STR));
		$qb->set('color', $qb->createNamedParameter($color, IQueryBuilder::PARAM_STR));
		$qb->where(
			$qb->expr()->eq('id', $qb->createNamedParameter($categoryId, IQueryBuilder::PARAM_INT))
		);

		return $qb->execute();
	}

	/**
	 * @param int $categoryId
	 * @return bool success
	 */
	public function deleteById($categoryId) {

		// get rid of this category in bills
		$qb = $this->db->getQueryBuilder();
		$qb->update('cospend_bills');
		$qb->set('categoryid', $qb->createNamedParameter(0, IQueryBuilder::PARAM_INT));
		$qb->where(
			$qb->expr()->eq('categoryid', $qb->createNamedParameter($categoryId, IQueryBuilder::PARAM_INT))
		);
		$qb->execute();
		$qb->resetQueryParts();

		// get rid of category
		$qb->delete($this->tableName)
			->where(
				$qb->expr()->eq('id', $qb->createNamedParameter($categoryId, IQueryBuilder::PARAM_INT))
			);
		$rows = $qb->execute();

		return $rows === 1;
	}

}
