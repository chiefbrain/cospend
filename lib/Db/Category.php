<?php

/**
 * Nextcloud - cospend
 *
 * This file is licensed under the Affero General Public License version 3 or
 * later. See the COPYING file.
 *
 * @author Oliver Wittkopf <gitlab@4ward.wittkopf.eu>
 * @copyright Oliver Wittkopf 2019
 */

namespace OCA\Cospend\Db;

use OCP\AppFramework\Db\Entity;

/**
 * @method string getProjectid()
 * @method void setProjectid(string $value)
 * @method string getName()
 * @method void setName(string $value)
 * @method string getIcon()
 * @method void setIcon(string $value)
 * @method string getColor()
 * @method void setColor(string $value)
 */

class Category extends Entity {
	protected $projectid;
	protected $name;
	protected $icon;
	protected $color;

	public function __construct() {
		$this->addType('id', 'integer');
		$this->addType('projectid', 'string');
		$this->addType('name', 'string');
		$this->addType('icon', 'string');
		$this->addType('color', 'string');
	}
}
