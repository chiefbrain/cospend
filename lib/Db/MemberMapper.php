<?php

namespace OCA\Cospend\Db;

use OCA\Cospend\Exception\CospendFatalException;
use OCP\AppFramework\Db\DoesNotExistException;
use OCP\AppFramework\Db\Entity;
use OCP\AppFramework\Db\MultipleObjectsReturnedException;
use OCP\AppFramework\Db\QBMapper;
use OCP\DB\QueryBuilder\IQueryBuilder;
use OCP\IDBConnection;

class MemberMapper extends QBMapper {

	public function __construct(IDBConnection $db) {
		parent::__construct($db, 'cospend_members');
	}

	/***
	 * @param string $projectId
	 * @param string $order
	 * @param string|null $lastchanged
	 * @return Member[]|Entity[]
	 */
	public function list($projectId, $order, $lastchanged=null) {
		$qb = $this->db->getQueryBuilder();

		$qb->select('*')
			->from($this->tableName)
			->where(
				$qb->expr()->eq('projectid', $qb->createNamedParameter($projectId, IQueryBuilder::PARAM_STR))
			);

		if ($lastchanged !== null and is_numeric($lastchanged)) {
			$qb->andWhere(
				$qb->expr()->gt('lastchanged', $qb->createNamedParameter($lastchanged, IQueryBuilder::PARAM_INT))
			);
		}
		$qb->orderBy($order, 'ASC');

		return $this->findEntities($qb);
	}

	/***
	 * @param string $projectId
	 * @param string $name
	 * @return Member[]|Entity[]
	 */
	public function listByName($projectId, $name) {
		$qb = $this->db->getQueryBuilder();
		$qb->select('*')
			->from($this->tableName)
			->where(
				$qb->expr()->eq('projectid', $qb->createNamedParameter($projectId, IQueryBuilder::PARAM_STR))
			)
			->andWhere(
				$qb->expr()->eq('name', $qb->createNamedParameter($name, IQueryBuilder::PARAM_STR))
			);

		return $this->findEntities($qb);
	}

	/***
	 * @param string $projectId
	 * @param string $name
	 * @param float $weight
	 * @param bool $active
	 * @param int $timestamp
	 * @param string|null $color
	 * @return Member|Entity
	 * @throws DoesNotExistException
	 * @throws CospendFatalException
	 */
	public function add($projectId, $name, $weight, $active, $color, $timestamp) {
		$qb = $this->db->getQueryBuilder();
		$qb->insert($this->tableName)
			->values([
				'projectid' => $qb->createNamedParameter($projectId, IQueryBuilder::PARAM_STR),
				'name' => $qb->createNamedParameter($name, IQueryBuilder::PARAM_STR),
				'weight' => $qb->createNamedParameter($weight, IQueryBuilder::PARAM_STR),
				'activated' => $qb->createNamedParameter($active, IQueryBuilder::PARAM_BOOL),
				'color' => $qb->createNamedParameter($color, IQueryBuilder::PARAM_STR),
				'lastchanged' => $qb->createNamedParameter($timestamp, IQueryBuilder::PARAM_INT)
			]);
		$qb->execute();

		return $this->get($qb->getLastInsertId());
	}

	/***
	 * @param int $id
	 * @return Member|Entity
	 * @throws DoesNotExistException
	 * @throws CospendFatalException
	 */
	public function get($id) {
		$qb = $this->db->getQueryBuilder();
		$qb->select('*')
			->from($this->tableName)
			->Where(
				$qb->expr()->eq('id', $qb->createNamedParameter($id, IQueryBuilder::PARAM_INT))
			);
		try {
			return $this->findEntity($qb);
		} catch (MultipleObjectsReturnedException $e) {
			throw new CospendFatalException("DB error: member id not unique.");
		}
	}

	/***
	 * @param int $id
	 * @param string $name
	 * @param float $weight
	 * @param int $timestamp
	 * @param string|null $color
	 * @return bool
	 */
	public function updateById($id, $name, $weight, $timestamp, $color) {
		$qb = $this->db->getQueryBuilder();

		$qb->update($this->tableName);
		$qb->set('weight', $qb->createNamedParameter($weight, IQueryBuilder::PARAM_STR))
			->set('lastchanged', $qb->createNamedParameter($timestamp, IQueryBuilder::PARAM_INT))

			->set('name', $qb->createNamedParameter($name, IQueryBuilder::PARAM_STR))
			->set('color', $qb->createNamedParameter($color, IQueryBuilder::PARAM_STR))
			->where(
				$qb->expr()->eq('id', $qb->createNamedParameter($id, IQueryBuilder::PARAM_INT))
			);

		return $qb->execute();
	}

	/***
	 * @param int $id
	 * @return bool success
	 */
	public function deactivate($id) {
		$qb = $this->db->getQueryBuilder();
		$qb->update($this->tableName);
		$qb->set('activated', $qb->createNamedParameter(0, IQueryBuilder::PARAM_INT));
		$qb->where(
			$qb->expr()->eq('id', $qb->createNamedParameter($id, IQueryBuilder::PARAM_INT))
		);

		return $qb->execute();
}

}
