<?php

/**
 * Nextcloud - cospend
 *
 * This file is licensed under the Affero General Public License version 3 or
 * later. See the COPYING file.
 *
 * @author Julien Veyssier <eneiluj@posteo.net
 * @copyright Julien Veyssier 2019
 */

namespace OCA\Cospend\Db;

use OCP\AppFramework\Db\Entity;

/**
 * @method int getBillid()
 * @method void setBillid(int $value)
 * @method int getMemberid()
 * @method void setMemberid(int $value)
 */

class BillOwer extends Entity {

    protected $billid;
    private $memberid;

    public function __construct() {
        $this->addType('id', 'integer');
		$this->addType('billid', 'integer');
		$this->addType('memberid', 'integer');
    }
}
