<?php

namespace OCA\Cospend\Db;

use OCA\Cospend\Exception\CospendFatalException;
use OCP\AppFramework\Db\DoesNotExistException;
use OCP\AppFramework\Db\Entity;
use OCP\AppFramework\Db\MultipleObjectsReturnedException;
use OCP\AppFramework\Db\QBMapper;
use OCP\DB\QueryBuilder\IQueryBuilder;
use OCP\IDBConnection;

class CurrencyMapper extends QBMapper {

	public function __construct(IDBConnection $db) {
		parent::__construct($db, 'cospend_currencies');
	}

	/**
	 * @param string $projectId
	 * @return Currency[]|Entity[]
	 */
	public function list($projectId) {

		$qb = $this->db->getQueryBuilder();
		$qb->select('*')
			->from($this->tableName)
			->where(
				$qb->expr()->eq('projectid', $qb->createNamedParameter($projectId, IQueryBuilder::PARAM_STR))
			);

		return $this->findEntities($qb);
	}

	/**
	 * @param string $projectId
	 * @param string $name
	 * @param float $exchangeRate
	 * @return Currency|Entity
	 * @throws DoesNotExistException
	 * @throws CospendFatalException
	 */
	public function add($projectId, $name, $exchangeRate) {
		$qb = $this->db->getQueryBuilder();

		$qb->insert($this->tableName)
			->values([
				'projectid' => $qb->createNamedParameter($projectId, IQueryBuilder::PARAM_STR),
				'name' => $qb->createNamedParameter($name, IQueryBuilder::PARAM_STR),
				'exchange_rate' => $qb->createNamedParameter($exchangeRate, IQueryBuilder::PARAM_INT)
			]);

		$qb->execute();

		return $this->get($qb->getLastInsertId());
	}

	/**
	 * @param int $currencyId
	 * @return Currency|Entity
	 * @throws DoesNotExistException
	 * @throws CospendFatalException
	 */
	public function get($currencyId) {
		$qb = $this->db->getQueryBuilder();
		$qb->select('*')
			->from($this->tableName)
			->Where(
				$qb->expr()->eq('id', $qb->createNamedParameter($currencyId, IQueryBuilder::PARAM_INT))
			);
		try {
			return $this->findEntity($qb);
		} catch (MultipleObjectsReturnedException $e) {
			throw new CospendFatalException("DB error: currency id not unique.");
		}
	}

	/**
	 * @param int $currencyId
	 * @param string $name
	 * @param float $exchangeRate
	 * @return bool success
	 */
	public function updateById($currencyId, $name, $exchangeRate) {
		$qb = $this->db->getQueryBuilder();

		$qb->update($this->tableName);
		$qb->set('exchange_rate', $qb->createNamedParameter($exchangeRate, IQueryBuilder::PARAM_INT));
		$qb->set('name', $qb->createNamedParameter($name, IQueryBuilder::PARAM_STR));
		$qb->where(
			$qb->expr()->eq('id', $qb->createNamedParameter($currencyId, IQueryBuilder::PARAM_INT))
		);

		return $qb->execute();
	}

	/**
	 * @param int $currencyId
	 * @return bool success
	 */
	public function deleteById($currencyId) {
		$qb = $this->db->getQueryBuilder();
		$qb->delete($this->tableName)
			->where(
				$qb->expr()->eq('id', $qb->createNamedParameter($currencyId, IQueryBuilder::PARAM_INT))
			);
		$rows = $qb->execute();

		return $rows === 1;
	}
}
