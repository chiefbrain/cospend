<?php

/**
 * Nextcloud - cospend
 *
 * This file is licensed under the Affero General Public License version 3 or
 * later. See the COPYING file.
 *
 * @author Julien Veyssier <eneiluj@posteo.net
 * @copyright Julien Veyssier 2019
 */

namespace OCA\Cospend\Db;

use OCA\Cospend\Exception\CospendFatalException;
use OCP\AppFramework\Db\DoesNotExistException;
use OCP\AppFramework\Db\Entity;
use OCP\AppFramework\Db\MultipleObjectsReturnedException;
use OCP\AppFramework\Db\QBMapper;
use OCP\DB\QueryBuilder\IQueryBuilder;
use OCP\IDBConnection;

class BillOwerMapper extends QBMapper {

	public function __construct(IDBConnection $db) {
		parent::__construct($db, 'cospend_bill_owers');
	}

	/**
	 * @param int $billId
	 * @return Member[]|Entity[]
	 */
	public function list($billId) {
		$qb = $this->db->getQueryBuilder();

		$qb->select('m.*')
			->from($this->tableName)
			->innerJoin('bo', 'cospend_members', 'm', $qb->expr()->eq('bo.memberid', 'm.id'))
			->where(
				$qb->expr()->eq('bo.billid', $qb->createNamedParameter($billId, IQueryBuilder::PARAM_INT))
			);

		return $this->findEntities($qb);
	}

	/***
	 * @param $billId
	 * @param int $owerId
	 * @return BillOwer|Entity
	 * @throws CospendFatalException
	 * @throws DoesNotExistException
	 */
	public function add($billId, $owerId) {
		$qb = $this->db->getQueryBuilder();

		$qb->insert($this->tableName)
			->values([
				'billid' => $qb->createNamedParameter($billId, IQueryBuilder::PARAM_INT),
				'memberid' => $qb->createNamedParameter($owerId, IQueryBuilder::PARAM_INT)
			]);
		$qb->execute();

		return $this->get($qb->getLastInsertId());
	}

	/***
	 * @param $billOwerId
	 * @return BillOwer|Entity
	 * @throws CospendFatalException
	 * @throws DoesNotExistException
	 */
	public function get($billOwerId) {
		$qb = $this->db->getQueryBuilder();
		$qb->select('*')
			->from($this->tableName)
			->Where(
				$qb->expr()->eq('id', $qb->createNamedParameter($billOwerId, IQueryBuilder::PARAM_INT))
			);
		try {
			return $this->findEntity($qb);
		} catch (MultipleObjectsReturnedException $e) {
			throw new CospendFatalException("DB error: billOwer id not unique.");
		}
	}

	/**
	 * @param int $billId
	 * @return int affected billOwers
	 */
	public function deleteByBill($billId) {
		$qb = $this->db->getQueryBuilder();
		$qb->delete($this->tableName)
			->where(
				$qb->expr()->eq('billid', $qb->createNamedParameter($billId, IQueryBuilder::PARAM_INT))
			);

		return $qb->execute();
	}
}
