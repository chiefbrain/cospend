<?php

/**
 * Nextcloud - cospend
 *
 * This file is licensed under the Affero General Public License version 3 or
 * later. See the COPYING file.
 *
 * @author Julien Veyssier <eneiluj@posteo.net>
 * @copyright Julien Veyssier 2019
 */

namespace OCA\Cospend\Db;

use OCA\Cospend\Exception\CospendFatalException;
use OCP\AppFramework\Db\DoesNotExistException;
use OCP\AppFramework\Db\Entity;
use OCP\AppFramework\Db\MultipleObjectsReturnedException;
use OCP\AppFramework\Db\QBMapper;
use OCP\DB\QueryBuilder\IQueryBuilder;
use OCP\IDBConnection;

class ShareMapper extends QBMapper {

    public function __construct(IDBConnection $db) {
        parent::__construct($db, 'cospend_shares');
    }

	/**
	 * @param string $projectId
	 * @return Share[]|Entity[]
	 */
	public function list($projectId) {

		$qb = $this->db->getQueryBuilder();
		$qb->select('*')
			->from($this->tableName)
			->where(
				$qb->expr()->eq('projectid', $qb->createNamedParameter($projectId, IQueryBuilder::PARAM_STR))
			);

		return $this->findEntities($qb);
	}

	/**
	 * @param int $shareId
	 * @return Share|Entity
	 * @throws DoesNotExistException
	 * @throws CospendFatalException
	 */
	public function get($shareId) {
		$qb = $this->db->getQueryBuilder();
		$qb->select('*')
			->from($this->tableName)
			->Where(
				$qb->expr()->eq('id', $qb->createNamedParameter($shareId, IQueryBuilder::PARAM_INT))
			);
		try {
			return $this->findEntity($qb);
		} catch (MultipleObjectsReturnedException $e) {
			throw new CospendFatalException("DB error: share id not unique.");
		}
	}

	/**
	 * @param int $shareId
	 * @return bool success
	 */
	public function deleteById($shareId) {
		$qb = $this->db->getQueryBuilder();
		$qb->delete($this->tableName)
			->where(
				$qb->expr()->eq('id', $qb->createNamedParameter($shareId, IQueryBuilder::PARAM_INT))
			);
		$rows = $qb->execute();

		return $rows === 1;
	}
}
