<?php

/**
 * Nextcloud - cospend
 *
 * This file is licensed under the Affero General Public License version 3 or
 * later. See the COPYING file.
 *
 * @author Julien Veyssier <eneiluj@posteo.net
 * @copyright Julien Veyssier 2019
 */

namespace OCA\Cospend\Db;

use OCP\AppFramework\Db\Entity;

/**
 * @method string getWhat()
 * @method void setWhat(string $value)
 * @method int getPayerid()
 * @method void setPayerid(int $value)
 * @method float getTimestamp()
 * @method void setTimestamp(float $value)
 * @method float getAmount()
 * @method void setAmount(float $value)
 * @method string getRepeat()
 * @method void setRepeat(string $value)
 * @method integer getRepeatallactive()
 * @method void setRepeatallactive(integer $value)
 * @method string getProjectid()
 * @method void setProjectid(string $value)
 * @method integer getCategoryid()
 * @method void setCategoryid(integer $value)
 * @method string getPaymentmode()
 * @method void setPaymentmode(string $value)
 * @method integer getLastchanged()
 * @method void setLastchanged(integer $value)
 * @method string getRepeatuntil()
 * @method void setRepeatuntil(string $value)
 */

class Bill extends Entity {

    protected $what;
    protected $payerid;
    protected $timestamp;
	protected $amount;
	protected $repeat;
    protected $repeatallactive;
    protected $projectid;
    protected $categoryid;
    protected $paymentmode;
    protected $lastchanged;
    protected $repeatuntil;
    public $owers;

    public function __construct() {
        $this->addType('id', 'integer');
        $this->addType('what', 'string');
        $this->addType('payerid', 'integer');
        $this->addType('timestamp', 'float');
        $this->addType('amount', 'float');
        $this->addType('repeat', 'string');
        $this->addType('repeatallactive', 'integer');
        $this->addType('projectid', 'string');
        $this->addType('categoryid', 'integer');
        $this->addType('paymentmode', 'string');
        $this->addType('lastchanged', 'integer');
        $this->addType('repeatuntil', 'string');

        $this->owers = array();
    }
}
