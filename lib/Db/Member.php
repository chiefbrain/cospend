<?php

/**
 * Nextcloud - cospend
 *
 * This file is licensed under the Affero General Public License version 3 or
 * later. See the COPYING file.
 *
 * @author Julien Veyssier <eneiluj@posteo.net
 * @copyright Julien Veyssier 2019
 */

namespace OCA\Cospend\Db;

use OCP\AppFramework\Db\Entity;
/**
 * @method int getProjectid()
 * @method void setProjectid(int $value)
 * @method string getName()
 * @method void setName(string $value)
 * @method float getWeight()
 * @method void setWeight(float $value)
 * @method bool getActivated()
 * @method void setActivated(bool $value)
 * @method string getColor()
 * @method void setColor(string $value)
 * @method integer getLastchanged()
 * @method void setLastchanged(integer $value)
 */

class Member extends Entity {

	protected $projectid;
	protected $name;
	protected $weight;
	protected $activated;
	protected $color;
	protected $lastchanged;

	public function __construct() {
		$this->addType('id', 'integer');
		$this->addType('projectid', 'string');
		$this->addType('name', 'string');
		$this->addType('weight', 'float');
		$this->addType('activated', 'bool');
		$this->addType('color', 'string');
		$this->addType('lastchanged', 'integer');

	}
}
