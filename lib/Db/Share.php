<?php

/**
 * Nextcloud - cospend
 *
 * This file is licensed under the Affero General Public License version 3 or
 * later. See the COPYING file.
 *
 * @author Julien Veyssier <eneiluj@posteo.net
 * @copyright Julien Veyssier 2019
 */

namespace OCA\Cospend\Db;

use OCP\AppFramework\Db\Entity;

/**
 * @method string getProjectid()
 * @method void setProjectid(string $value)
 */

class Share extends Entity {

    protected $what;

    public function __construct() {
        $this->addType('id', 'integer');
        $this->addType('projectid', 'string');
    }
}
