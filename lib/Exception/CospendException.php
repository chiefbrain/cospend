<?php

namespace OCA\Cospend\Exception;

use Exception;

class CospendException extends Exception
{
	public $message;
	public $exception;

	/***
	 * CospendExeption constructor.
	 *
	 * @param string $message
	 * @param Exception|null $exception
	 */
	public function __construct($message = "", $exception = null) {
			parent::__construct($message, 0);

		$this->message = $message;
		$this->exception = $exception;
	}
}
