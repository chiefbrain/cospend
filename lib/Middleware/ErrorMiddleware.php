<?php

namespace OCA\Cospend\Middleware;

use Exception;
use OCA\Cospend\Exception\CospendException;
use OCA\Mail\Http\JsonResponse;
use OCP\AppFramework\Controller;
use OCP\AppFramework\Http;
use OCP\AppFramework\Http\DataResponse;
use OCP\AppFramework\Http\Response;
use OCP\AppFramework\Middleware;
use OCP\IConfig;
use OCP\ILogger;

class ErrorMiddleware extends Middleware {

	/** @var ILogger */
	private $logger;

	/** @var IConfig */
	private $config;

	/**
	 * @param IConfig $config
	 * @param ILogger $logger
	 */
	public function __construct(IConfig $config, ILogger $logger) {
		$this->config = $config;
		$this->logger = $logger;
	}

	/**
	 * @param Controller $controller
	 * @param string $methodName
	 * @param Exception $exception
	 *
	 * @return Response
	 * @throws Exception
	 */
	public function afterException($controller, $methodName, Exception $exception) {
			parent::afterException($controller, $methodName, $exception);


		if ($exception instanceof CospendException) {
			$this->logger->logException($exception, [
				'level' => ILogger::ERROR
			]);

			if ($this->config->getSystemValue('debug', false)) {
				return JsonResponse::errorFromThrowable(
					$exception->exception,
					Http::STATUS_INTERNAL_SERVER_ERROR,
					[
						'debug' => true,
					]
				);
			} else {
				return new DataResponse($exception->message, 400);
			}
		}

		throw $exception;
	}
}
