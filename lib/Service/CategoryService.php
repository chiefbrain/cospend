<?php

/**
 * Nextcloud - cospend
 *
 * This file is licensed under the Affero General Public License version 3 or
 * later. See the COPYING file.
 *
 * @author Julien Veyssier
 * @copyright Julien Veyssier 2019
 */

namespace OCA\Cospend\Service;

use OCA\Cospend\Exception\CospendException;
use OCA\Cospend\Db\Category;
use OCA\Cospend\Db\CategoryMapper;
use OCA\Cospend\Db\ProjectMapper;
use OCP\AppFramework\Db\DoesNotExistException;
use OCP\IL10N;
use OCP\IConfig;
use OCP\ILogger;

class CategoryService extends BaseService {

	private $logger;
	private $config;
	private $trans;
	private $projectMapper;
	private $categoryMapper;

	public function __construct(ILogger $logger,
								IL10N $l10n,
								IConfig $config,
								ProjectMapper $projectMapper,
								CategoryMapper $categoryMapper) {
		parent::__construct($l10n);

		$this->trans = $l10n;
		$this->config = $config;
		$this->logger = $logger;
		$this->projectMapper = $projectMapper;
		$this->categoryMapper = $categoryMapper;
	}

	/***
	 * @param string $projectId
	 * @return Category[]
	 */
	public function list(string $projectId) {
		return $this->categoryMapper->list($projectId);
	}

	/**
	 * @param string $projectId
	 * @param string $name
	 * @param string $icon
	 * @param string $color
	 * @return Category
	 * @throws CospendException
	 */
	public function add($projectId, $name, $icon, $color) {
		try {
			return $this->categoryMapper->add(
				$projectId,
				$this->checkName($name),
				$icon,
				$color);
		} catch (DoesNotExistException $e) {
			throw new CospendException("Add category failed.", $e);
		}
	}

	/**
	 * @param string $projectId
	 * @param string $categoryId
	 * @return Category
	 * @throws CospendException
	 */
	public function get($projectId, $categoryId) {
		try {
			$category = $this->categoryMapper->get(
				$this->checkId($categoryId));

			if($category->getProjectid() !== $projectId)
				throw new CospendException($this->trans->t('This project have no such category.'));

			return $category;
		} catch (DoesNotExistException $e) {
			throw new CospendException("Get category failed.", $e);
		}
	}

	/**
	 * @param string $projectId
	 * @param string $categoryId
	 * @param string $name
	 * @param string $icon
	 * @param string $color
	 * @return bool success
	 * @throws CospendException
	 */
	public function update($projectId, $categoryId, $name, $icon, $color) {
		$categoryToEdit = $this->get($projectId, $categoryId);

		return $this->categoryMapper->updateById(
			$categoryToEdit->getId(),
			$this->checkName($name),
			$icon,
			$color);
	}

	/**
	 * @param string $projectId
	 * @param string $categoryId
	 * @return bool
	 * @throws CospendException
	 */
	public function deleteById($projectId, $categoryId) {
		$categoryToDelete = $this->get($projectId, $categoryId);
		return $this->categoryMapper->deleteById($categoryToDelete->getId());
	}
}

