<?php

/**
 * Nextcloud - cospend
 *
 * This file is licensed under the Affero General Public License version 3 or
 * later. See the COPYING file.
 *
 * @author Julien Veyssier
 * @copyright Julien Veyssier 2019
 */

namespace OCA\Cospend\Service;

use OCA\Cospend\Db\Share;
use OCA\Cospend\Db\ShareMapper;
use OCA\Cospend\Exception\CospendException;
use OCP\AppFramework\Db\DoesNotExistException;
use OCP\IL10N;
use OCP\IConfig;
use OCP\ILogger;

class ShareService extends BaseService {

	private $logger;
	private $config;
	private $trans;
	private $shareMapper;

	public function __construct(ILogger $logger,
								IL10N $l10n,
								IConfig $config,
								ShareMapper $shareMapper) {
		parent::__construct($l10n);

		$this->trans = $l10n;
		$this->config = $config;
		$this->logger = $logger;
		$this->shareMapper = $shareMapper;
	}

	/***
	 * @param int $projectId
	 * @return Share[]
	 */
	public function getShares($projectId) {
		return $this->shareMapper->list($projectId);
	}

	/**
	 * @param string $projectId
	 * @param string $shareId
	 * @return Share
	 * @throws CospendException
	 */
	public function get($projectId, $shareId) {
		try {
			$share = $this->shareMapper->get(
				$this->checkId($shareId));

			if($share->getProjectid() !== $projectId)
				throw new CospendException($this->trans->t('This project have no such currency.'));

			return $share;
		} catch (DoesNotExistException $e) {
			throw new CospendException("Get share failed.", $e);
		}
	}

	/**
	 * @param string $projectId
	 * @param string $shareId
	 * @return bool success
	 * @throws CospendException
	 */
	public function deleteShare(string $projectId, $shareId) {
		$shareToDelete = $this->get($projectId, $shareId);
		return $this->shareMapper->deleteById($shareToDelete->getId());
	}
}

