<?php

/**
 * Nextcloud - cospend
 *
 * This file is licensed under the Affero General Public License version 3 or
 * later. See the COPYING file.
 *
 * @author Julien Veyssier
 * @copyright Julien Veyssier 2019
 */

namespace OCA\Cospend\Service;

use OCA\Cospend\Exception\CospendException;
use OCA\Cospend\Db\Currency;
use OCA\Cospend\Db\CurrencyMapper;
use OCA\Cospend\Db\ProjectMapper;
use OCP\AppFramework\Db\DoesNotExistException;
use OCP\IL10N;
use OCP\IConfig;
use OCP\ILogger;

class CurrencyService extends BaseService {

	private $logger;
	private $config;
	private $trans;
	private $projectMapper;
	private $currencyMapper;

	public function __construct(ILogger $logger,
								IL10N $l10n,
								IConfig $config,
								ProjectMapper $projectMapper,
								CurrencyMapper $currencyMapper) {
		parent::__construct($l10n);

		$this->trans = $l10n;
		$this->config = $config;
		$this->logger = $logger;
		$this->projectMapper = $projectMapper;
		$this->currencyMapper = $currencyMapper;
	}

	/***
	 * @param string $exchangeRate
	 * @return float
	 * @throws CospendException
	 */
	private function checkExchangeRate($exchangeRate)
	{
		if(!is_float($exchangeRate)) {
			throw new CospendException($this->trans->t('Incorrect field value: Exchange rate'));
		}

		return floatval($exchangeRate);
	}

	/***
	 * @param string $projectId
	 * @return Currency[]
	 */
	public function list($projectId) {
		return $this->currencyMapper->list($projectId);
	}

	/**
	 * @param string $projectId
	 * @param string $name
	 * @param string $exchangeRate
	 * @return Currency
	 * @throws CospendException
	 */
	public function add($projectId, $name, $exchangeRate) {
		try {
			return $this->currencyMapper->add(
				$projectId,
				$this->checkName($name),
				$this->checkExchangeRate($exchangeRate));
		} catch (DoesNotExistException $e) {
			throw new CospendException("Add currency failed.", $e);
		}
	}

	/**
	 * @param string $projectId
	 * @param string $currencyId
	 * @return Currency
	 * @throws CospendException
	 */
	public function get($projectId, $currencyId) {
		try {
			$currency = $this->currencyMapper->get(
				$this->checkId($currencyId));

			if($currency->getProjectid() !== $projectId)
				throw new CospendException($this->trans->t('This project have no such currency.'));

			return $currency;
		} catch (DoesNotExistException $e) {
			throw new CospendException("Get currency failed.", $e);
		}
	}

	/**
	 * @param string $projectId
	 * @param string $currencyId
	 * @param string $name
	 * @param string $exchange_rate
	 * @return bool success
	 * @throws CospendException
	 */
	public function update($projectId, $currencyId, $name, $exchange_rate) {
		$currencyToEdit = $this->get($projectId, $currencyId);

		return $this->currencyMapper->updateById(
			$currencyToEdit->getId(),
			$this->checkName($name),
			$this->checkExchangeRate($exchange_rate));
	}

	/**
	 * @param string $projectId
	 * @param string $currencyId
	 * @return bool success
	 * @throws CospendException
	 */
	public function deleteById($projectId, $currencyId) {
		$currencyToDelete = $this->get($projectId, $currencyId);
		return $this->currencyMapper->deleteById($currencyToDelete->getId());
	}
}
