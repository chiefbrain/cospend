<?php

/**
 * Nextcloud - cospend
 *
 * This file is licensed under the Affero General Public License version 3 or
 * later. See the COPYING file.
 *
 * @author Julien Veyssier
 * @copyright Julien Veyssier 2019
 */

namespace OCA\Cospend\Service;

use DateTime;
use OCA\Cospend\Db\Bill;
use OCA\Cospend\Db\BillOwerMapper;
use OCA\Cospend\Db\Member;
use OCA\Cospend\Exception\CospendException;
use OCP\AppFramework\Db\DoesNotExistException;
use OCP\IL10N;
use OCP\IConfig;
use OCP\ILogger;

use OCA\Cospend\Db\BillMapper;

class BillService extends BaseService {

	private $logger;
	private $config;
	private $trans;
	private $billMapper;
	private $memberService;
    private $billOwerMapper;

	public function __construct(ILogger $logger,
								IL10N $l10n,
								IConfig $config,
								BillMapper $billMapper,
								MemberService $memberService,
								BillOwerMapper $billOwerMapper) {
		parent::__construct($l10n);

		$this->trans = $l10n;
		$this->config = $config;
		$this->logger = $logger;
		$this->billMapper = $billMapper;
		$this->memberService = $memberService;
		$this->billOwerMapper = $billOwerMapper;
	}

	/***
	 * @param string $what
	 * @return string
	 * @throws CospendException
	 */
	private function checkWhat($what) {
		if ($what === null || $what === '') {
			throw new CospendException($this->trans->t('This field is required'));
		}

		return $what;
	}

	/**
	 * @param string|null $timestamp
	 * @param string|null $date
	 * @return int
	 * @throws CospendException
	 */
	private function checkTimestamp($timestamp, $date) {
		// priority to timestamp (moneybuster might send both for a moment)
		if ($timestamp === null || !is_numeric($timestamp)) {
			if ($date === null || $date === '') {
				throw new CospendException($this->trans->t('Timestamp (or date) field is required'));
			} else {
				$dateTs = strtotime($date);
				if ($dateTs === false) {
					throw new CospendException($this->trans->t('Invalid date'));
				}

				return $dateTs;
			}
		} else {
			return intval($timestamp);
		}
	}

	/***
	 * @param string $amount
	 * @return int
	 * @throws CospendException
	 */
	private function checkAmount($amount) {
		if ($amount === null || $amount === '' || !is_numeric($amount)) {
			throw new CospendException($this->trans->t('This field is required'));
		}

		return intval($amount);
	}

	/***
	 * @param string $projectId
	 * @param string $payerId
	 * @return int
	 * @throws CospendException
	 */
	private function checkPayerId($projectId, $payerId) {
		if ($payerId === null || $payerId === '' || !is_numeric($payerId)) {
			throw new CospendException($this->trans->t('This field is required.'));
		}

		$member = $this->memberService->getMemberById($projectId, $payerId);

		if ($member === null) {
			throw new CospendException($this->trans->t('Not a valid choice.'));
		}

		return $member->getId();
	}

	/***
	 * @param string $repeat
	 * @return string
	 * @throws CospendException
	 */
	private function checkRepeat($repeat) {
		if ($repeat === null || $repeat === '' || strlen($repeat) !== 1) {
			throw new CospendException($this->trans->t('Invalid value'));
		}

		return $repeat;
	}

	/***
	 * @param int $repeatAllActive
	 * @return int
	 * @throws CospendException
	 */
	private function checkRepeatAllActive(int $repeatAllActive) {
		if ($repeatAllActive === null || ($repeatAllActive !== '' && !is_numeric($repeatAllActive))) {
			throw new CospendException($this->trans->t('Invalid value.'));
		}
		if ($repeatAllActive !== null && $repeatAllActive === '') {
			return 0;
		}

		return intval($repeatAllActive);
	}

	/***
	 * @param string $repeatUntil
	 * @return string|null
	 */
	private function checkRepeatUntil($repeatUntil) {
		if ($repeatUntil !== null && $repeatUntil === '') {
			return null;
		}

		return $repeatUntil;
	}

	/***
	 * @param string $categoryId
	 * @return int
	 * @throws CospendException
	 */
	private function checkCategoryId($categoryId) {
		if ($categoryId === null || !is_numeric($categoryId) || intval($categoryId) === 0) {
			throw new CospendException($this->trans->t('Invalid value.'));
		}

		return intval($categoryId);
	}

	private function checkCategoryAcceptNull($categoryId) {
		if ($categoryId === null || !is_numeric($categoryId) || intval($categoryId) === 0) {
			return null;
		}

		return intval($categoryId);
	}





	/***
	 * @param string $paymentMode
	 * @return string mixed
	 * @throws CospendException
	 */
	private function checkPaymentMode($paymentMode) {
		if ($paymentMode === null || !is_string($paymentMode)) {
			throw new CospendException($this->trans->t('Invalid value.'));
		}
		return $paymentMode;
	}

	/***
	 * @param string|null $paymentMode
	 * @return string|null mixed
	 * @throws CospendException
	 */
	private function checkPaymentModOnN($paymentMode) {
		$mode = $this->checkPaymentMode($paymentMode);
		if ($mode !== 'n') {
			return $mode;
		}

		return null;
}


	/***
	 * @param string|null $lastChanged
	 * @return float|null
	 * @throws CospendException
	 */
	private function checkLastChanged($lastChanged) {
		if($lastChanged === null)
			return null;

		if (!is_numeric($lastChanged)) {
			throw new CospendException("Invalid date.");
		}

		return floatval($lastChanged);
	}

	/***
	 * @param string|null $date
	 * @return int|null
	 */
	private function checkDate($date) {
		if($date !== null && $date !== '') {
			return strtotime($date);
		}

		return null;
	}

	/***
	 * @param string $projectId
	 * @param string $payedFor
	 * @return array
	 * @throws CospendException
	 */
	private function checkOwers($projectId, $payedFor) {
		$owerIds = explode(',', $payedFor);
		if ($payedFor === null || $payedFor === '' || count($owerIds) === 0) {
			throw new CospendException($this->trans->t('Invalid value.'));
		}
		foreach ($owerIds as $owerId) {
			if (!is_numeric($owerId)) {
				throw new CospendException($this->trans->t('Invalid value'));
			}
			if ($this->memberService->getMemberById($projectId, $owerId) === null) {
				throw new CospendException($this->trans->t('Not a valid choice'));
			}
		}

		return $owerIds;
	}

	/**
	 * @param string $projectId
	 * @param string|null $dateMin
	 * @param string|null $dateMax
	 * @param string|null $paymentMode
	 * @param string|null $categoryId
	 * @param string|null $amountMin
	 * @param string|null $amountMax
	 * @param string|null $lastChanged
	 * @return Bill[]
	 * @throws CospendException
	 */
	public function getBills($projectId, $dateMin=null, $dateMax=null, $paymentMode=null, $categoryId=null,
							 $amountMin=null, $amountMax=null, $lastChanged=null) {

		// first get all bill ids
		$bills = $this->billMapper->find(
			$projectId,
			$this->checkDate($dateMin),
			$this->checkDate($dateMax),
			$this->checkPaymentModOnN($paymentMode),
			$this->checkCategoryAcceptNull($categoryId),
			$this->checkAmount($amountMin),
			$this->checkAmount($amountMax),
			$this->checkLastChanged($lastChanged)
			);

		// get bill owers
		foreach ($bills as $bill) {
			$bill->owers = $this->billOwerMapper->list($bill->getId());
		}

		return $bills;
	}

	/**
	 * @param string $projectId
	 * @return array
	 * @throws CospendException
	 */
	public function getAllBillIds($projectId) {

		return array_column($this->getBills($projectId), 'id');
	}

	/**
	 * @param string $projectId
	 * @param string $billId
	 * @return Member[]
	 * @throws CospendException
	 */
	public function findBillOwersByProject($projectId, $billId) {
		$bill = $this->get($projectId, $billId);
		return $this->billOwerMapper->list(($bill->getId()));
	}

	/**
	 * @param string $projectId
	 * @param $date
	 * @param $what
	 * @param $payerId
	 * @param $payedFor
	 * @param $amount
	 * @param $repeat
	 * @param null $paymentMode
	 * @param int|null $categoryId
	 * @param int $repeatAllActive
	 * @param null $repeatUntil
	 * @param null $timestamp
	 * @return Bill
	 * @throws CospendException
	 */
	public function add($projectId, $date, $what, $payerId, $payedFor,
						$amount, $repeat, $paymentMode=null, $categoryId=null,
						$repeatAllActive=0, $repeatUntil=null, $timestamp=null) {

		// check owers
		$owerIds = $this->checkOwers($projectId, $payedFor);

		try {
			$bill = $this->billMapper->add(
				$projectId,
				$this->checkWhat($what),
				$this->checkTimestamp($timestamp, $date),
				$this->checkAmount($amount),
				$this->checkPayerId($projectId, $payerId),
				$this->checkRepeat($repeat),
				$this->checkRepeatAllActive($repeatAllActive),
				$this->checkRepeatUntil($repeatUntil),
				$this->checkCategoryId($categoryId),
				$this->checkPaymentMode($paymentMode),
				(new DateTime())->getTimestamp());

			// insert bill owers
			foreach ($owerIds as $owerId) {
				$this->billOwerMapper->add($bill->getId(), $owerId);
			}

			return $bill;
		} catch (DoesNotExistException $e) {
			throw new CospendException("Add bill failed.", $e);
		}
	}

	/**
	 * @param string $projectId
	 * @param string $billId
	 * @return Bill
	 * @throws CospendException
	 */
	public function get($projectId, $billId) {
		try {
			$bill = $this->billMapper->get(
				$this->checkId($billId));

			if ($bill->getProjectid() !== $projectId)
				throw new CospendException($this->trans->t('This project have no such bill.'));

			return $bill;
		} catch (DoesNotExistException $e) {
			throw new CospendException("Get bill failed.", $e);
		}
	}

	/**
	 * @param string $projectId
	 * @param string $billId
	 * @param string $date
	 * @param string $what
	 * @param string $payerId
	 * @param string $payedFor
	 * @param string $amount
	 * @param string $repeat
	 * @param string|null $paymentMode
	 * @param string|null $categoryId
	 * @param string|null $repeatAllActive
	 * @param string|null $repeatUntil
	 * @param string|null $timestamp
	 * @return bool
	 * @throws CospendException
	 */
	public function editBill($projectId, $billId, $date, $what, $payerId, $payedFor,
							 $amount, $repeat, $paymentMode=null, $categoryId=null,
							 $repeatAllActive=null, $repeatUntil=null, $timestamp=null) {

		$billToEdit = $this->get($projectId, $billId);

		// check owers
		$owerIds = $this->checkOwers($projectId, $payedFor);

		if(!$this->billMapper->updateById(
			$billToEdit->getId(),
			$this->checkWhat($what),
			$this->checkTimestamp($timestamp, $date),
			$this->checkAmount($amount),
			$this->checkPayerId($projectId, $payerId),
			$this->checkRepeat($repeat),
			$this->checkRepeatAllActive($repeatAllActive),
			$this->checkRepeatUntil($repeatUntil),
			$this->checkCategoryId($categoryId),
			$this->checkPaymentMode($paymentMode),
			(new DateTime())->getTimestamp()))
		{
			return false;
		}

		// edit the bill owers
		if (count($owerIds) > 0) {
			// delete old bill owers
			$this->billOwerMapper->deleteByBill($billToEdit->getId());

			// insert bill owers
			foreach ($owerIds as $owerId) {
				try {
					$this->billOwerMapper->add($billToEdit->getId(), $owerId);
				} catch (DoesNotExistException $e) {
					throw new CospendException("Insert billOwer failed.", $e);
				}
			}
		}

		return true;
	}

	/**
	 * @param string $projectId
	 * @param string $billId
	 * @return bool
	 * @throws CospendException
	 */
	public function deleteById($projectId, $billId) {
		$billToDelete = $this->get($projectId, $billId);

		$this->deleteOwers($projectId, $billId);

		return $this->billMapper->deleteById($billToDelete->getId());
	}

	/**
	 * @param string $projectId
	 * @param string $billId
	 * @return int affected billOwers
	 * @throws CospendException
	 */
	public function deleteOwers($projectId, $billId) {
		$billToDeleteFrom = $this->get($projectId, $billId);
		return $this->billOwerMapper->deleteByBill($billToDeleteFrom->getId());
	}

}
