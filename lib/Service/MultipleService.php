<?php

/**
 * Nextcloud - cospend
 *
 * This file is licensed under the Affero General Public License version 3 or
 * later. See the COPYING file.
 *
 * @author Julien Veyssier
 * @copyright Julien Veyssier 2019
 */

namespace OCA\Cospend\Service;

use DateInterval;
use DateTime;
use DateTimeImmutable;
use Exception;
use OC;
use OCA\Cospend\Db\BillOwerMapper;
use OCA\Cospend\Db\CategoryMapper;
use OCA\Cospend\Db\CurrencyMapper;
use OCA\Cospend\Exception\CospendException;
use OCP\AppFramework\Db\DoesNotExistException;
use OCP\Files\File;
use OCP\Files\FileInfo;
use OCP\Files\InvalidPathException;
use OCP\Files\Node;
use OCP\Files\NotFoundException;
use OCP\Files\NotPermittedException;
use OCP\IL10N;
use OCP\IConfig;
use OCP\ILogger;
use OCP\DB\QueryBuilder\IQueryBuilder;

use OCP\IGroupManager;
use OCP\IAvatarManager;

use OCP\IUserManager;
use OCP\Lock\LockedException;
use OCP\Share\IManager;

use OCA\Cospend\Activity\ActivityManager;
use function str_replace;

define('CAT_GROCERY', -1);
define('CAT_BAR', -2);
define('CAT_RENT', -3);
define('CAT_BILL', -4);
define('CAT_CULTURE', -5);
define('CAT_HEALTH', -6);
define('CAT_SHOPPING', -10);
define('CAT_REIMBURSEMENT', -11);
define('CAT_RESTAURANT', -12);
define('CAT_ACCOMODATION', -13);
define('CAT_TRANSPORT', -14);
define('CAT_SPORT', -15);

/**
 * @param $string
 * @param $test
 * @return bool
 */
function endswith($string, $test) {
    $strlen = strlen($string);
    $testlen = strlen($test);
    if ($testlen > $strlen) return false;
    return substr_compare($string, $test, $strlen - $testlen, $testlen) === 0;
}

class MultipleService {

    private $logger;
    private $config;
    private $qb;
    private $dbconnection;
    private $trans;
    private $projectService;
	private $categoryMapper;
    private $billService;
    private $memberService;
    private $billOwerMapper;
    private $currencyMapper;
    private $activityManager;
    private $avatarManager;
    private $groupManager;
    private $userManager;
    private $shareManager;
    private $categoryNames;

    public function __construct (ILogger $logger,
                                IL10N $l10n,
                                IConfig $config,
                                ProjectService $projectService,
								CategoryMapper $categoryMapper,
                                BillService $billService,
								MemberService $memberService,
								BillOwerMapper $billOwerMapper,
								CurrencyMapper $currencyMapper,
                                ActivityManager $activityManager,
                                IAvatarManager $avatarManager,
                                IManager $shareManager,
                                IUserManager $userManager,
                                IGroupManager $groupManager) {
        $this->trans = $l10n;
        $this->config = $config;
        $this->logger = $logger;
        $this->qb = OC::$server->getDatabaseConnection()->getQueryBuilder();
        $this->dbconnection = OC::$server->getDatabaseConnection();
        $this->projectService = $projectService;
		$this->categoryMapper = $categoryMapper;
        $this->billService = $billService;
        $this->memberService = $memberService;
		$this->billOwerMapper = $billOwerMapper;
		$this->currencyMapper = $currencyMapper;
        $this->activityManager = $activityManager;
        $this->avatarManager = $avatarManager;
        $this->groupManager = $groupManager;
        $this->userManager = $userManager;
        $this->shareManager = $shareManager;

        $this->categoryNames = [
        '-1'=>$this->trans->t('Grocery'),
        '-2'=>$this->trans->t('Bar/Party'),
        '-3'=>$this->trans->t('Rent'),
        '-4'=>$this->trans->t('Bill'),
        '-5'=>$this->trans->t('Excursion/Culture'),
        '-6'=>$this->trans->t('Health'),
        '-10'=>$this->trans->t('Shopping'),
        '-11'=>$this->trans->t('Reimbursement'),
        '-12'=>$this->trans->t('Restaurant'),
        '-13'=>$this->trans->t('Accommodation'),
        '-14'=>$this->trans->t('Transport'),
        '-15'=>$this->trans->t('Sport')
        ];

    }

    /**
     * @param $str
     * @return string
     */
    private function db_quote_escape_string($str){
        return $this->dbconnection->quote($str);
    }

	/**
	 * check if user owns the project
	 * or if the project is shared with the user
	 *
	 * @param $userId
	 * @param string $projectId
	 * @return bool
	 * @throws CospendException
	 */
    public function userCanAccessProject($userId, $projectId) {
        $projectInfo = $this->getProjectInfo($projectId);
        if ($projectInfo !== null) {
            // does the user own the project ?
            if ($projectInfo['userid'] === $userId) {
                return true;
            }
            else {
                $qb = $this->dbconnection->getQueryBuilder();
                // is the project shared with the user ?
                $qb->select('userid', 'projectid')
                    ->from('cospend_shares', 's')
                    ->where(
                        $qb->expr()->eq('type', $qb->createNamedParameter('u', IQueryBuilder::PARAM_STR))
                    )
                    ->andWhere(
                        $qb->expr()->eq('projectid', $qb->createNamedParameter($projectId, IQueryBuilder::PARAM_STR))
                    )
                    ->andWhere(
                        $qb->expr()->eq('userid', $qb->createNamedParameter($userId, IQueryBuilder::PARAM_STR))
                    );
                $req = $qb->execute();
                $dbProjectId = null;
                while ($row = $req->fetch()) {
                    $dbProjectId = $row['projectid'];
                    break;
                }
                $req->closeCursor();
                $qb = $qb->resetQueryParts();

                if ($dbProjectId !== null) {
                    return true;
                }
                else {
                    // if not, is the project shared with a group containing the user?
                    $userO = $this->userManager->get($userId);
                    $accessWithGroup = null;

                    $qb->select('userid')
                        ->from('cospend_shares', 's')
                        ->where(
                            $qb->expr()->eq('type', $qb->createNamedParameter('g', IQueryBuilder::PARAM_STR))
                        )
                        ->andWhere(
                            $qb->expr()->eq('projectid', $qb->createNamedParameter($projectId, IQueryBuilder::PARAM_STR))
                        );
                    $req = $qb->execute();
                    while ($row = $req->fetch()){
                        $groupId = $row['userid'];
                        if ($this->groupManager->groupExists($groupId) && $this->groupManager->get($groupId)->inGroup($userO)) {
                            $accessWithGroup = $groupId;
                            break;
                        }
                    }
                    $req->closeCursor();
                    $qb = $qb->resetQueryParts();

                    if ($accessWithGroup !== null) {
                        return true;
                    }
                    else {
                        // if not, are circles enabled and is the project shared with a circle containing the user?
                        $circlesEnabled = OC::$server->getAppManager()->isEnabledForUser('circles');
                        if ($circlesEnabled) {
                            $dbCircleId = null;

                            $qb->select('userid')
                                ->from('cospend_shares', 's')
                                ->where(
                                    $qb->expr()->eq('type', $qb->createNamedParameter('c', IQueryBuilder::PARAM_STR))
                                )
                                ->andWhere(
                                    $qb->expr()->eq('projectid', $qb->createNamedParameter($projectId, IQueryBuilder::PARAM_STR))
                                );
                            $req = $qb->execute();
                            while ($row = $req->fetch()) {
                                $circleId = $row['userid'];
                                if ($this->isUserInCircle($userId, $circleId)) {
                                    return true;
                                }
                            }
                        }
                        return false;
                    }

                }
            }
        }
        else {
            return false;
        }
    }

	/**
	 * check if user owns the project
	 * or if the project is shared with the user with corresponding permission
	 *
	 * @param $userId
	 * @param string $projectId
	 * @param $permission
	 * @return bool
	 * @throws CospendException
	 */
    public function userHasPermission($userId, $projectId, $permission) {
        $projectInfo = $this->getProjectInfo($projectId);
        if ($projectInfo !== null) {
            // does the user own the project ?
            if ($projectInfo['userid'] === $userId) {
                return true;
            }
            else {
                $qb = $this->dbconnection->getQueryBuilder();
                // is the project shared with the user ?
                $qb->select('userid', 'projectid', 'permissions')
                    ->from('cospend_shares', 's')
                    ->where(
                        $qb->expr()->eq('type', $qb->createNamedParameter('u', IQueryBuilder::PARAM_STR))
                    )
                    ->andWhere(
                        $qb->expr()->eq('projectid', $qb->createNamedParameter($projectId, IQueryBuilder::PARAM_STR))
                    )
                    ->andWhere(
                        $qb->expr()->eq('userid', $qb->createNamedParameter($userId, IQueryBuilder::PARAM_STR))
                    );
                $req = $qb->execute();
                $dbProjectId = null;
                $dbPermissions = null;
                while ($row = $req->fetch()) {
                    $dbProjectId = $row['projectid'];
                    $dbPermissions = $row['permissions'];
                    break;
                }
                $req->closeCursor();
                $qb = $qb->resetQueryParts();

                if ($dbProjectId !== null and strrpos($dbPermissions, $permission) !== false) {
                    return true;
                }
                else {
                    // if not, is the project shared with a group containing the user?
                    $userO = $this->userManager->get($userId);
                    $accessWithGroup = null;

                    $qb->select('userid', 'permissions')
                        ->from('cospend_shares', 's')
                        ->where(
                            $qb->expr()->eq('type', $qb->createNamedParameter('g', IQueryBuilder::PARAM_STR))
                        )
                        ->andWhere(
                            $qb->expr()->eq('projectid', $qb->createNamedParameter($projectId, IQueryBuilder::PARAM_STR))
                        );
                    $req = $qb->execute();
                    while ($row = $req->fetch()){
                        $groupId = $row['userid'];
                        $dbPermissions = $row['permissions'];
                        if ($this->groupManager->groupExists($groupId)
                            and $this->groupManager->get($groupId)->inGroup($userO)
                            and strrpos($dbPermissions, $permission) !== false
                        ) {
                            $accessWithGroup = $groupId;
                            break;
                        }
                    }
                    $req->closeCursor();
                    $qb = $qb->resetQueryParts();

                    if ($accessWithGroup !== null) {
                        return true;
                    }
                    else {
                        // if not, are circles enabled and is the project shared with a circle containing the user
                        // with asked permission?
                        $circlesEnabled = OC::$server->getAppManager()->isEnabledForUser('circles');
                        if ($circlesEnabled) {
                            $dbCircleId = null;

                            $qb->select('userid', 'permissions')
                                ->from('cospend_shares', 's')
                                ->where(
                                    $qb->expr()->eq('type', $qb->createNamedParameter('c', IQueryBuilder::PARAM_STR))
                                )
                                ->andWhere(
                                    $qb->expr()->eq('projectid', $qb->createNamedParameter($projectId, IQueryBuilder::PARAM_STR))
                                );
                            $req = $qb->execute();
                            while ($row = $req->fetch()) {
                                $circleId = $row['userid'];
                                $dbPermissions = $row['permissions'];
                                // got permission
                                if (strrpos($dbPermissions, $permission) !== false) {
                                    if ($this->isUserInCircle($userId, $circleId)) {
                                        return true;
                                    }
                                }
                            }
                        }
                        return false;
                    }
                }
            }
        }
        else {
            return false;
        }
    }

	/**
	 * @param string $projectId
	 * @param $permission
	 * @return bool
	 * @throws CospendException
	 */
    public function guestHasPermission($projectId, $permission) {
        $projectInfo = $this->getProjectInfo($projectId);
        if ($projectInfo !== null) {
            return (strrpos($projectInfo['guestpermissions'], $permission) !== false);
        }
        else {
            return false;
        }
    }

    /**
     * check if user owns the external project
     *
     * @param $userId
     * @param string $projectId
     * @param $ncurl
     * @return bool
     */
    public function userCanAccessExternalProject($userId, $projectId, $ncurl) {
        $qb = $this->dbconnection->getQueryBuilder();
        $qb->select('projectid')
           ->from('cospend_ext_projects', 'ep')
           ->where(
               $qb->expr()->eq('projectid', $qb->createNamedParameter($projectId, IQueryBuilder::PARAM_STR))
           )
           ->andWhere(
               $qb->expr()->eq('ncurl', $qb->createNamedParameter($ncurl, IQueryBuilder::PARAM_STR))
           )
           ->andWhere(
               $qb->expr()->eq('userid', $qb->createNamedParameter($userId, IQueryBuilder::PARAM_STR))
           );
        $req = $qb->execute();
        $dbProjectId = null;
        while ($row = $req->fetch()){
            $dbProjectId = $row['projectid'];
            break;
        }
        $req->closeCursor();
        $qb->resetQueryParts();

        return ($dbProjectId !== null);
    }

    /**
     * @param string $ncurl
     * @param string $id
     * @param string $password
     * @param string $userId
     * @return string|array
     */
    public function addExternalProject($ncurl, $id, $password, $userId) {
        $qb = $this->dbconnection->getQueryBuilder();
        $qb->select('projectid')
           ->from('cospend_ext_projects', 'ep')
           ->where(
               $qb->expr()->eq('projectid', $qb->createNamedParameter($id, IQueryBuilder::PARAM_STR))
           )
           ->andWhere(
               $qb->expr()->eq('ncurl', $qb->createNamedParameter($ncurl, IQueryBuilder::PARAM_STR))
           );
        $req = $qb->execute();
        $dbProjectId = null;
        while ($row = $req->fetch()){
            $dbProjectId = $row['projectid'];
            break;
        }
        $req->closeCursor();
        $qb = $qb->resetQueryParts();
        if ($dbProjectId === null) {
            // check if id is valid
            if (strpos($id, '/') !== false) {
                return ['message'=>$this->trans->t('Invalid project id')];
            }
            $qb->insert('cospend_ext_projects')
                ->values([
                    'userid' => $qb->createNamedParameter($userId, IQueryBuilder::PARAM_STR),
                    'projectid' => $qb->createNamedParameter($id, IQueryBuilder::PARAM_STR),
                    'ncurl' => $qb->createNamedParameter($ncurl, IQueryBuilder::PARAM_STR),
                    'password' => $qb->createNamedParameter($password, IQueryBuilder::PARAM_STR)
                ]);
            $qb->execute();

            return $id;
        }
        else {
            return ['message'=>$this->trans->t('A project with id "%1$s" and url "%2$s" already exists', [$id, $ncurl])];
        }
    }

    /**
     * @param string $projectId
     * @return array|string
     * @throws Exception
     */
    public function autoSettlement($projectId) {
        $transactions = $this->getProjectSettlement($projectId);
        if (!is_array($transactions)) {
            return ['message'=>$this->trans->t('Error when getting project settlement transactions')];
        }

        $members = $this->memberService->getMembers($projectId);
        $memberIdToName = [];
        foreach ($members as $member) {
            $memberIdToName[$member['id']] = $member['name'];
        }

		$ts = (new DateTime())->getTimestamp();

        foreach ($transactions as $transaction) {
            $fromId = $transaction['from'];
            $toId = $transaction['to'];
            $amount = floatval($transaction['amount']);
            $billTitle = $memberIdToName[$fromId].' → '.$memberIdToName[$toId];
            $this->billService->add($projectId, null, $billTitle, $fromId, $toId, $amount, 'n', null, CAT_REIMBURSEMENT, 0, null, $ts);
        }
        return 'OK';
    }

	/**
	 * @param string $projectId
	 * @return array
	 * @throws CospendException
	 */
    public function getProjectSettlement($projectId) {
        $balances = $this->getBalance($projectId);
        return $this->settle($balances);
    }

    /**
     * @param $balances
     * @return array
     */
    private function settle($balances) {
        $debitersCrediters = $this->orderBalance($balances);
        $debiters = $debitersCrediters[0];
        $crediters = $debitersCrediters[1];
        return $this->reduceBalance($crediters, $debiters);
    }

    /**
     * @param $balances
     * @return array
     */
    private function orderBalance($balances) {
        $crediters = [];
        $debiters = [];
        foreach ($balances as $id => $balance) {
            if ($balance > 0.0) {
                array_push($crediters, [$id, $balance]);
            }
            else if ($balance < 0.0) {
                array_push($debiters, [$id, $balance]);
            }
        }

        return [$debiters, $crediters];
    }

    /**
     * @param $crediters
     * @param $debiters
     * @param null $results
     * @return array
     */
    private function reduceBalance($crediters, $debiters, $results=null) {
        if (count($crediters) === 0 or count($debiters) === 0) {
            return $results;
        }

        if ($results === null) {
            $results = [];
        }

        $crediters = $this->sortCreditersDebiters($crediters);
        $debiters = $this->sortCreditersDebiters($debiters, true);

        $deb = array_pop($debiters);
        $debiter = $deb[0];
        $debiterBalance = $deb[1];

        $cred = array_pop($crediters);
        $crediter = $cred[0];
        $crediterBalance = $cred[1];

        if (abs($debiterBalance) > abs($crediterBalance)) {
            $amount = abs($crediterBalance);
        }
        else {
            $amount = abs($debiterBalance);
        }

        $newResults = $results;
        array_push($newResults, ['to'=>$crediter, 'amount'=>$amount, 'from'=>$debiter]);

        $newDebiterBalance = $debiterBalance + $amount;
        if ($newDebiterBalance < 0.0) {
            array_push($debiters, [$debiter, $newDebiterBalance]);
            $debiters = $this->sortCreditersDebiters($debiters, true);
        }

        $newCrediterBalance = $crediterBalance - $amount;
        if ($newCrediterBalance > 0.0) {
            array_push($crediters, [$crediter, $newCrediterBalance]);
            $crediters = $this->sortCreditersDebiters($crediters);
        }

        return $this->reduceBalance($crediters, $debiters, $newResults);
    }

    /**
     * @param $arr
     * @param bool $reverse
     * @return array
     */
    private function sortCreditersDebiters($arr, $reverse=false) {
        $res = [];
        if ($reverse) {
            foreach ($arr as $elem) {
                $i = 0;
                while ($i < count($res) and $elem[1] < $res[$i][1]) {
                    $i++;
                }
                array_splice($res, $i, 0, [$elem]);
            }
        }
        else {
            foreach ($arr as $elem) {
                $i = 0;
                while ($i < count($res) and $elem[1] >= $res[$i][1]) {
                    $i++;
                }
                array_splice($res, $i, 0, [$elem]);
            }
        }
        return $res;
    }

    /**
     * @param string $projectId
     * @param $ncurl
     * @param $password
     * @return string
     */
    public function editExternalProject($projectId, $ncurl, $password) {
        $qb = $this->dbconnection->getQueryBuilder();
        $qb->update('cospend_ext_projects');
        $qb->set('password', $qb->createNamedParameter($password, IQueryBuilder::PARAM_STR));
        $qb->where(
            $qb->expr()->eq('projectid', $qb->createNamedParameter($projectId, IQueryBuilder::PARAM_STR))
        )
        ->andWhere(
            $qb->expr()->eq('ncurl', $qb->createNamedParameter($ncurl, IQueryBuilder::PARAM_STR))
        );
        $qb->execute();
        $qb->resetQueryParts();

        return 'UPDATED';
    }

    /**
     * @param string $projectId
     * @param $ncurl
     * @return string
     */
    public function deleteExternalProject($projectId, $ncurl) {
        $qb = $this->dbconnection->getQueryBuilder();
        $qb->delete('cospend_ext_projects')
            ->where(
                $qb->expr()->eq('projectid', $qb->createNamedParameter($projectId, IQueryBuilder::PARAM_STR))
            )
            ->andWhere(
                $qb->expr()->eq('ncurl', $qb->createNamedParameter($ncurl, IQueryBuilder::PARAM_STR))
            );
        $qb->execute();
        $qb->resetQueryParts();

        return 'DELETED';
    }

	/**
	 * @param string $projectId
	 * @return array
	 * @throws CospendException
	 */
    private function getBalance($projectId) {
        $membersWeight = [];
        $membersBalance = [];

        $members = $this->memberService->getMembers($projectId);
        foreach ($members as $member) {
            $memberId = $member['id'];
            $memberWeight = $member['weight'];
            $membersWeight[$memberId] = $memberWeight;
            $membersBalance[$memberId] = 0.0;
        }

        $bills = $this->billService->getBills($projectId);
        foreach ($bills as $bill) {
            $payerId = $bill['payer_id'];
            $amount = $bill['amount'];
            $owers = $bill['owers'];

            $membersBalance[$payerId] += $amount;

            $nbOwerShares = 0.0;
            foreach ($owers as $ower) {
                $owerWeight = $ower['weight'];
                if ($owerWeight === 0.0) {
                    $owerWeight = 1.0;
                }
                $nbOwerShares += $owerWeight;
            }
            foreach ($owers as $ower) {
                $owerWeight = $ower['weight'];
                if ($owerWeight === 0.0) {
                    $owerWeight = 1.0;
                }
                $owerId = $ower['id'];
                $spent = $amount / $nbOwerShares * $owerWeight;
                $membersBalance[$owerId] -= $spent;
            }
        }

        return $membersBalance;
    }

    /**
     * @param $userId
     * @param $circleId
     * @return bool
	 * @noinspection PhpFullyQualifiedNameUsageInspection
	 * @noinspection PhpUndefinedClassInspection
	 * @noinspection PhpUndefinedNamespaceInspection
	 * @noinspection PhpUndefinedMethodInspection
	 */
    private function isUserInCircle($userId, $circleId) {
        $circleDetails = null;
        try {
            $circleDetails = \OCA\Circles\Api\v1\Circles::detailsCircle($circleId);
        }
        catch (\OCA\Circles\Exceptions\CircleDoesNotExistException $e) {
        }
        if ($circleDetails) {
            // is the circle owner
            if ($circleDetails->getOwner()->getUserId() === $userId) {
                return true;
            }
            else {
                foreach ($circleDetails->getMembers() as $m) {
                    // is member of this circle
                    if ($m->getUserId() === $userId) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * @param string $projectId
     * @return array
     */
    private function getUserShares($projectId) {
        $shares = [];

        $userIdToName = [];
        foreach($this->userManager->search('') as $u) {
            $userIdToName[$u->getUID()] = $u->getDisplayName();
        }

        $qb = $this->dbconnection->getQueryBuilder();
        $qb->select('projectid', 'userid', 'id', 'permissions')
           ->from('cospend_shares', 'sh')
           ->where(
               $qb->expr()->eq('projectid', $qb->createNamedParameter($projectId, IQueryBuilder::PARAM_STR))
           )
           ->andWhere(
               $qb->expr()->eq('type', $qb->createNamedParameter('u', IQueryBuilder::PARAM_STR))
           );
        $req = $qb->execute();
        while ($row = $req->fetch()){
            $dbuserId = $row['userid'];
            $dbId = $row['id'];
            $dbPermissions = $row['permissions'];
            if (array_key_exists($dbuserId, $userIdToName)) {
                array_push($shares, [
                    'userid'=>$dbuserId,
                    'name'=>$userIdToName[$dbuserId],
                    'id'=>$dbId,
                    'permissions'=>$dbPermissions
                ]);
            }
        }
        $req->closeCursor();
        $qb->resetQueryParts();

        return $shares;
    }

    /**
     * @param string $projectId
     * @return array
     */
    private function getGroupShares($projectId) {
        $shares = [];

        $groupIdToName = [];
        foreach($this->groupManager->search('') as $g) {
            $groupIdToName[$g->getGID()] = $g->getDisplayName();
        }

        $qb = $this->dbconnection->getQueryBuilder();
        $qb->select('projectid', 'userid', 'id', 'permissions')
           ->from('cospend_shares', 'sh')
           ->where(
               $qb->expr()->eq('projectid', $qb->createNamedParameter($projectId, IQueryBuilder::PARAM_STR))
           )
           ->andWhere(
               $qb->expr()->eq('type', $qb->createNamedParameter('g', IQueryBuilder::PARAM_STR))
           );
        $req = $qb->execute();
        while ($row = $req->fetch()){
            $dbGroupId = $row['userid'];
            $dbId = $row['id'];
            $dbPermissions = $row['permissions'];
            if (array_key_exists($dbGroupId, $groupIdToName)) {
                array_push($shares, [
                    'groupid'=>$dbGroupId,
                    'name'=>$groupIdToName[$dbGroupId],
                    'id'=>$dbId,
                    'permissions'=>$dbPermissions
                ]);
            }
        }
        $req->closeCursor();
        $qb->resetQueryParts();

        return $shares;
    }

    /**
     * @param string $projectId
     * @return array
	 * @noinspection PhpFullyQualifiedNameUsageInspection
	 * @noinspection PhpUndefinedClassInspection
	 * @noinspection PhpUndefinedMethodInspection
	 * @noinspection PhpUndefinedNamespaceInspection
	 */
    private function getCircleShares($projectId) {
        $shares = [];

        $circlesEnabled = OC::$server->getAppManager()->isEnabledForUser('circles');
        if ($circlesEnabled) {
            $circleIdToName = [];
            $cs = \OCA\Circles\Api\v1\Circles::listCircles(\OCA\Circles\Model\Circle::CIRCLES_ALL, '', 0);
            foreach ($cs as $c) {
                $circleUniqueId = $c->getUniqueId();
                $circleName = $c->getName();
                $circleIdToName[$circleUniqueId] = $circleName;
            }

            $qb = $this->dbconnection->getQueryBuilder();
            $qb->select('projectid', 'userid', 'id', 'permissions')
            ->from('cospend_shares', 'sh')
            ->where(
                $qb->expr()->eq('projectid', $qb->createNamedParameter($projectId, IQueryBuilder::PARAM_STR))
            )
            ->andWhere(
                $qb->expr()->eq('type', $qb->createNamedParameter('c', IQueryBuilder::PARAM_STR))
            );
            $req = $qb->execute();
            while ($row = $req->fetch()){
                $dbCircleId = $row['userid'];
                $dbId = $row['id'];
                $dbPermissions = $row['permissions'];
                if (array_key_exists($dbCircleId, $circleIdToName)) {
                    array_push($shares, [
                        'circleid'=>$dbCircleId,
                        'name'=>$circleIdToName[$dbCircleId],
                        'id'=>$dbId,
                        'permissions'=>$dbPermissions
                    ]);
                }
            }
            $req->closeCursor();
            $qb->resetQueryParts();
        }
        return $shares;
    }

    /**
     * daily check of repeated bills
     *
     * @throws Exception
     */
    public function cronRepeatBills() {
        $result = [];
        $projects = [];
        $now = new DateTime();
        // in case cron job wasn't executed during several days,
        // continue trying to repeat bills as long as there was at least one repeated
        $continue = true;
        while ($continue) {
            $continue = false;
            // get bills whith repetition flag
            $qb = $this->dbconnection->getQueryBuilder();
            $qb->select('id', 'projectid', 'what', 'timestamp', 'amount', 'payerid', 'repeat', 'repeatallactive')
            ->from('cospend_bills', 'b')
            ->where(
                $qb->expr()->neq('repeat', $qb->createNamedParameter('n', IQueryBuilder::PARAM_STR))
            );
            $req = $qb->execute();
            $bills = [];
            while ($row = $req->fetch()){
                $id = $row['id'];
                $what = $row['what'];
                $repeat = $row['repeat'];
                $repeatallactive = $row['repeatallactive'];
                $timestamp = $row['timestamp'];
                $projectId = $row['projectid'];
                array_push($bills, [
                    'id' => $id,
                    'what' => $what,
                    'repeat' => $repeat,
                    'repeatallactive' => $repeatallactive,
                    'projectid' => $projectId
                ]);
            }
            $req->closeCursor();
            $qb->resetQueryParts();

            foreach ($bills as $bill) {
                // Use DateTimeImmutable instead of DateTime so that $billDate->add() returns a
                // new instance instead of modifying $billDate
                $billDate = DateTimeImmutable::createFromFormat('U', $bill['timestamp']);

                $nextDate = null;
                switch($bill['repeat']) {
                case 'd':
                    $nextDate = $billDate->add(new DateInterval('P1D'));
                    break;

                case 'w';
                    $nextDate = $billDate->add(new DateInterval('P7D'));
                    break;

                case 'm':
                    if (intval($billDate->format('m')) === 12) {
                        $nextYear = (int) $billDate->format('Y') + 1;
                        $nextMonth = 1;
                    }
                    else {
                        $nextYear = $billDate->format('Y');
                        $nextMonth = (int) $billDate->format('m') + 1;
                    }

                    // same day of month if possible, otherwise at end of month
                    $nextDate = new DateTime();
                    $nextDate->setDate($nextYear, $nextMonth, 1);
                    if ($billDate->format('d') > $nextDate->format('t')) {
                        $nextDate->setDate($nextYear, $nextMonth, $nextDate->format('t'));
                    }
                    else {
                        $nextDate->setDate($nextYear, $nextMonth, $billDate->format('d'));
                    }
                    break;

                case 'y':
                    $nextYear = (int) $billDate->format('Y') + 1;
                    $nextMonth = $billDate->format('m');

                    // same day of month if possible, otherwise at end of month + same month
                    $nextDate = new DateTime();
                    $nextDate->setDate((int) $billDate->format('Y') + 1, $billDate->format('m'), 1);
                    if ($billDate->format('d') > $nextDate->format('t')) {
                        $nextDate->setDate($nextYear, $nextMonth, $nextDate->format('t'));
                    }
                    else {
                        $nextDate->setDate($nextYear, $nextMonth, $billDate->format('d'));
                    }
                    break;
                }

                // Unknown repeat interval
                if ($nextDate === null) {
                    continue;
                }

                // Repeat if $nextDate is in the past (or today)
                $diff = $now->diff($nextDate);
                if ($nextDate->format('Y-m-d') === $now->format('Y-m-d') || $diff->invert) {
                    $this->repeatBill($bill['projectid'], $bill['id'], $nextDate);
                    if (!array_key_exists($bill['projectid'], $projects)) {
                        $projects[$bill['projectid']] = $this->getProjectInfo($bill['projectid']);
                    }
                    array_push($result, [
                        'date_orig' => $billDate->format('Y-m-d'),
                        'date_repeat' => $nextDate->format('Y-m-d'),
                        'what' => $bill['what'],
                        'project_name' => $projects[$bill['projectid']]['name']
                    ]);
                    $continue = true;
                }
            }
        }
        return $result;
    }

    /**
     * duplicate the bill today and give it the repeat flag
     * remove the repeat flag on original bill
     *
     * @param string $projectId
     * @param $billId
     * @param DateTime $datetime
     * @throws Exception
     */
    private function repeatBill($projectId, $billId, $datetime) {
        $bill = $this->billService->get($projectId, $billId);

        $owerIds = [];
        if ($bill->getRepeatallactive() === 1) {
            $pInfo = $this->getProjectInfo($projectId);
            foreach ($pInfo['active_members'] as $am) {
                array_push($owerIds, $am['id']);
            }
        }
        else {
            foreach ($bill['owers'] as $ower) {
                if ($ower['activated']) {
                    array_push($owerIds, $ower['id']);
                }
            }
        }
        $owerIdsStr = implode(',', $owerIds);

        // if bill should be repeated until...
        if ($bill->getRepeatuntil() !== null && $bill->getRepeatuntil() !== '') {
			/** @var DateTime */ $untilDate = new DateTime($bill->getRepeatuntil());
			/** @var DateTime */ $billDate = DateTimeImmutable::createFromFormat('U', $bill->getTimestamp());
			/** @var DateTime */ $nextDate = new DateTime('now');
            if ($bill->getRepeat() === 'd') {
                $nextDate = $billDate->add(new DateInterval('P1D'));
            }
            else if ($bill->getRepeat() === 'w') {
                $nextDate = $billDate->add(new DateInterval('P7D'));
            }
            else if ($bill->getRepeat() === 'm') {
                if (intval($billDate->format('m')) === 12) {
                    $nextYear = (int) $billDate->format('Y') + 1;
                    $nextMonth = 1;
                }
                else {
                    $nextYear = $billDate->format('Y');
                    $nextMonth = (int) $billDate->format('m') + 1;
                }

                // same day of month if possible, otherwise at end of month
                $nextDate = new DateTime();
                $nextDate->setDate($nextYear, $nextMonth, 1);
                if ($billDate->format('d') > $nextDate->format('t')) {
                    $nextDate->setDate($nextYear, $nextMonth, $nextDate->format('t'));
                }
                else {
                    $nextDate->setDate($nextYear, $nextMonth, $billDate->format('d'));
                }
            }
            else if ($bill->getRepeat() === 'y') {
                $nextYear = (int) $billDate->format('Y') + 1;
                $nextMonth = $billDate->format('m');

                // same day of month if possible, otherwise at end of month + same month
                $nextDate = new DateTime();
                $nextDate->setDate($billDate->format('Y') + 1, $billDate->format('m'), 1);
                if ($billDate->format('d') > $nextDate->format('t')) {
                    $nextDate->setDate($nextYear, $nextMonth, $nextDate->format('t'));
                }
                else {
                    $nextDate->setDate($nextYear, $nextMonth, $billDate->format('d'));
                }
            }
            if ($nextDate >= $untilDate) {
                $bill->setRepeat('n');
            }
        }

		$billObj = $this->billService->add($projectId, null, $bill->getWhat(), $bill->getPayerid(),
                                    $owerIdsStr, $bill->getAmount(), $bill->getRepeat(), $bill->getPaymentmode(),
                                    $bill->getCategoryid(), $bill->getRepeatallactive(), $bill->getRepeatuntil(),
                                    $datetime->getTimestamp());

        $this->activityManager->triggerEvent(
            ActivityManager::COSPEND_OBJECT_BILL, $billObj,
            ActivityManager::SUBJECT_BILL_CREATE,
            []
        );

        // now we can remove repeat flag on original bill
        $this->billService->editBill($projectId, $billId, null, $bill->getWhat(), $bill->getPayerid(), null,
                        $bill->getAmount(), 'n', null, null, 0, null);
    }

    /**
     * @param string $projectId
     * @param $userId
     * @param $fromUserId
     * @return array|int
     * @throws Exception
     */
    public function addUserShare($projectId, $userId, $fromUserId) {
        // check if userId exists
        $userIds = [];
        foreach($this->userManager->search('') as $u) {
            if ($u->getUID() !== $fromUserId) {
                array_push($userIds, $u->getUID());
            }
        }
        if ($userId !== '' and in_array($userId, $userIds)) {
            $qb = $this->dbconnection->getQueryBuilder();
            $projectInfo = $this->getProjectInfo($projectId);
            // check if someone tries to share the project with its owner
            if ($userId !== $projectInfo['userid']) {
                // check if user share exists
                $qb->select('userid', 'projectid')
                    ->from('cospend_shares', 's')
                    ->where(
                        $qb->expr()->eq('type', $qb->createNamedParameter('u', IQueryBuilder::PARAM_STR))
                    )
                    ->andWhere(
                        $qb->expr()->eq('projectid', $qb->createNamedParameter($projectId, IQueryBuilder::PARAM_STR))
                    )
                    ->andWhere(
                        $qb->expr()->eq('userid', $qb->createNamedParameter($userId, IQueryBuilder::PARAM_STR))
                    );
                $req = $qb->execute();
                $dbuserId = null;
                while ($row = $req->fetch()){
                    $dbuserId = $row['userid'];
                    break;
                }
                $req->closeCursor();
                $qb = $qb->resetQueryParts();

                if ($dbuserId === null) {
                    $qb->insert('cospend_shares')
                        ->values([
                            'projectid' => $qb->createNamedParameter($projectId, IQueryBuilder::PARAM_STR),
                            'userid' => $qb->createNamedParameter($userId, IQueryBuilder::PARAM_STR),
                            'type' => $qb->createNamedParameter('u', IQueryBuilder::PARAM_STR)
                        ]);
                    $qb->execute();
                    $qb->resetQueryParts();

                    $insertedShareId = intval($qb->getLastInsertId());
                    $response = $insertedShareId;

                    // activity
                    $projectObj = $this->projectService->getProjectById($projectId);
                    $this->activityManager->triggerEvent(
                        ActivityManager::COSPEND_OBJECT_PROJECT, $projectObj,
                        ActivityManager::SUBJECT_PROJECT_SHARE,
                        ['who'=>$userId, 'type'=>'u']
                    );

                    // SEND NOTIFICATION
                    $manager = OC::$server->getNotificationManager();
                    $notification = $manager->createNotification();

                    $acceptAction = $notification->createAction();
                    $acceptAction->setLabel('accept')
                        ->setLink('/apps/cospend', 'GET');

                    $declineAction = $notification->createAction();
                    $declineAction->setLabel('decline')
                        ->setLink('/apps/cospend', 'GET');

                    $notification->setApp('cospend')
                        ->setUser($userId)
                        ->setDateTime(new DateTime())
                        ->setObject('addusershare', $projectId)
                        ->setSubject('add_user_share', [$fromUserId, $projectInfo['name']])
                        ->addAction($acceptAction)
                        ->addAction($declineAction)
                        ;

                    $manager->notify($notification);

                    return $response;
                }
                else {
                    return ['message'=>$this->trans->t('Already shared with this user')];
                }
            }
            else {
                return ['message'=>$this->trans->t('Impossible to share the project with its owner')];
            }
        }
        else {
            return ['message'=>$this->trans->t('No such user')];
        }
    }

    /**
     * @param string $projectId
     * @param $shId
     * @param $permissions
     * @return array|string
     */
    public function editSharePermissions($projectId, $shId, $permissions) {
        // check if user share exists
        $qb = $this->dbconnection->getQueryBuilder();
        $qb->select('id', 'projectid')
            ->from('cospend_shares', 's')
            ->where(
                $qb->expr()->eq('projectid', $qb->createNamedParameter($projectId, IQueryBuilder::PARAM_STR))
            )
            ->andWhere(
                $qb->expr()->eq('id', $qb->createNamedParameter($shId, IQueryBuilder::PARAM_INT))
            );
        $req = $qb->execute();
        $dbId = null;
        while ($row = $req->fetch()){
            $dbId = $row['id'];
            break;
        }
        $req->closeCursor();
        $qb->resetQueryParts();

        if ($dbId !== null) {
            // set the permissions
            $qb->update('cospend_shares')
                ->set('permissions', $qb->createNamedParameter($permissions, IQueryBuilder::PARAM_STR))
                ->where(
                    $qb->expr()->eq('projectid', $qb->createNamedParameter($projectId, IQueryBuilder::PARAM_STR))
                )
                ->andWhere(
                    $qb->expr()->eq('id', $qb->createNamedParameter($shId, IQueryBuilder::PARAM_INT))
                );
            $qb->execute();
            $qb->resetQueryParts();

            return 'OK';
        }
        else {
            return ['message'=>$this->trans->t('No such share')];
        }
    }

    /**
     * @param string $projectId
     * @param $permissions
     * @return string
     */
    public function editGuestPermissions($projectId, $permissions) {
        // check if project exists
        $qb = $this->dbconnection->getQueryBuilder();

        // set the permissions
        $qb->update('cospend_projects')
            ->set('guestpermissions', $qb->createNamedParameter($permissions, IQueryBuilder::PARAM_STR))
            ->where(
                $qb->expr()->eq('id', $qb->createNamedParameter($projectId, IQueryBuilder::PARAM_STR))
            );
        $qb->execute();
        $qb->resetQueryParts();

        return 'OK';
    }

    /**
     * @param string $projectId
     * @param $shId
     * @param $fromUserId
     * @return array|string
     * @throws Exception
     */
    public function deleteUserShare($projectId, $shId, $fromUserId) {
        // check if user share exists
        $qb = $this->dbconnection->getQueryBuilder();
        $qb->select('id', 'userid', 'projectid')
            ->from('cospend_shares', 's')
            ->where(
                $qb->expr()->eq('type', $qb->createNamedParameter('u', IQueryBuilder::PARAM_STR))
            )
            ->andWhere(
                $qb->expr()->eq('projectid', $qb->createNamedParameter($projectId, IQueryBuilder::PARAM_STR))
            )
            ->andWhere(
                $qb->expr()->eq('id', $qb->createNamedParameter($shId, IQueryBuilder::PARAM_INT))
            );
        $req = $qb->execute();
        $dbId = null;
        $dbuserId = null;
        while ($row = $req->fetch()){
            $dbId = $row['id'];
            $dbuserId = $row['userid'];
            break;
        }
        $req->closeCursor();
        $qb = $qb->resetQueryParts();

        if ($dbId !== null) {
            // delete
            $qb->delete('cospend_shares')
                ->where(
                    $qb->expr()->eq('projectid', $qb->createNamedParameter($projectId, IQueryBuilder::PARAM_STR))
                )
                ->andWhere(
                    $qb->expr()->eq('id', $qb->createNamedParameter($shId, IQueryBuilder::PARAM_INT))
                )
                ->andWhere(
                    $qb->expr()->eq('type', $qb->createNamedParameter('u', IQueryBuilder::PARAM_STR))
                );
            $qb->execute();
            $qb->resetQueryParts();

            $response = 'OK';

            // activity
            $projectObj = $this->projectService->getProjectById($projectId);
            $this->activityManager->triggerEvent(
                ActivityManager::COSPEND_OBJECT_PROJECT, $projectObj,
                ActivityManager::SUBJECT_PROJECT_UNSHARE,
                ['who'=>$dbuserId, 'type'=>'u']
            );

            // SEND NOTIFICATION
            $projectInfo = $this->getProjectInfo($projectId);

            $manager = OC::$server->getNotificationManager();
            $notification = $manager->createNotification();

            $acceptAction = $notification->createAction();
            $acceptAction->setLabel('accept')
                ->setLink('/apps/cospend', 'GET');

            $declineAction = $notification->createAction();
            $declineAction->setLabel('decline')
                ->setLink('/apps/cospend', 'GET');

            $notification->setApp('cospend')
                ->setUser($dbuserId)
                ->setDateTime(new DateTime())
                ->setObject('deleteusershare', $projectId)
                ->setSubject('delete_user_share', [$fromUserId, $projectInfo['name']])
                ->addAction($acceptAction)
                ->addAction($declineAction)
                ;

            $manager->notify($notification);

            return $response;
        }
        else {
            return ['message'=>$this->trans->t('No such share')];
        }
    }

	/**
	 * @param string $projectId
	 * @param int $groupId
	 * @param $fromUserId
	 * @return array|int
	 * @throws CospendException
	 */
    public function addGroupShare($projectId, $groupId, $fromUserId) {
        // check if groupId exists
        $groupIds = [];
        foreach($this->groupManager->search('') as $g) {
            array_push($groupIds, $g->getGID());
        }
        if ($groupId !== '' and in_array($groupId, $groupIds)) {
            $qb = $this->dbconnection->getQueryBuilder();
            // check if user share exists
            $qb->select('userid', 'projectid')
                ->from('cospend_shares', 's')
                ->where(
                    $qb->expr()->eq('type', $qb->createNamedParameter('g', IQueryBuilder::PARAM_STR))
                )
                ->andWhere(
                    $qb->expr()->eq('projectid', $qb->createNamedParameter($projectId, IQueryBuilder::PARAM_STR))
                )
                ->andWhere(
                    $qb->expr()->eq('userid', $qb->createNamedParameter($groupId, IQueryBuilder::PARAM_STR))
                );
            $req = $qb->execute();
            $dbGroupId = null;
            while ($row = $req->fetch()){
                $dbGroupId = $row['userid'];
                break;
            }
            $req->closeCursor();
            $qb = $qb->resetQueryParts();

            if ($dbGroupId === null) {
                $qb->insert('cospend_shares')
                    ->values([
                        'projectid' => $qb->createNamedParameter($projectId, IQueryBuilder::PARAM_STR),
                        'userid' => $qb->createNamedParameter($groupId, IQueryBuilder::PARAM_STR),
                        'type' => $qb->createNamedParameter('g', IQueryBuilder::PARAM_STR)
                    ]);
                $qb->execute();
                $qb = $qb->resetQueryParts();

                $insertedShareId = intval($qb->getLastInsertId());
                $response = $insertedShareId;

                // activity
                $projectObj = $this->projectService->getProjectById($projectId);
                $this->activityManager->triggerEvent(
                    ActivityManager::COSPEND_OBJECT_PROJECT, $projectObj,
                    ActivityManager::SUBJECT_PROJECT_SHARE,
                    ['who'=>$groupId, 'type'=>'g']
                );

                return $response;
            }
            else {
                return ['message'=>$this->trans->t('Already shared with this group')];
            }
        }
        else {
            return ['message'=>$this->trans->t('No such group')];
        }
    }

	/**
	 * @param string $projectId
	 * @param int $shId
	 * @param $fromUserId
	 * @return array|string
	 * @throws CospendException
	 */
    public function deleteGroupShare($projectId, $shId, $fromUserId) {
        // check if group share exists
        $qb = $this->dbconnection->getQueryBuilder();
        $qb->select('userid', 'projectid', 'id')
            ->from('cospend_shares', 's')
            ->where(
                $qb->expr()->eq('type', $qb->createNamedParameter('g', IQueryBuilder::PARAM_STR))
            )
            ->andWhere(
                $qb->expr()->eq('projectid', $qb->createNamedParameter($projectId, IQueryBuilder::PARAM_STR))
            )
            ->andWhere(
                $qb->expr()->eq('id', $qb->createNamedParameter($shId, IQueryBuilder::PARAM_INT))
            );
        $req = $qb->execute();
        $dbGroupId = null;
        $dbId = null;
        while ($row = $req->fetch()){
            $dbGroupId = $row['userid'];
            break;
        }
        $req->closeCursor();
        $qb = $qb->resetQueryParts();

        if ($dbGroupId !== null) {
            // delete
            $qb->delete('cospend_shares')
                ->where(
                    $qb->expr()->eq('projectid', $qb->createNamedParameter($projectId, IQueryBuilder::PARAM_STR))
                )
                ->andWhere(
                    $qb->expr()->eq('id', $qb->createNamedParameter($shId, IQueryBuilder::PARAM_INT))
                )
                ->andWhere(
                    $qb->expr()->eq('type', $qb->createNamedParameter('g', IQueryBuilder::PARAM_STR))
                );
            $qb->execute();
            $qb->resetQueryParts();

            $response = 'OK';

            // activity
            $projectObj = $this->projectService->getProjectById($projectId);
            $this->activityManager->triggerEvent(
                ActivityManager::COSPEND_OBJECT_PROJECT, $projectObj,
                ActivityManager::SUBJECT_PROJECT_UNSHARE,
                ['who'=>$dbGroupId, 'type'=>'g']
            );

            return $response;
        }
        else {
            return ['message'=>$this->trans->t('No such share')];
        }
    }

	/**
	 * @param string $projectId
	 * @param int $circleId
	 * @param $fromUserId
	 * @return array|int
	 * @throws CospendException
	 * @noinspection PhpFullyQualifiedNameUsageInspection
	 * @noinspection PhpUndefinedClassInspection
	 * @noinspection PhpUndefinedNamespaceInspection
	 * @noinspection PhpUndefinedMethodInspection
	 */
    public function addCircleShare($projectId, $circleId, $fromUserId) {
        // check if circleId exists
        $circlesEnabled = OC::$server->getAppManager()->isEnabledForUser('circles');
        if ($circlesEnabled) {
            $cs = \OCA\Circles\Api\v1\Circles::listCircles(\OCA\Circles\Model\Circle::CIRCLES_ALL, '', 0);
            $exists = false;
            foreach ($cs as $c) {
                if ($c->getUniqueId() === $circleId) {
                    if ($c->getType() === \OCA\Circles\Model\Circle::CIRCLES_PERSONAL) {
                        return ['message'=>$this->trans->t('Sharing with personal circles is not supported')];
                    }
                    else {
                        $exists = true;
                    }
                }
            }
            if ($circleId !== '' and $exists) {
                $qb = $this->dbconnection->getQueryBuilder();
                // check if circle share exists
                $qb->select('userid', 'projectid')
                    ->from('cospend_shares', 's')
                    ->where(
                        $qb->expr()->eq('type', $qb->createNamedParameter('c', IQueryBuilder::PARAM_STR))
                    )
                    ->andWhere(
                        $qb->expr()->eq('projectid', $qb->createNamedParameter($projectId, IQueryBuilder::PARAM_STR))
                    )
                    ->andWhere(
                        $qb->expr()->eq('userid', $qb->createNamedParameter($circleId, IQueryBuilder::PARAM_STR))
                    );
                $req = $qb->execute();
                $dbCircleId = null;
                while ($row = $req->fetch()){
                    $dbCircleId = $row['userid'];
                    break;
                }
                $req->closeCursor();
                $qb = $qb->resetQueryParts();

                if ($dbCircleId === null) {
                    $qb->insert('cospend_shares')
                        ->values([
                            'projectid' => $qb->createNamedParameter($projectId, IQueryBuilder::PARAM_STR),
                            'userid' => $qb->createNamedParameter($circleId, IQueryBuilder::PARAM_STR),
                            'type' => $qb->createNamedParameter('c', IQueryBuilder::PARAM_STR)
                        ]);
                    $qb->execute();
                    $qb->resetQueryParts();

                    $insertedShareId = intval($qb->getLastInsertId());
                    $response = $insertedShareId;

                    // activity
                    $projectObj = $this->projectService->getProjectById($projectId);
                    $this->activityManager->triggerEvent(
                        ActivityManager::COSPEND_OBJECT_PROJECT, $projectObj,
                        ActivityManager::SUBJECT_PROJECT_SHARE,
                        ['who'=>$circleId, 'type'=>'c']
                    );

                    return $response;
                }
                else {
                    return ['message'=>$this->trans->t('Already shared with this circle')];
                }
            }
            else {
                return ['message'=>$this->trans->t('No such circle')];
            }
        }
        else {
            return ['message'=>$this->trans->t('Circles app is not enabled')];
        }
    }

	/**
	 * @param string $projectId
	 * @param int $shId
	 * @param $fromUserId
	 * @return array|string
	 * @throws CospendException
	 */
    public function deleteCircleShare($projectId, $shId, $fromUserId) {
        // check if circle share exists
        $qb = $this->dbconnection->getQueryBuilder();
        $qb->select('userid', 'projectid', 'id')
            ->from('cospend_shares', 's')
            ->where(
                $qb->expr()->eq('type', $qb->createNamedParameter('c', IQueryBuilder::PARAM_STR))
            )
            ->andWhere(
                $qb->expr()->eq('projectid', $qb->createNamedParameter($projectId, IQueryBuilder::PARAM_STR))
            )
            ->andWhere(
                $qb->expr()->eq('id', $qb->createNamedParameter($shId, IQueryBuilder::PARAM_INT))
            );
        $req = $qb->execute();
        $dbCircleId = null;
        $dbId = null;
        while ($row = $req->fetch()){
            $dbCircleId = $row['userid'];
            break;
        }
        $req->closeCursor();
        $qb = $qb->resetQueryParts();

        if ($dbCircleId !== null) {
            // delete
            $qb->delete('cospend_shares')
                ->where(
                    $qb->expr()->eq('projectid', $qb->createNamedParameter($projectId, IQueryBuilder::PARAM_STR))
                )
                ->andWhere(
                    $qb->expr()->eq('id', $qb->createNamedParameter($shId, IQueryBuilder::PARAM_INT))
                )
                ->andWhere(
                    $qb->expr()->eq('type', $qb->createNamedParameter('c', IQueryBuilder::PARAM_STR))
                );
            $qb->execute();
            $qb->resetQueryParts();

            $response = 'OK';

            // activity
            $projectObj = $this->projectService->getProjectById($projectId);
            $this->activityManager->triggerEvent(
                ActivityManager::COSPEND_OBJECT_PROJECT, $projectObj,
                ActivityManager::SUBJECT_PROJECT_UNSHARE,
                ['who'=>$dbCircleId, 'type'=>'c']
            );
        }
        else {
            $response = ['message'=>$this->trans->t('No such share')];
        }

        return $response;
    }

	/**
	 * @param string $projectId
	 * @param $userId
	 * @return array
	 * @throws InvalidPathException
	 * @throws NotFoundException
	 * @throws NotPermittedException
	 * @throws CospendException
	 */
    public function exportCsvSettlement($projectId, $userId) {
        // create export directory if needed
        $outPath = $this->config->getUserValue($userId, 'cospend', 'outputDirectory', '/Cospend');
        $userFolder = OC::$server->getUserFolder($userId);
        $msg = $this->createAndCheckExportDirectory($userFolder, $outPath);
        if ($msg !== '') {
            return ['message'=>$msg];
        }
        $folder = $userFolder->get($outPath);

        // create file
        if ($folder->nodeExists($projectId.'-settlement.csv')) {
			/* @var $fileToDelete Node */ $fileToDelete = $folder->get($projectId.'-settlement.csv');
			$fileToDelete->delete();
        }
		/** @var $file Node */ $file = $folder->newFile($projectId.'-settlement.csv');
        $handler = $file->fopen('w');
        fwrite($handler, $this->trans->t('Who pays?').','. $this->trans->t('To whom?').','. $this->trans->t('How much?')."\n");
        $transactions = $this->getProjectSettlement($projectId);

        $members = $this->memberService->getMembers($projectId);
        $memberIdToName = [];
        foreach ($members as $member) {
            $memberIdToName[$member['id']] = $member['name'];
        }

        foreach ($transactions as $transaction) {
            fwrite($handler, '"'.$memberIdToName[$transaction['from']].'","'.$memberIdToName[$transaction['to']].'",'.floatval($transaction['amount'])."\n");
        }

        fclose($handler);
        $file->touch();
        return ['path'=>$outPath.'/'.$projectId.'-settlement.csv'];
    }

    /**
     * @param Node $userFolder
     * @param $outPath
     * @return string
     */
    private function createAndCheckExportDirectory($userFolder, $outPath) {
        if (!$userFolder->nodeExists($outPath)) {
            $userFolder->newFolder($outPath);
        }
        if ($userFolder->nodeExists($outPath)) {
			/** @var $folder Node */ $folder = $userFolder->get($outPath);
            if ($folder->getType() !== FileInfo::TYPE_FOLDER) {
                return $this->trans->t('%1$s is not a folder', [$outPath]);
            }
            else if (!$folder->isCreatable()) {
                return $this->trans->t('%1$s is not writeable', [$outPath]);
            }
            else {
                return '';
            }
        }
        else {
            return $this->trans->t('Impossible to create %1$s', [$outPath]);
        }
    }

	/**
	 * @param string $projectId
	 * @param $userId
	 * @param null $dateMin
	 * @param null $dateMax
	 * @param null $paymentMode
	 * @param null $category
	 * @param null $amountMin
	 * @param null $amountMax
	 * @param string $showDisabled
	 * @param null $currencyId
	 * @return array
	 * @throws DoesNotExistException
	 * @throws InvalidPathException
	 * @throws LockedException
	 * @throws NotFoundException
	 * @throws NotPermittedException
	 * @throws CospendException
	 */
    public function exportCsvStatistics($projectId, $userId, $dateMin=null, $dateMax=null, $paymentMode=null, $category=null,
                                        $amountMin=null, $amountMax=null, $showDisabled='1', $currencyId=null) {
        // create export directory if needed
        $outPath = $this->config->getUserValue($userId, 'cospend', 'outputDirectory', '/Cospend');
        $userFolder = OC::$server->getUserFolder($userId);
        $msg = $this->createAndCheckExportDirectory($userFolder, $outPath);
        if ($msg !== '') {
            return ['message'=>$msg];
        }
		/* @var Node */ $folder = $userFolder->get($outPath);

        // create file
        if ($folder->nodeExists($projectId.'-stats.csv')) {
			/* @var $fileToDelete Node */ $fileToDelete = $folder->get($projectId.'-stats.csv');
			$fileToDelete->delete();
        }
        /**@var $file File */ $file = $folder->newFile($projectId.'-stats.csv');
        $handler = $file->fopen('w');
        fwrite($handler, $this->trans->t('Member name').','. $this->trans->t('Paid').','. $this->trans->t('Spent').','. $this->trans->t('Balance')."\n");
        $allStats = $this->getProjectStatistics($projectId, 'lowername', $dateMin, $dateMax, $paymentMode,
                                                $category, $amountMin, $amountMax, $showDisabled, $currencyId);
        $stats = $allStats['stats'];
        if (!is_array($stats)) {
        }

        foreach ($stats as $stat) {
            fwrite($handler, '"'.$stat['member']['name'].'",'.floatval($stat['paid']).','.floatval($stat['spent']).','.floatval($stat['balance'])."\n");
        }

        fclose($handler);
        $file->touch();
        return ['path'=>$outPath.'/'.$projectId.'-stats.csv'];
    }

	/**
	 * @param string $projectId
	 * @param $name
	 * @param $userId
	 * @return array
	 * @throws NotFoundException
	 * @throws InvalidPathException
	 * @throws NotPermittedException
	 * @throws LockedException
	 * @throws CospendException
	 */
    public function exportCsvProject($projectId, $name, $userId) {
        // create export directory if needed
        $outPath = $this->config->getUserValue($userId, 'cospend', 'outputDirectory', '/Cospend');
        $userFolder = OC::$server->getUserFolder($userId);
        $msg = $this->createAndCheckExportDirectory($userFolder, $outPath);
        if ($msg !== '') {
            return ['message'=>$msg];
        }
        /**@var Node */ $folder = $userFolder->get($outPath);

        $projectInfo = $this->getProjectInfo($projectId);

        // create file
        $filename = $projectId.'.csv';
        if ($name !== null) {
            $filename = $name;
            if (!endswith($filename, '.csv')) {
                $filename .= '.csv';
            }
        }
        if ($folder->nodeExists($filename)) {
            /* @var $fileToDelete Node */ $fileToDelete = $folder->get($filename);
			$fileToDelete->delete();
        }
        /**@var $file File */ $file = $folder->newFile($filename);
        $handler = $file->fopen('w');
        fwrite($handler, "what,amount,date,payer_name,payerWeight,payerActive,owers,repeat,repeatallactive,repeatuntil,categoryid,paymentmode\n");
        $members = $projectInfo['members'];
        $memberIdToName = [];
        $memberIdToWeight = [];
        $memberIdToActive = [];
        foreach ($members as $member) {
            $memberIdToName[$member['id']] = $member['name'];
            $memberIdToWeight[$member['id']] = $member['weight'];
            $memberIdToActive[$member['id']] = intval($member['activated']);
            fwrite($handler, 'deleteMeIfYouWant,1,1970-01-01,"'.$member['name'].'",'.floatval($member['weight']).','.
                              intval($member['activated']).',"'.$member['name'].'",n,,,,'."\n");
        }
        $bills = $this->billService->getBills($projectId);
        foreach ($bills as $bill) {
            $owerNames = [];
            foreach ($bill['owers'] as $ower) {
                array_push($owerNames, $ower['name']);
            }
            $owersTxt = implode(', ', $owerNames);

            $payerId = $bill['payerId'];
            $payerName = $memberIdToName[$payerId];
            $payerWeight = $memberIdToWeight[$payerId];
            $payerActive = $memberIdToActive[$payerId];
            $dateTime = DateTime::createFromFormat('U', $bill['timestamp']);
            $oldDateStr = $dateTime->format('Y-m-d');
            fwrite($handler, '"'.$bill['what'].'",'.floatval($bill['amount']).','.$oldDateStr.','.$bill['timestamp'].
                             ',"'.$payerName.'",'.
                             floatval($payerWeight).','.$payerActive.',"'.$owersTxt.'",'.$bill['repeat'].
                             ','.$bill['repeatallactive'].','.
                             $bill['repeatuntil'].','.$bill['categoryid'].','.$bill['paymentmode']."\n");
        }

        // write categories
        $categories = $projectInfo['categories'];
        if (count($categories) > 0) {
            fwrite($handler, "\n");
            fwrite($handler, "categoryname,categoryid,icon,color\n");
            foreach ($categories as $id=>$cat) {
                fwrite($handler, '"'.$cat['name'].'",'.intval($id).',"'.$cat['icon'].'","'.$cat['color'].'"'."\n");
            }
        }

        // write currencies
        $currencies = $projectInfo['currencies'];
        if (count($currencies) > 0) {
            fwrite($handler, "\n");
            fwrite($handler, "currencyname,exchange_rate\n");
            // main currency
            fwrite($handler, '"'.$projectInfo['currencyname'].'",1'."\n");
            foreach ($currencies as $cur) {
                fwrite($handler, '"'.$cur['name'].'",'.floatval($cur['exchange_rate'])."\n");
            }
        }

        fclose($handler);
        $file->touch();
        return ['path'=>$outPath.'/'.$filename];
    }

    /**
     * @param $path
     * @param $userId
     * @return array|string|string[]
     * @throws NotFoundException
     * @throws Exception;
     */
    public function importCsvProject($path, $userId) {
        $cleanPath = str_replace(array('../', '..\\'), '',  $path);
        $userFolder = OC::$server->getUserFolder($userId);
        if ($userFolder->nodeExists($cleanPath)) {
            $file = $userFolder->get($cleanPath);
            if ($file->getType() === FileInfo::TYPE_FILE) {
                if (($handle = $file->fopen('r')) !== false) {
                    $columns = [];
                    $membersWeight = [];
                    $membersActive = [];
                    $bills = [];
                    $currencies = [];
                    $mainCurrencyName = null;
                    $categories = [];
                    $categoryIdConv = [];
                    $previousLineEmpty = false;
                    $currentSection = null;
                    $row = 0;
                    while (($data = fgetcsv($handle, 1000, ',')) !== false) {
                        if ($data === [null]) {
                            $previousLineEmpty = true;
                        }
                        // determine which section we're entering
                        else if ($row === 0 or $previousLineEmpty) {
                            $previousLineEmpty = false;
                            $nbCol = count($data);
                            $columns = [];
                            for ($c=0; $c < $nbCol; $c++) {
                                $columns[$data[$c]] = $c;
                            }
                            if (array_key_exists('what', $columns) and
                                array_key_exists('amount', $columns) and
                                (array_key_exists('date', $columns) or array_key_exists('timestamp', $columns)) and
                                array_key_exists('payer_name', $columns) and
                                array_key_exists('payer_weight', $columns) and
                                array_key_exists('owers', $columns)
                            ) {
                                $currentSection = 'bills';
                            }
                            else if (array_key_exists('icon', $columns) and
                                     array_key_exists('color', $columns) and
                                     array_key_exists('categoryid', $columns) and
                                     array_key_exists('categoryname', $columns)
                            ) {
                                $currentSection = 'categories';
                            }
                            else if (array_key_exists('exchange_rate', $columns) and
                                     array_key_exists('currencyname', $columns)
                            ) {
                                $currentSection = 'currencies';
                            }
                            else {
                                fclose($handle);
                                return ['message'=>$this->trans->t('Malformed CSV, bad column names at line %1$s', [$row + 1])];
                            }
                        }
                        // normal line : bill or category
                        else {
                            $previousLineEmpty = false;
                            if ($currentSection === 'categories') {
                                $icon = $data[$columns['icon']];
                                $color = $data[$columns['color']];
                                $categoryId = $data[$columns['categoryid']];
                                $categoryname = $data[$columns['categoryname']];
                                array_push($categories, [
                                    'icon'=>$icon,
                                    'color'=>$color,
                                    'id'=>$categoryId,
                                    'name'=>$categoryname
                                ]);
                            }
                            else if ($currentSection === 'currencies') {
                                $name = $data[$columns['currencyname']];
                                $exchange_rate = $data[$columns['exchange_rate']];
                                if (floatval($exchange_rate) === 1.0) {
                                    $mainCurrencyName = $name;
                                }
                                else {
                                    array_push($currencies, [
                                        'name'=>$name,
                                        'exchange_rate'=>$exchange_rate
                                    ]);
                                }
                            }
                            else if ($currentSection === 'bills') {
                                $what = $data[$columns['what']];
                                $amount = $data[$columns['amount']];
                                // priority to timestamp
                                if (array_key_exists('timestamp', $columns)) {
                                    $timestamp = $data[$columns['timestamp']];
                                }
                                else if (array_key_exists('date', $columns)) {
                                    $date = $data[$columns['date']];
                                    $timestamp = strtotime($date);
                                }
                                $payer_name = $data[$columns['payer_name']];
                                $payer_weight = $data[$columns['payer_weight']];
                                $owers = $data[$columns['owers']];
                                $payer_active = array_key_exists('payer_active', $columns) ? $data[$columns['payer_active']] : 1;
                                $repeat = array_key_exists('repeat', $columns) ? $data[$columns['repeat']] : 'n';
                                $categoryId = array_key_exists('categoryid', $columns) ? intval($data[$columns['categoryid']]) : null;
                                $paymentmode = array_key_exists('paymentmode', $columns) ? $data[$columns['paymentmode']] : null;
                                $repeatallactive = array_key_exists('repeatallactive', $columns) ? $data[$columns['repeatallactive']] : 0;
                                $repeatuntil = array_key_exists('repeatuntil', $columns) ? $data[$columns['repeatuntil']] : null;

                                // manage members
                                $membersActive[$payer_name] = intval($payer_active);
                                if (is_numeric($payer_weight)) {
                                    $membersWeight[$payer_name] = floatval($payer_weight);
                                }
                                else {
                                    fclose($handle);
                                    return ['message'=>$this->trans->t('Malformed CSV, bad payer weight on line %1$s', [$row + 1])];
                                }
                                if (strlen($owers) === 0) {
                                    fclose($handle);
                                    return ['message'=>$this->trans->t('Malformed CSV, bad owers on line %1$s', [$row + 1])];
                                }
                                if ($payer_name !== $owers) {
                                    $owersArray = explode(', ', $owers);
                                    foreach ($owersArray as $ower) {
                                        if (strlen($ower) === 0) {
                                            fclose($handle);
                                            return ['message'=>$this->trans->t('Malformed CSV, bad owers on line %1$s', [$row + 1])];
                                        }
                                        if (!array_key_exists($ower, $membersWeight)) {
                                            $membersWeight[$ower] = 1.0;
                                        }
                                    }
                                    if (!is_numeric($amount)) {
                                        fclose($handle);
                                        return ['message'=>$this->trans->t('Malformed CSV, bad amount on line %1$s', [$row + 1])];
                                    }
                                    array_push($bills, [
                                        'what'=>$what,
                                        'timestamp'=>$timestamp,
                                        'amount'=>$amount,
                                        'payer_name'=>$payer_name,
                                        'owers'=>$owersArray,
                                        'paymentmode'=>$paymentmode,
                                        'categoryid'=>$categoryId,
                                        'repeat'=>$repeat,
                                        'repeatuntil'=>$repeatuntil,
                                        'repeatallactive'=>$repeatallactive
                                    ]);
                                }
                            }
                        }
                        $row++;
                    }
                    fclose($handle);

                    $memberNameToId = [];

                    // add project
                    $user = $this->userManager->get($userId);
                    $userEmail = $user->getEMailAddress();
                    $projectId = str_replace('.csv', '', $file->getName());
                    $projectName = $projectId;
                    $projResult = $this->projectService->createProject($projectName, $projectId, '', $userEmail, $userId);
                    if (!is_string($projResult)) {
                        return ['message'=>$this->trans->t('Error in project creation, %1$s', [$projResult['message']])];
                    }
                    // set project main currency
                    if ($mainCurrencyName !== null) {
                        $this->projectService->editProject($projectId, $projectName, null, null, null, $mainCurrencyName);
                    }
                    // add categories
                    foreach ($categories as $cat) {
                        $insertedCat = $this->categoryMapper->add($projectId, $cat['name'], $cat['icon'], $cat['color']);
                        if (!is_null($insertedCat)) {
                            $this->projectService->deleteProject($projectId);
                            return ['message'=>$this->trans->t('Error when adding category %1$s', [$cat['name']])];
                        }
                        $categoryIdConv[$cat['id']] = $insertedCat->id;
                    }
                    // add currencies
                    foreach ($currencies as $cur) {
                        $insertedCur = $this->currencyMapper->add($projectId, $cur['name'], floatval($cur['exchange_rate']));
                        if (!is_null($insertedCur)) {
                            $this->projectService->deleteProject($projectId);
                            return ['message'=>$this->trans->t('Error when adding currency %1$s', [$cur['name']])];
                        }
                    }
                    // add members
                    foreach ($membersWeight as $memberName => $weight) {
                        $insertedMember = $this->memberService->addMember($projectId, $memberName, $weight, $membersActive[$memberName]);
                        if (is_null($insertedMember)) { // TODO is never null
                            $this->projectService->deleteProject($projectId);
                            return ['message'=>$this->trans->t('Error when adding member %1$s', [$memberName])];
                        }
                        $memberNameToId[$memberName] = $insertedMember['id'];
                    }
                    // add bills
                    foreach ($bills as $bill) {
                        // manage category id if this is a custom category
                        $catId = $bill['categoryid'];
                        if (is_numeric($catId) and intval($catId) > 0) {
                            $catId = $categoryIdConv[$catId];
                        }
                        $payerId = $memberNameToId[$bill['payer_name']];
                        $owerIds = [];
                        foreach ($bill['owers'] as $owerName) {
                            array_push($owerIds, $memberNameToId[$owerName]);
                        }
                        $owerIdsStr = implode(',', $owerIds);
                        $addBillResult = $this->billService->add($projectId, null, $bill['what'], $payerId,
                                                        $owerIdsStr, $bill['amount'], $bill['repeat'],
                                                        $bill['paymentmode'],
                                                        $catId, $bill['repeatallactive'],
                                                        $bill['repeatuntil'], $bill['timestamp']);
                        if (!is_numeric($addBillResult)) { // TODO never null
                            $this->projectService->deleteProject($projectId);
                            return ['message'=>$this->trans->t('Error when adding bill %1$s', [$bill['what']])];
                        }
                    }

                    return $projectId;
                }
                else {
                    return ['message'=>$this->trans->t('Access denied')];
                }
            }
            else {
                return ['message'=>$this->trans->t('Access denied')];
            }
        }
        else {
            return ['message'=>$this->trans->t('Access denied')];
        }
    }

    /**
     * @NoAdminRequired
     * @param $path
     * @param $userId
     * @return array|string|string[]
     * @throws NotFoundException
     * @throws Exception;
     */
    public function importSWProject($path, $userId) {
        $cleanPath = str_replace(array('../', '..\\'), '',  $path);
        $userFolder = OC::$server->getUserFolder();
        if ($userFolder->nodeExists($cleanPath)) {
            $file = $userFolder->get($cleanPath);
            if ($file->getType() === FileInfo::TYPE_FILE) {
                if (($handle = $file->fopen('r')) !== false) {
                    $columns = [];
                    $membersWeight = [];
                    $bills = [];
                    $owersArray = [];
                    $row = 0;
                    while (($data = fgetcsv($handle, 1000, ',')) !== false) {
                        $owersList = [];
                        $payer_name = '';
                        // first line : get column order
                        if ($row === 0) {
                            $nbCol = count($data);
                            for ($c=0; $c < $nbCol; $c++) {
                                $columns[$data[$c]] = $c;
                            }
                            if (!array_key_exists('Date', $columns) or
                                !array_key_exists('Description', $columns) or
                                !array_key_exists('Category', $columns) or
                                !array_key_exists('Cost', $columns) or
                                !array_key_exists('Currency', $columns)
                            ) {
                                fclose($handle);
                                return ['message'=>$this->trans->t('Malformed CSV, bad column names')];
                            }
                            // manage members
                            $m=0;
                            for ($c=5; $c < $nbCol; $c++){
                                $owersArray[$m] = $data[$c];
                                $m++;
                            }
                            foreach ($owersArray as $ower) {
                                if (strlen($ower) === 0) {
                                    fclose($handle);
                                    return ['message'=>$this->trans->t('Malformed CSV, cannot have an empty ower')];
                                }
                                if (!array_key_exists($ower, $membersWeight)) {
                                    $membersWeight[$ower] = 1.0;
                                }
                            }
                        } /** @noinspection PhpStatementHasEmptyBodyInspection */
						elseif (!isset($data[$columns['Date']]) || empty($data[$columns['Date']])) {
                            // skip empty lines
                        } /** @noinspection PhpStatementHasEmptyBodyInspection */
                        elseif (isset($data[$columns['Description']]) && $data[$columns['Description']] === 'Total balance') {
                            // skip the total lines
                        }
                        // normal line : bill
                        else {
                            $what = $data[$columns['Description']];
                            $amount = $data[$columns['Cost']];
                            $date = $data[$columns['Date']];
                            $timestamp = strtotime($date);
                            $l = 0;
                            $nbCol = count($data);
                            for ($c=5; $c < $nbCol; $c++){
                                if (max($data[$c], 0) !== 0){
                                    $payer_name = $owersArray[$c-5];
                                }
                                if ($data[$c] === $amount){
                                    continue;
                                } elseif ($data[$c] === -$amount){
                                    $owersList = [];
                                    $owersList[$l+1] = $owersArray[$c-5];
                                    break;
                                } else {
                                    $owersList[$l+1] = $owersArray[$c-5];
                                }
                            }
                            if (!isset($payer_name) || empty($payer_name)) {
                                return ['message'=>$this->trans->t('Malformed CSV, no payer on line %1$s', [$row])];
                            }

                            if (!is_numeric($amount)) {
                                fclose($handle);
                                return ['message'=>$this->trans->t('Malformed CSV, bad amount on line %1$s', [$row])];
                            }
                            array_push($bills,
                                [
                                    'what'=>$what,
                                    'timestamp'=>$timestamp,
                                    'amount'=>$amount,
                                    'payer_name'=>$payer_name,
                                    'owers'=>$owersList,
                                ]
                            );
                        }
                        $row++;
                    }
                    fclose($handle);

                    $memberNameToId = [];

                    // add project
                    $user = $this->userManager->get($userId);
                    $userEmail = $user->getEMailAddress();
                    $projectId = str_replace('.csv', '', $file->getName());
                    $projectName = $projectId;
                    $projResult = $this->projectService->createProject($projectName, $projectId, '', $userEmail, $userId);
                    if (!is_string($projResult)) {
                        return ['message'=>$this->trans->t('Error in project creation, %1$s', [$projResult['message']])];
                    }
                    // add members
                    foreach ($membersWeight as $memberName => $weight) {
                        $insertedMember = $this->memberService->addMember($projectId, $memberName, $weight);
                        if (!is_array($insertedMember)) {
                            $this->projectService->deleteProject($projectId);
                            return ['message'=>$this->trans->t('Error when adding member %1$s', [$memberName])];
                        }
                        $memberNameToId[$memberName] = $insertedMember['id'];
                    }
                    // add bills
                    foreach ($bills as $bill) {
                        $payerId = $memberNameToId[$bill['payer_name']];
                        $owerIds = [];
                        foreach ($bill['owers'] as $owerName) {
                            array_push($owerIds, $memberNameToId[$owerName]);
                        }
                        $owerIdsStr = implode(',', $owerIds);
                        $addBillResult = $this->billService->add($projectId, null, $bill['what'], $payerId, $owerIdsStr, $bill['amount'], 'n',
                                                        null, null, 0, null, $timestamp);
                        if (!is_numeric($addBillResult)) {
                            $this->projectService->deleteProject($projectId);
                            return ['message'=>$this->trans->t('Error when adding bill %1$s', [$bill['what']])];
                        }
                    }
                    return $projectId;
                }
                else {
                    return ['message'=>$this->trans->t('Access denied')];
                }
            }
            else {
                return ['message'=>$this->trans->t('Access denied')];
            }
        }
        else {
            return ['message'=>$this->trans->t('Access denied')];
        }
    }

    /**
     * auto export
     * triggered by NC cron job
     *
     * export projects
     *
     * @throws Exception
     */
    public function cronAutoExport() {
        date_default_timezone_set('UTC');
        // last day
        $now = new DateTime();
        $y = $now->format('Y');
        $m = $now->format('m');
        $d = $now->format('d');

        // get begining of today
        $dateMaxDay = new DateTime($y.'-'.$m.'-'.$d);
        $maxDayTimestamp = $dateMaxDay->getTimestamp();
        $minDayTimestamp = $maxDayTimestamp - 24*60*60;

        $dateMaxDay->modify('-1 day');
        $dailySuffix = '_'.$this->trans->t('daily').'_'.$dateMaxDay->format('Y-m-d');

        // last week
        $now = new DateTime();
        while (intval($now->format('N')) !== 1) {
            $now->modify('-1 day');
        }
        $y = $now->format('Y');
        $m = $now->format('m');
        $d = $now->format('d');
        $dateWeekMax = new DateTime($y.'-'.$m.'-'.$d);
        $maxWeekTimestamp = $dateWeekMax->getTimestamp();
        $minWeekTimestamp = $maxWeekTimestamp - 7*24*60*60;
        $dateWeekMin = new DateTime($y.'-'.$m.'-'.$d);
        $dateWeekMin->modify('-7 day');
        $weeklySuffix = '_'.$this->trans->t('weekly').'_'.$dateWeekMin->format('Y-m-d');

        // last month
        $now = new DateTime();
        while (intval($now->format('d')) !== 1) {
            $now->modify('-1 day');
        }
        $y = $now->format('Y');
        $m = $now->format('m');
        $d = $now->format('d');
        $dateMonthMax = new DateTime($y.'-'.$m.'-'.$d);
        $maxMonthTimestamp = $dateMonthMax->getTimestamp();
        $now->modify('-1 day');
        while (intval($now->format('d')) !== 1) {
            $now->modify('-1 day');
        }
        $y = intval($now->format('Y'));
        $m = intval($now->format('m'));
        $d = intval($now->format('d'));
        $dateMonthMin = new DateTime($y.'-'.$m.'-'.$d);
        $minMonthTimestamp = $dateMonthMin->getTimestamp();
        $monthlySuffix = '_'.$this->trans->t('monthly').'_'.$dateMonthMin->format('Y-m');

        $weekFilterArray = array();
        $weekFilterArray['tsmin'] = $minWeekTimestamp;
        $weekFilterArray['tsmax'] = $maxWeekTimestamp;
        $dayFilterArray = array();
        $dayFilterArray['tsmin'] = $minDayTimestamp;
        $dayFilterArray['tsmax'] = $maxDayTimestamp;
        $monthFilterArray = array();
        $monthFilterArray['tsmin'] = $minMonthTimestamp;
        $monthFilterArray['tsmax'] = $maxMonthTimestamp;

        $qb = $this->dbconnection->getQueryBuilder();

        foreach ($this->userManager->search('') as $u) {
            $uid = $u->getUID();
            $outPath = $this->config->getUserValue($uid, 'cospend', 'outputDirectory', '/Cospend');

            $qb->select('p.id', 'p.name', 'p.autoexport')
            ->from('cospend_projects', 'p')
            ->where(
                $qb->expr()->eq('userid', $qb->createNamedParameter($uid, IQueryBuilder::PARAM_STR))
            )
            ->andWhere(
                $qb->expr()->neq('p.autoexport', $qb->createNamedParameter('n', IQueryBuilder::PARAM_STR))
            );
            $req = $qb->execute();

            $dbProjectId = null;
            $dbPassword = null;
            while ($row = $req->fetch()) {
                $dbProjectId = $row['id'];
                $autoexport = $row['autoexport'];

                $suffix = $dailySuffix;
                if ($autoexport === 'w') {
                    $suffix = $weeklySuffix;
                }
                else if ($autoexport === 'm') {
                    $suffix = $monthlySuffix;
                }
                // check if file already exists
                $exportName = $dbProjectId.$suffix.'.csv';

                $userFolder = OC::$server->getUserFolder($uid);
                if (! $userFolder->nodeExists($outPath.'/'.$exportName)) {
                    $this->exportCsvProject($dbProjectId, $exportName, $uid);
                }
            }
            $req->closeCursor();
            $qb = $qb->resetQueryParts();
        }
    }

	/**
	 * @param string $projectId
	 * @return array|null
	 * @throws CospendException
	 */
	public function getProjectInfo($projectId) {
		$projectInfo = null;

		$qb = $this->dbconnection->getQueryBuilder();

		$qb->select('id', 'password', 'name', 'email', 'userid', 'lastchanged', 'guestpermissions', 'autoexport', 'currencyname')
			->from('cospend_projects', 'p')
			->where(
				$qb->expr()->eq('id', $qb->createNamedParameter($projectId, IQueryBuilder::PARAM_STR))
			);
		$req = $qb->execute();

		$dbProjectId = null;
		$dbPassword = null;
		while ($row = $req->fetch()){
			$dbProjectId = $row['id'];
			$dbName = $row['name'];
			$dbEmail= $row['email'];
			$dbUserId = $row['userid'];
			$dbGuestPermissions = $row['guestpermissions'];
			$dbLastchanged = intval($row['lastchanged']);
			$dbAutoexport= $row['autoexport'];
			$dbCurrencyName= $row['currencyname'];
			break;
		}
		$req->closeCursor();
		$qb->resetQueryParts();
		if ($dbProjectId !== null) {
			$members = $this->memberService->getMembers($dbProjectId);
			$activeMembers = [];
			foreach ($members as $member) {
				if ($member['activated']) {
					array_push($activeMembers, $member);
				}
			}
			$balance = $this->getBalance($dbProjectId);
			$currencies = $this->currencyMapper->list($dbProjectId);
			$categories = $this->categoryMapper->list($dbProjectId);
			$projectInfo = [
				'userid'=>$dbUserId,
				'name'=>$dbName,
				'contact_email'=>$dbEmail,
				'id'=>$dbProjectId,
				'guestpermissions'=>$dbGuestPermissions,
				'autoexport'=>$dbAutoexport,
				'currencyname'=>$dbCurrencyName,
				'currencies'=>$currencies,
				'categories'=>$categories,
				'active_members'=>$activeMembers,
				'members'=>$members,
				'balance'=>$balance,
				'lastchanged'=>$dbLastchanged
			];
		}

		return $projectInfo;
	}

	/**
	 * @param string $projectId
	 * @param null $memberOrder
	 * @param null $dateMin
	 * @param null $dateMax
	 * @param null $paymentMode
	 * @param null $category
	 * @param null $amountMin
	 * @param null $amountMax
	 * @param string $showDisabled
	 * @param null $currencyId
	 * @return array
	 * @throws DoesNotExistException
	 * @throws CospendException
	 */
	public function getProjectStatistics($projectId, $memberOrder=null, $dateMin=null, $dateMax=null,
										 $paymentMode=null, $category=null, $amountMin=null, $amountMax=null,
										 $showDisabled='1', $currencyId=null) {
		$membersWeight = [];
		$membersNbBills = [];
		$membersBalance = [];
		$membersFilteredBalance = [];
		$membersPaid = [];
		$membersSpent = [];

		$showDisabled = ($showDisabled === '1');

		$currency = null;
		if ($currencyId !== null and intval($currencyId) !== 0) {
			$currency = $this->currencyMapper->get($currencyId);
		}

		$projectCategories = $this->categoryMapper->list($projectId);

		// get the real global balances with no filters
		$balances = $this->getBalance($projectId);

		$members = $this->memberService->getMembers($projectId, $memberOrder);
		foreach ($members as $member) {
			$memberId = $member['id'];
			$memberWeight = $member['weight'];
			$membersWeight[$memberId] = $memberWeight;
			$membersNbBills[$memberId] = 0;
			$membersBalance[$memberId] = $balances[$memberId];
			$membersFilteredBalance[$memberId] = 0.0;
			$membersPaid[$memberId] = 0.0;
			$membersSpent[$memberId] = 0.0;
		}

		// build list of members to display
		$membersToDisplay = [];
		foreach ($members as $member) {
			$memberId = $member['id'];
			// only take enabled members or those with non-zero balance
			$mBalance = floatval($membersBalance[$memberId]);
			if ($showDisabled or $member['activated'] or $mBalance >= 0.01 or $mBalance <= -0.01) {
				$membersToDisplay[$memberId] = $member;
			}
		}

		// compute stats
		$bills = $this->billService->getBills($projectId, $dateMin, $dateMax, $paymentMode, $category, $amountMin, $amountMax);
		// compute classic stats
		foreach ($bills as $bill) {
			$payerId = $bill['payer_id'];
			$amount = $bill['amount'];
			$owers = $bill['owers'];

			$membersNbBills[$payerId]++;
			$membersFilteredBalance[$payerId] += $amount;
			$membersPaid[$payerId] += $amount;

			$nbOwerShares = 0.0;
			foreach ($owers as $ower) {
				$owerWeight = $ower['weight'];
				if ($owerWeight === 0.0) {
					$owerWeight = 1.0;
				}
				$nbOwerShares += $owerWeight;
			}
			foreach ($owers as $ower) {
				$owerWeight = $ower['weight'];
				if ($owerWeight === 0.0) {
					$owerWeight = 1.0;
				}
				$owerId = $ower['id'];
				$spent = $amount / $nbOwerShares * $owerWeight;
				$membersFilteredBalance[$owerId] -= $spent;
				$membersSpent[$owerId] += $spent;
			}
		}

		// build global stats data
		$statistics = [];
		if ($currency === null) {
			foreach ($membersToDisplay as $memberId => $member) {
				$statistic = [
					'balance' => $membersBalance[$memberId],
					'filtered_balance' => $membersFilteredBalance[$memberId],
					'paid' => $membersPaid[$memberId],
					'spent' => $membersSpent[$memberId],
					'member' => $member
				];
				array_push($statistics, $statistic);
			}
		}
		else {
			foreach ($membersToDisplay as $memberId => $member) {
				$statistic = [
					'balance' => ($membersBalance[$memberId] === 0.0) ? 0 : $membersBalance[$memberId] / $currency['exchange_rate'],
					'filtered_balance' => ($membersFilteredBalance[$memberId] === 0.0) ? 0 : $membersFilteredBalance[$memberId] / $currency['exchange_rate'],
					'paid' => ($membersPaid[$memberId] === 0.0) ? 0 : $membersPaid[$memberId] / $currency['exchange_rate'],
					'spent' => ($membersSpent[$memberId] === 0.0) ? 0 : $membersSpent[$memberId] / $currency['exchange_rate'],
					'member' => $member
				];
				array_push($statistics, $statistic);
			}
		}

		// compute monthly stats
		$monthlyStats = [];
		$allMembersKey = 0;
		foreach ($bills as $bill) {
			$payerId = $bill['payer_id'];
			$amount = $bill['amount'];
			$date = DateTime::createFromFormat('U', $bill['timestamp']);
			$month = $date->format('Y-m');
			if (!array_key_exists($month, $monthlyStats)) {
				$monthlyStats[$month] = [];
				foreach ($membersToDisplay as $memberId => $member) {
					$monthlyStats[$month][$memberId] = 0;
				}
				$monthlyStats[$month][$allMembersKey] = 0;
			}

			if (array_key_exists($payerId, $membersToDisplay)) {
				$monthlyStats[$month][$payerId] += $amount;
				$monthlyStats[$month][$allMembersKey] += $amount;
			}
		}
		// monthly average
		$nbMonth = count(array_keys($monthlyStats));
		if ($nbMonth > 0) {
			$averageStats = [];
			foreach ($membersToDisplay as $memberId => $member) {
				$sum = 0;
				foreach ($monthlyStats as $month=>$mStat) {
					$sum += $monthlyStats[$month][$memberId];
				}
				$averageStats[$memberId] = $sum / $nbMonth;
			}
			// average for all members
			$sum = 0;
			foreach ($monthlyStats as $month=>$mStat) {
				$sum += $monthlyStats[$month][$allMembersKey];
			}
			$averageStats[$allMembersKey] = $sum / $nbMonth;

			$averageKey = $this->trans->t('Average per month');
			$monthlyStats[$averageKey] = $averageStats;
		}
		// convert if necessary
		if ($currency !== null) {
			foreach ($monthlyStats as $month=>$mStat) {
				foreach ($mStat as $mid=>$val) {
					$monthlyStats[$month][$mid] = ($monthlyStats[$month][$mid] === 0.0) ? 0 : $monthlyStats[$month][$mid] / $currency['exchange_rate'];
				}
			}
		}
		// compute category stats
		$categoryStats = [];
		foreach ($bills as $bill) {
			$categoryId = $bill['categoryid'];
			if (!array_key_exists(strval($categoryId), $this->categoryNames) and
				!array_key_exists(strval($categoryId), $projectCategories)
			) {
				$categoryId = 0;
			}
			$amount = $bill['amount'];
			if (!array_key_exists($categoryId, $categoryStats)) {
				$categoryStats[$categoryId] = 0;
			}
			$categoryStats[$categoryId] += $amount;
		}
		// convert if necessary
		if ($currency !== null) {
			foreach ($categoryStats as $catId=>$val) {
				$categoryStats[$catId] = ($val === 0.0) ? 0 : $val / $currency['exchange_rate'];
			}
		}
		// compute category per member stats
		$categoryMemberStats = [];
		foreach ($bills as $bill) {
			$payerId = $bill['payer_id'];
			$categoryId = $bill['categoryid'];
			if (!array_key_exists(strval($categoryId), $this->categoryNames) and
				!array_key_exists(strval($categoryId), $projectCategories)
			) {
				$categoryId = 0;
			}
			$amount = $bill['amount'];
			if (!array_key_exists($categoryId, $categoryMemberStats)) {
				$categoryMemberStats[$categoryId] = [];
				foreach ($membersToDisplay as $memberId => $member) {
					$categoryMemberStats[$categoryId][$memberId] = 0;
				}
			}
			if (array_key_exists($payerId, $membersToDisplay)) {
				$categoryMemberStats[$categoryId][$payerId] += $amount;
			}
		}
		// convert if necessary
		if ($currency !== null) {
			foreach ($categoryMemberStats as $catId=>$mStat) {
				foreach ($mStat as $mid=>$val) {
					$categoryMemberStats[$catId][$mid] = ($val === 0.0) ? 0 : $val / $currency['exchange_rate'];
				}
			}
		}

		return [
			'stats'=>$statistics,
			'monthlyStats'=>$monthlyStats,
			'categoryStats'=>$categoryStats,
			'categoryMemberStats'=>$categoryMemberStats,
			'memberIds'=>array_keys($membersToDisplay)
		];
	}

	/**
	 * @param $userId
	 * @return array
	 * @throws CospendException
	 */
	public function getProjects($userId) {
		$projects = [];
		$projectIds = [];

		$qb = $this->dbconnection->getQueryBuilder();

		$qb->select('p.id', 'p.password', 'p.name', 'p.email', 'p.autoexport', 'p.guestpermissions', 'p.currencyname', 'p.lastchanged')
			->from('cospend_projects', 'p')
			->where(
				$qb->expr()->eq('userid', $qb->createNamedParameter($userId, IQueryBuilder::PARAM_STR))
			);
		$req = $qb->execute();

		$dbProjectId = null;
		$dbPassword = null;
		while ($row = $req->fetch()){
			$dbProjectId = $row['id'];
			array_push($projectIds, $dbProjectId);
			$dbName  = $row['name'];
			$dbEmail = $row['email'];
			$autoexport = $row['autoexport'];
			$guestpermissions = $row['guestpermissions'];
			$dbCurrencyName = $row['currencyname'];
			$dbLastchanged = intval($row['lastchanged']);
			array_push($projects, [
				'name'=>$dbName,
				'contact_email'=>$dbEmail,
				'id'=>$dbProjectId,
				'autoexport'=>$autoexport,
				'lastchanged'=>$dbLastchanged,
				'active_members'=>null,
				'members'=>null,
				'balance'=>null,
				'shares'=>[],
				'guestpermissions'=>$guestpermissions,
				'currencyname'=>$dbCurrencyName
			]);
		}
		$req->closeCursor();

		$qb = $qb->resetQueryParts();

		// shared with user
		$qb->select('p.id', 'p.password', 'p.name', 'p.email', 'p.autoexport', 'p.guestpermissions', 'p.currencyname', 'p.lastchanged')
			->from('cospend_projects', 'p')
			->innerJoin('p', 'cospend_shares', 's', $qb->expr()->eq('p.id', 's.projectid'))
			->where(
				$qb->expr()->eq('s.userid', $qb->createNamedParameter($userId, IQueryBuilder::PARAM_STR))
			)
			->andWhere(
				$qb->expr()->eq('s.type', $qb->createNamedParameter('u', IQueryBuilder::PARAM_STR))
			);
		$req = $qb->execute();

		$dbProjectId = null;
		$dbPassword = null;
		while ($row = $req->fetch()){
			$dbProjectId = $row['id'];
			// avoid putting twice the same project
			// this can happen with a share loop
			if (!in_array($dbProjectId, $projectIds)) {
				$dbName = $row['name'];
				$dbEmail= $row['email'];
				$autoexport = $row['autoexport'];
				$guestpermissions = $row['guestpermissions'];
				$dbCurrencyName = $row['currencyname'];
				$dbLastchanged = intval($row['lastchanged']);
				array_push($projects, [
					'name'=>$dbName,
					'contact_email'=>$dbEmail,
					'id'=>$dbProjectId,
					'autoexport'=>$autoexport,
					'lastchanged'=>$dbLastchanged,
					'active_members'=>null,
					'members'=>null,
					'balance'=>null,
					'shares'=>[],
					'guestpermissions'=>$guestpermissions,
					'currencyname'=>$dbCurrencyName
				]);
				array_push($projectIds, $dbProjectId);
			}
		}
		$req->closeCursor();
		$qb = $qb->resetQueryParts();

		// shared with one of the groups the user is member of
		$userO = $this->userManager->get($userId);

		// get group with which a project is shared
		$candidateGroupIds = [];
		$qb->select('userid')
			->from('cospend_shares', 's')
			->where(
				$qb->expr()->eq('type', $qb->createNamedParameter('g', IQueryBuilder::PARAM_STR))
			)
			->groupBy('userid');
		$req = $qb->execute();
		while ($row = $req->fetch()){
			$groupId = $row['userid'];
			array_push($candidateGroupIds, $groupId);
		}
		$req->closeCursor();
		$qb = $qb->resetQueryParts();

		// is the user member of these groups?
		foreach ($candidateGroupIds as $candidateGroupId) {
			$group = $this->groupManager->get($candidateGroupId);
			if ($group !== null && $group->inGroup($userO)) {
				// get projects shared with this group
				$qb->select('p.id', 'p.password', 'p.name', 'p.email', 'p.autoexport', 'p.guestpermissions', 'p.currencyname', 'p.lastchanged')
					->from('cospend_projects', 'p')
					->innerJoin('p', 'cospend_shares', 's', $qb->expr()->eq('p.id', 's.projectid'))
					->where(
						$qb->expr()->eq('s.userid', $qb->createNamedParameter($candidateGroupId, IQueryBuilder::PARAM_STR))
					)
					->andWhere(
						$qb->expr()->eq('s.type', $qb->createNamedParameter('g', IQueryBuilder::PARAM_STR))
					);
				$req = $qb->execute();

				$dbProjectId = null;
				$dbPassword = null;
				while ($row = $req->fetch()){
					$dbProjectId = $row['id'];
					// avoid putting twice the same project
					// this can happen with a share loop
					if (!in_array($dbProjectId, $projectIds)) {
						$dbName = $row['name'];
						$dbEmail= $row['email'];
						$autoexport = $row['autoexport'];
						$guestpermissions = $row['guestpermissions'];
						$dbCurrencyName = $row['currencyname'];
						$dbLastchanged = intval($row['lastchanged']);
						array_push($projects, [
							'name'=>$dbName,
							'contact_email'=>$dbEmail,
							'id'=>$dbProjectId,
							'autoexport'=>$autoexport,
							'lastchanged'=>$dbLastchanged,
							'active_members'=>null,
							'members'=>null,
							'balance'=>null,
							'shares'=>[],
							'guestpermissions'=>$guestpermissions,
							'currencyname'=>$dbCurrencyName
						]);
						array_push($projectIds, $dbProjectId);
					}
				}
				$req->closeCursor();
				$qb = $qb->resetQueryParts();
			}
		}

		$circlesEnabled = OC::$server->getAppManager()->isEnabledForUser('circles');
		if ($circlesEnabled) {
			// get circles with which a project is shared
			$candidateCircleIds = [];
			$qb->select('userid')
				->from('cospend_shares', 's')
				->where(
					$qb->expr()->eq('type', $qb->createNamedParameter('c', IQueryBuilder::PARAM_STR))
				)
				->groupBy('userid');
			$req = $qb->execute();
			while ($row = $req->fetch()){
				$circleId = $row['userid'];
				array_push($candidateCircleIds, $circleId);
			}
			$req->closeCursor();
			$qb = $qb->resetQueryParts();

			// is the user member of these circles?
			foreach ($candidateCircleIds as $candidateCircleId) {
				if ($this->isUserInCircle($userId, $candidateCircleId)) {
					// get projects shared with this circle
					$qb->select('p.id', 'p.password', 'p.name', 'p.email', 'p.autoexport', 'p.guestpermissions', 'p.currencyname', 'p.lastchanged')
						->from('cospend_projects', 'p')
						->innerJoin('p', 'cospend_shares', 's', $qb->expr()->eq('p.id', 's.projectid'))
						->where(
							$qb->expr()->eq('s.userid', $qb->createNamedParameter($candidateCircleId, IQueryBuilder::PARAM_STR))
						)
						->andWhere(
							$qb->expr()->eq('s.type', $qb->createNamedParameter('c', IQueryBuilder::PARAM_STR))
						);
					$req = $qb->execute();

					$dbProjectId = null;
					$dbPassword = null;
					while ($row = $req->fetch()){
						$dbProjectId = $row['id'];
						// avoid putting twice the same project
						// this can happen with a share loop or multiple shares
						if (!in_array($dbProjectId, $projectIds)) {
							$dbName = $row['name'];
							$dbEmail= $row['email'];
							$autoexport = $row['autoexport'];
							$guestpermissions = $row['guestpermissions'];
							$dbCurrencyName = $row['currencyname'];
							$dbLastchanged = intval($row['lastchanged']);
							array_push($projects, [
								'name'=>$dbName,
								'contact_email'=>$dbEmail,
								'id'=>$dbProjectId,
								'autoexport'=>$autoexport,
								'lastchanged'=>$dbLastchanged,
								'active_members'=>null,
								'members'=>null,
								'balance'=>null,
								'shares'=>[],
								'guestpermissions'=>$guestpermissions,
								'currencyname'=>$dbCurrencyName
							]);
							array_push($projectIds, $dbProjectId);
						}
					}
					$req->closeCursor();
					$qb = $qb->resetQueryParts();
				}
			}
		}

		// get values for projects we're gonna return
		for ($i = 0; $i < count($projects); $i++) {
			$dbProjectId = $projects[$i]['id'];
			$members = $this->memberService->getMembers($dbProjectId, 'lowername');
			$shares = $this->getUserShares($dbProjectId);
			$groupShares = $this->getGroupShares($dbProjectId);
			$circleShares = $this->getCircleShares($dbProjectId);
			$currencies = $this->currencyMapper->list($dbProjectId);
			$categories = $this->categoryMapper->list($dbProjectId);

			$activeMembers = [];
			foreach ($members as $member) {
				if ($member['activated']) {
					array_push($activeMembers, $member);
				}
			}
			$balance = $this->getBalance($dbProjectId);
			$projects[$i]['active_members'] = $activeMembers;
			$projects[$i]['members'] = $members;
			$projects[$i]['balance'] = $balance;
			$projects[$i]['shares'] = $shares;
			$projects[$i]['group_shares'] = $groupShares;
			$projects[$i]['circle_shares'] = $circleShares;
			$projects[$i]['currencies'] = $currencies;
			$projects[$i]['categories'] = $categories;
		}

		// get external projects
		$qb->select('ep.projectid', 'ep.password', 'ep.ncurl')
			->from('cospend_ext_projects', 'ep')
			->where(
				$qb->expr()->eq('userid', $qb->createNamedParameter($userId, IQueryBuilder::PARAM_STR))
			);
		$req = $qb->execute();

		while ($row = $req->fetch()){
			$dbProjectId = $row['projectid'];
			$dbPassword = $row['password'];
			$dbNcUrl = $row['ncurl'];
			array_push($projects, [
				'name'=>$dbProjectId.'@'.$dbNcUrl,
				'ncurl'=>$dbNcUrl,
				'id'=>$dbProjectId,
				'password'=>$dbPassword,
				'active_members'=>null,
				'members'=>null,
				'balance'=>null,
				'shares'=>[],
				'external'=>true
			]);
		}
		$req->closeCursor();

		return $projects;
	}
}
