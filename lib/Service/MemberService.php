<?php

/**
 * Nextcloud - cospend
 *
 * This file is licensed under the Affero General Public License version 3 or
 * later. See the COPYING file.
 *
 * @author Julien Veyssier
 * @copyright Julien Veyssier 2019
 */

namespace OCA\Cospend\Service;

use DateTime;
use OC\Avatar\AvatarManager;
use OCA\Cospend\Controller\UtilsController;
use OCA\Cospend\Exception\CospendException;
use OCA\Cospend\Db\Member;
use OCA\Cospend\Db\MemberMapper;
use OCA\Cospend\Db\ProjectMapper;
use OCP\AppFramework\Db\DoesNotExistException;
use OCP\IL10N;
use OCP\IConfig;
use OCP\ILogger;

class MemberService extends BaseService {

	private $logger;
	private $config;
	private $trans;
	private $projectMapper;
	private $memberMapper;
	private $avatarManager;

	public function __construct(ILogger $logger,
								IL10N $l10n,
								IConfig $config,
								ProjectMapper $projectMapper,
								MemberMapper $memberMapper,
								AvatarManager $avatarManager) {
		parent::__construct($l10n);

		$this->trans = $l10n;
		$this->config = $config;
		$this->logger = $logger;
		$this->projectMapper = $projectMapper;
		$this->memberMapper = $memberMapper;
		$this->avatarManager = $avatarManager;
	}

	/***
	 * @param string $projectId
	 * @param string $name
	 * @return string
	 * @throws CospendException
	 */
	private function checkMemberName($projectId, $name) {
		$nameToAdd = $this->checkName($name);

		if (count($this->getMembersByName($projectId, $name)) > 0) {
			throw new CospendException($this->trans->t('This project already has this member.'));
		}
			return $nameToAdd;
	}

	/***
	 * @param string $weight
	 * @return float
	 * @throws CospendException
	 */
	private function checkWeight($weight) {
		if ($weight !== null && $weight !== '') {
			if (is_numeric($weight) and floatval($weight) > 0.0) {
				return floatval($weight);
			}
			else {
				throw new CospendException($this->trans->t('Weight is not a valid decimal value.'));
			}
		} else {
			return 1.0;
		}
	}

	/***
	 * @param $active
	 * @return bool
	 * @throws CospendException
	 */
	private function checkActive($active) {
		if ($active === null ||  $active === '' || !is_bool($active)) {
			throw new CospendException($this->trans->t('Active is not a valid bool value.'));
		}

		return boolval($active);
	}

	/***
	 * @param string|null $order
	 * @return string
	 */
	private function checkOrder($order) {
		if ($order !== null) {
			if ($order === 'lowername') {
				return 'name';
			}
			else {
				return $order;
			}
		}
		else {
			return 'name';
		}
	}

	/***
	 * @param string $color
	 * @return string|null
	 */
	private function checkColor($color) {
		if ($color !== null && $color === '') {
			return null;
		}

		return $color;
	}

	/**
	 * @param string $projectId
	 * @param string|null $order
	 * @param string|null $lastchanged
	 * @return Member[]
	 */
	public function getMembers($projectId, $order=null, $lastchanged=null) {
		$members = $this->memberMapper->list(
			$projectId,
			$this->checkOrder($order),
			$lastchanged);

		foreach ($members as $member) {
			$this->correctColor($member);
		}

		if ($order === 'lowername') {
			$members = array_reverse($members);
		}

		return $members;
	}

	/**
	 * @param string $projectId
	 * @param string $name
	 * @return Member[]
	 * @throws CospendException
	 */
	public function getMembersByName($projectId, $name) {
		$members = $this->memberMapper->listByName(
			$projectId,
			$this->checkName($name));

		foreach ($members as $member) {
			$this->correctColor($member);
		}

		return $members;
	}

	/**
	 * @param string $projectId
	 * @param string $name
	 * @param string $weight
	 * @param string $active
	 * @param string|null $color
	 * @return Member
	 * @throws CospendException
	 */
	public function addMember($projectId, $name, $weight, $active="1", $color=null) {
		try {
			return $this->memberMapper->add(
				$projectId,
				$this->checkMemberName($projectId, $name),
				$this->checkWeight($weight),
				$this->checkActive($active),
				$color,
				(new DateTime())->getTimestamp());
				} catch (DoesNotExistException $e) {
			throw new CospendException("Add member failed.", $e);
			}
	}


	/**
	 * @param string $projectId
	 * @param string $memberId
	 * @return Member
	 * @throws CospendException
	 */
	public function getMemberById($projectId, $memberId) {
		try {
			$member = $this->memberMapper->get(
				$this->checkId($memberId));

			if($member->getProjectid() !== $projectId) {
				throw new CospendException($this->trans->t('This project have no such member.'));
			}

			$this->correctColor($member);

			return $member;
		} catch (DoesNotExistException $e) {
			throw new CospendException("Get member failed.", $e);
		}
	}

	/**
	 * @param string $projectId
	 * @param string $memberId
	 * @param string $name
	 * @param string $weight
	 * @param string|null $color
	 * @return bool success
	 * @throws CospendException
	 */
	public function editMember($projectId, $memberId, $name, $weight, $color=null) {
		$memberToEdit = $this->getMemberById($projectId, $memberId);
		return $this->memberMapper->updateById(
			$memberToEdit->getId(),
			$this->checkName($name),
			$this->checkWeight($weight),
			(new DateTime())->getTimestamp(),
			$this->checkColor($color));
	}


	/**
	 * @param string $projectId
	 * @param string $memberId
	 * @return bool success
	 * @throws CospendException
	 */
	public function deleteMember($projectId, $memberId) {
		$memberToDeactivate = $this->getMemberById($projectId, $memberId);

		if ($memberToDeactivate->getActivated()) {
			return $this->memberMapper->deactivate($memberToDeactivate->getId());
		} else {
			return true;
		}
	}

	/***
	 * @param Member $member
	 */
	private function correctColor(&$member): void {
		if ($member->getColor() === null) {
			$av = $this->avatarManager->getGuestAvatar($member->getName());
			$member->setColor($av->avatarBackgroundColor($member->getName()));
		} else {
			$member->setColor((string)UtilsController::hexToRgb($member->getColor()));
		}
	}

}
