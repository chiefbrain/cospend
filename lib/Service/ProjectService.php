<?php

/**
 * Nextcloud - cospend
 *
 * This file is licensed under the Affero General Public License version 3 or
 * later. See the COPYING file.
 *
 * @author Julien Veyssier
 * @copyright Julien Veyssier 2019
 */

namespace OCA\Cospend\Service;

use DateTime;
use OCA\Cospend\Db\Project;
use OCA\Cospend\Exception\CospendException;
use OCA\Cospend\Db\ProjectMapper;
use OCA\Cospend\Exception\CospendFatalException;
use OCP\AppFramework\Db\DoesNotExistException;
use OCP\IL10N;
use OCP\IConfig;
use OCP\ILogger;

class ProjectService extends BaseService {

	private $logger;
	private $config;
	private $trans;
	private $projectMapper;
	private $billService;
	private $memberService;
	private $categoryService;
	private $currencyService;
	private $shareService;

	public function __construct(ILogger $logger,
								IL10N $l10n,
								IConfig $config,
								ProjectMapper $projectMapper,
								BillService $billService,
								MemberService $memberService,
								CategoryService $categoryService,
								CurrencyService $currencyService,
								ShareService $shareService) {
		parent::__construct($l10n);

		$this->trans = $l10n;
		$this->config = $config;
		$this->logger = $logger;
		$this->projectMapper = $projectMapper;
		$this->billService = $billService;
		$this->memberService = $memberService;
		$this->categoryService = $categoryService;
		$this->currencyService = $currencyService;
		$this->shareService = $shareService;
	}

	/***
	 * @param string $id
	 * @return string
	 * @throws CospendException
	 */
	private function checkProjectId($id) {
		if (strpos($id, '/') !== false) {
			throw new CospendException($this->trans->t('Invalid project id.'));
		}

		return $id;
	}

	/***
	 * @param string $name
	 * @return string
	 * @throws CospendException
	 * @throws CospendFatalException
	 */
	private function checkProjectName($name)
	{
		try {
			$this->projectMapper->getByName($name);
			throw new CospendException($this->trans->t('A project with name "%1$s" already exists.', [$name]));
		} catch (DoesNotExistException $e) {}

		return $name;
	}

	/***
	 * @param string $password
	 * @return string
	 */
	private function checkPassword($password) {
		if ($password !== null && $password !== '') {
			return password_hash($password, PASSWORD_DEFAULT);
		}

		return '';
	}

	/***
	 * @param string|null $contactEmail
	 * @return string
	 * @throws CospendException
	 */
	private function checkContactEmail($contactEmail) {
		if ($contactEmail === null || $contactEmail === '') {
			return '';
		} else {
			if (filter_var($contactEmail, FILTER_VALIDATE_EMAIL)) {
				return $contactEmail;
			}
			else {
				throw new CospendException($this->trans->t('Invalid email address.'));
			}
		}
	}

	/***
	 * @param string $autoExport
	 * @return string|null
	 */
	private function checkAutoExport($autoExport) {
		if($autoExport === '')
			return null;

		return $autoExport;
	}

	/***
	 * @param string|null $currencyName
	 * @return string|null
	 */
	private function checkCurrencyName($currencyName) {
		if ($currencyName !== null && $currencyName !== '')  {
			return $currencyName;
		}

		return null;
	}

	/**
	 * @param string $name
	 * @param string $id
	 * @param string $password
	 * @param string $contactEmail
	 * @param string|null $userId
	 * @return Project
	 * @throws CospendException
	 */
	public function createProject($name, $id, $password, $contactEmail, $userId=null) {
		try {
			return $this->projectMapper->add(
				$this->checkProjectId($id),
				$this->checkProjectName($name),
				$this->checkPassword($password),
				$this->checkContactEmail($contactEmail),
				(new DateTime())->getTimestamp(),
				$userId);
		} catch (DoesNotExistException $e) {
			throw $e;
			// throw new CospendException("Add project failed.", $e);
		}
	}

	/**
	 * @param string $projectId
	 * @return Project
	 * @throws CospendException
	 */
	public function getProjectById($projectId) {
		try {
			return $this->projectMapper->get($projectId);
		} catch (DoesNotExistException $e) {
			throw new CospendException("Get project failed.", $e);
		}
	}

	/**
	 * @param string $projectId
	 * @param string $name
	 * @param string $contactEmail
	 * @param string $password
	 * @param string|null $autoExport
	 * @param string|null $currencyName
	 * @return bool success
	 * @throws CospendException
	 */
	public function editProject($projectId, $name, $contactEmail, $password, $autoExport=null, $currencyName=null) {
		$projectToEdit = $this->getProjectById($this->checkProjectId($projectId));

		return $this->projectMapper->updateById(
			$projectToEdit->getId(),
			$this->checkProjectName($name),
			$this->checkContactEmail($contactEmail),
			$this->checkPassword($password),
			$this->checkAutoExport($autoExport),
			$this->checkCurrencyName($currencyName),
			(new DateTime())->getTimestamp());
	}

	/**
	 * @param string $projectId
	 * @return bool
	 * @throws CospendException
	 */
	public function deleteProject($projectId) {
		$projectToDelete = $this->getProjectById($projectId);

		// delete project bills
		$bills = $this->billService->getBills($projectToDelete->getId());
		foreach ($bills as $bill) {
			$this->billService->deleteById($projectId, $bill->getId());
		}

		// delete project members
		$members = $this->memberService->getMembers($projectToDelete->getId());
		foreach ($members as $member) {
			$this->memberService->deleteMember($projectId, $member->getId());
		}

		// delete shares
		$shares = $this->shareService->getShares($projectToDelete->getId());
		foreach ($shares as $share) {
			$this->shareService->deleteShare($projectId, $share->getId());
		}

		// delete currencies
		$currencies = $this->currencyService->list($projectId);
		foreach ($currencies as $currency) {
			$this->currencyService->deleteById($projectId, $currency->getId());
		}

		// delete categories
		$categories = $this->categoryService->list($projectId);
		foreach ($categories as $category) {
			$this->categoryService->deleteById($projectId, $category->getId());
		}

		// delete project
		return $this->projectMapper->deleteById($projectToDelete->getId());
	}
}
