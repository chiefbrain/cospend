<?php

namespace OCA\Cospend\Service;

use OCA\Cospend\Exception\CospendException;
use OCP\IL10N;

class BaseService {

	private $trans;

	public function __construct(IL10N $l10n) {
		$this->trans = $l10n;
	}

	/***
	 * @param string $id
	 * @return int
	 * @throws CospendException
	 */
	protected function checkId($id)
	{
		if (!is_int($id)) {
			throw new CospendException($this->trans->t('Incorrect field value: Id'));
		}

		return intval($id);
	}

	/***
	 * @param string $name
	 * @return string
	 * @throws CospendException
	 */
	protected function checkName($name)
	{
		if ($name === null || $name === '' ) {
			throw new CospendException($this->trans->t('Incorrect field value: Name'));
		}

		return $name;
	}
}
