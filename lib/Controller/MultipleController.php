<?php /** @noinspection PhpUnused */

/**
 * Nextcloud - cospend
 *
 * This file is licensed under the Affero General Public License version 3 or
 * later. See the COPYING file.
 *
 * @author Julien Veyssier <eneiluj@posteo.net>
 * @copyright Julien Veyssier 2019
 */

namespace OCA\Cospend\Controller;

use DateTime;
use Exception;
use OC;

use OCA\Cospend\Exception\CospendException;
use OCA\Cospend\Service\BillService;
use OCA\Cospend\Service\CategoryService;
use OCA\Cospend\Service\CurrencyService;
use OCA\Cospend\Service\MemberService;
use OCA\Cospend\Service\ProjectService;
use OCP\AppFramework\Db\DoesNotExistException;
use OCP\Files\FileInfo;
use OCP\Files\InvalidPathException;
use OCP\Files\NotFoundException;
use OCP\Files\NotPermittedException;
use OCP\IConfig;
use \OCP\IL10N;

use OCP\AppFramework\Http\ContentSecurityPolicy;

use OCP\IRequest;
use OCP\AppFramework\Http\TemplateResponse;
use OCP\AppFramework\Http\Template\PublicTemplateResponse;
use OCP\AppFramework\Http\DataResponse;
use OCP\AppFramework\ApiController;
use OCP\Constants;
use OCP\Lock\LockedException;
use OCP\Share;
use OCP\DB\QueryBuilder\IQueryBuilder;
use OCP\IUserManager;
use OCP\Share\IManager;
use OCP\IServerContainer;
use OCP\IGroupManager;
use OCP\ILogger;
use OCA\Cospend\Service\MultipleService;
use OCA\Cospend\Activity\ActivityManager;

function endswith($string, $test) {
    $strlen = strlen($string);
    $testlen = strlen($test);
    if ($testlen > $strlen) return false;
    return substr_compare($string, $test, $strlen - $testlen, $testlen) === 0;
}

class MultipleController extends ApiController {

    private $userId;
    private $userfolder;
    private $config;
    private $appVersion;
    private $shareManager;
    private $userManager;
    private $groupManager;
    private $dbconnection;
    private $dbtype;
    private $dbdblquotes;
    private $trans;
    private $logger;
    protected $appName;
    private $billService;
    private $categoryService;
    private $currencyService;
    private $multipleService;
	private $projectService;
	private $memberService;
    private $activityManager;


    public function __construct($AppName,
                                IRequest $request,
                                IServerContainer $serverContainer,
                                IConfig $config,
                                IManager $shareManager,
                                IUserManager $userManager,
                                IGroupManager $groupManager,
                                IL10N $trans,
                                ILogger $logger,
                                BillService $billService,
                                CategoryService $categoryService,
                                CurrencyService $currencyService,
                                MultipleService $multipleService,
								ProjectService $projectService,
								MemberService $memberService,
                                ActivityManager $activityManager,
                                $UserId){
        parent::__construct($AppName, $request,
                            'PUT, POST, GET, DELETE, PATCH, OPTIONS',
                            'Authorization, Content-Type, Accept',
                            1728000);
        $this->logger = $logger;
        $this->appName = $AppName;
        $this->billService = $billService;
        $this->categoryService = $categoryService;
        $this->currencyService = $currencyService;
        $this->multipleService = $multipleService;
		$this->projectService = $projectService;
		$this->memberService = $memberService;
        $this->appVersion = $config->getAppValue('cospend', 'installed_version');
        $this->userId = $UserId;
        $this->userManager = $userManager;
        $this->groupManager = $groupManager;
        $this->activityManager = $activityManager;
        $this->trans = $trans;
        $this->dbtype = $config->getSystemValue('dbtype');
        // IConfig object
        $this->config = $config;

        if ($this->dbtype === 'pgsql'){
            $this->dbdblquotes = '"';
        }
        else{
            $this->dbdblquotes = '`';
        }
        $this->dbconnection = OC::$server->getDatabaseConnection();
        if ($UserId !== null and $UserId !== '' and $serverContainer !== null){
            // path of user files folder relative to DATA folder
            $this->userfolder = $serverContainer->getUserFolder($UserId);
        }
        $this->shareManager = $shareManager;
    }

    /*
     * quote and choose string escape function depending on database used
     */
    private function db_quote_escape_string($str){
        return $this->dbconnection->quote($str);
    }

    /**
     * Welcome page
     * @NoAdminRequired
     * @NoCSRFRequired
     */
    public function index() {
        // PARAMS to view
        $params = [
            'projectid'=>'',
            'password'=>'',
            'username'=>$this->userId,
            'cospend_version'=>$this->appVersion
        ];
        $response = new TemplateResponse('cospend', 'main', $params);
        $csp = new ContentSecurityPolicy();
        $csp->addAllowedImageDomain('*')
            ->addAllowedMediaDomain('*')
            //->addAllowedChildSrcDomain('*')
            ->addAllowedFrameDomain('*')
            ->addAllowedWorkerSrcDomain('*')
            //->allowInlineScript(true)
            //->allowEvalScript(true)
            ->addAllowedObjectDomain('*')
            ->addAllowedScriptDomain('*')
            ->addAllowedConnectDomain('*');
        $response->setContentSecurityPolicy($csp);
        return $response;
    }

    /**
     * @NoAdminRequired
     * @NoCSRFRequired
     * @PublicPage
     * @param $projectid
     * @param string $password
     * @return PublicTemplateResponse
     */
    public function pubLoginProjectPassword($projectid, $password='') {
        // PARAMS to view
        $params = [
            'projectid'=>$projectid,
            'password'=>$password,
            'wrong'=>false,
            'cospend_version'=>$this->appVersion
        ];
        $response = new PublicTemplateResponse('cospend', 'login', $params);
        $response->setHeaderTitle($this->trans->t('Cospend public access'));
        $response->setHeaderDetails($this->trans->t('Enter password of project %s', [$projectid]));
        $response->setFooterVisible(false);
        $csp = new ContentSecurityPolicy();
        $csp->addAllowedImageDomain('*')
            ->addAllowedMediaDomain('*')
            //->addAllowedChildSrcDomain('*')
            ->addAllowedFrameDomain('*')
            ->addAllowedWorkerSrcDomain('*')
            ->addAllowedObjectDomain('*')
            ->addAllowedScriptDomain('*')
            ->addAllowedConnectDomain('*');
        $response->setContentSecurityPolicy($csp);
        return $response;
    }

    /**
     * @NoAdminRequired
     * @NoCSRFRequired
     * @PublicPage
     * @param $projectid
     * @return PublicTemplateResponse
     */
    public function pubLoginProject($projectid) {
        // PARAMS to view
        $params = [
            'projectid'=>$projectid,
            'wrong'=>false,
            'cospend_version'=>$this->appVersion
        ];
        $response = new PublicTemplateResponse('cospend', 'login', $params);
        $response->setHeaderTitle($this->trans->t('Cospend public access'));
        $response->setHeaderDetails($this->trans->t('Enter password of project %s', [$projectid]));
        $response->setFooterVisible(false);
        $csp = new ContentSecurityPolicy();
        $csp->addAllowedImageDomain('*')
            ->addAllowedMediaDomain('*')
            //->addAllowedChildSrcDomain('*')
            ->addAllowedFrameDomain('*')
            ->addAllowedWorkerSrcDomain('*')
            ->addAllowedObjectDomain('*')
            ->addAllowedScriptDomain('*')
            ->addAllowedConnectDomain('*');
        $response->setContentSecurityPolicy($csp);
        return $response;
    }

    /**
     * @NoAdminRequired
     * @NoCSRFRequired
     * @PublicPage
     */
    public function pubLogin() {
        // PARAMS to view
        $params = [
            'wrong'=>false,
            'cospend_version'=>$this->appVersion
        ];
        $response = new PublicTemplateResponse('cospend', 'login', $params);
        $response->setHeaderTitle($this->trans->t('Cospend public access'));
        $response->setHeaderDetails($this->trans->t('Enter project id and password'));
        $response->setFooterVisible(false);
        $csp = new ContentSecurityPolicy();
        $csp->addAllowedImageDomain('*')
            ->addAllowedMediaDomain('*')
            //->addAllowedChildSrcDomain('*')
            ->addAllowedFrameDomain('*')
            ->addAllowedWorkerSrcDomain('*')
            ->addAllowedObjectDomain('*')
            ->addAllowedScriptDomain('*')
            ->addAllowedConnectDomain('*');
        $response->setContentSecurityPolicy($csp);
        return $response;
    }

    /**
     * @NoAdminRequired
     * @NoCSRFRequired
     * @PublicPage
     * @param $projectid
     * @param $password
     * @return PublicTemplateResponse
     */
    public function pubProject($projectid, $password) {
        if ($this->checkLogin($projectid, $password)) {
            // PARAMS to view
            $params = [
                'projectid'=>$projectid,
                'password'=>$password,
                'cospend_version'=>$this->appVersion
            ];
            $response = new PublicTemplateResponse('cospend', 'main', $params);
            $response->setHeaderTitle($this->trans->t('Cospend public access'));
            $response->setHeaderDetails($this->trans->t('Project %s', [$projectid]));
            $response->setFooterVisible(false);
            $csp = new ContentSecurityPolicy();
            $csp->addAllowedImageDomain('*')
                ->addAllowedMediaDomain('*')
                //->addAllowedChildSrcDomain('*')
                ->addAllowedFrameDomain('*')
                ->addAllowedWorkerSrcDomain('*')
                ->addAllowedObjectDomain('*')
                ->addAllowedScriptDomain('*')
                ->addAllowedConnectDomain('*');
            $response->setContentSecurityPolicy($csp);
            return $response;
        }
        else {
            //$response = new DataResponse(null, 403);
            //return $response;
            $params = [
                'wrong'=>true,
                'cospend_version'=>$this->appVersion
            ];
            $response = new PublicTemplateResponse('cospend', 'login', $params);
            $response->setHeaderTitle($this->trans->t('Cospend public access'));
            $response->setHeaderDetails($this->trans->t('Access denied'));
            $response->setFooterVisible(false);
            $csp = new ContentSecurityPolicy();
            $csp->addAllowedImageDomain('*')
                ->addAllowedMediaDomain('*')
                //->addAllowedChildSrcDomain('*')
                ->addAllowedFrameDomain('*')
                ->addAllowedWorkerSrcDomain('*')
                ->addAllowedObjectDomain('*')
                ->addAllowedScriptDomain('*')
                ->addAllowedConnectDomain('*');
            $response->setContentSecurityPolicy($csp);
            return $response;
        }
    }

    private function checkLogin($projectId, $password) {
        if ($projectId === '' || $projectId === null ||
            $password === '' || $password === null
        ) {
            return false;
        }
        else {
            $qb = $this->dbconnection->getQueryBuilder();
            $qb->select('id', 'password')
               ->from('cospend_projects', 'p')
               ->where(
                   $qb->expr()->eq('id', $qb->createNamedParameter($projectId, IQueryBuilder::PARAM_STR))
               );
            $req = $qb->execute();
            $dbid = null;
            $dbPassword = null;
            while ($row = $req->fetch()){
                $dbPassword = $row['password'];
                break;
            }
            $req->closeCursor();
            return (
                $password !== null &&
                $password !== '' &&
                $dbPassword !== null &&
                password_verify($password, $dbPassword)
            );
        }
    }

	/**
	 * @NoAdminRequired
	 * @param $id
	 * @param $name
	 * @param $password
	 * @return DataResponse
	 * @throws CospendException
	 */
    public function webCreateProject($id, $name, $password) {
        $user = $this->userManager->get($this->userId);
        $userEmail = $user->getEMailAddress();
        $result = $this->projectService->createProject($name, $id, $password, $userEmail, $this->userId);
        return new DataResponse($result);
    }

    /**
     * @NoAdminRequired
     * @param $id
     * @param $url
     * @param $password
     * @return DataResponse
     */
    public function webAddExternalProject($id, $url, $password) {
        $result = $this->multipleService->addExternalProject($url, $id, $password, $this->userId);
        if (!is_array($result) and is_string($result)) {
            // project id
            return new DataResponse($result);
        }
        else {
            return new DataResponse($result, 400);
        }
    }

	/**
	 * @NoAdminRequired
	 * @param $projectid
	 * @return DataResponse
	 * @throws CospendException
	 */
    public function webDeleteProject($projectid) {
        if ($this->multipleService->userHasPermission($this->userId, $projectid, 'd')) {
            $result = $this->projectService->deleteProject($projectid);
            if ($result) {
                return new DataResponse('DELETED');
            }
            else {
                return new DataResponse($result, 404);
            }
        }
        else {
            return new DataResponse(
                ['message'=>'You are not allowed to delete this project']
                , 403
            );
        }
    }

	/**
	 * @NoAdminRequired
	 * @param $projectid
	 * @param $billid
	 * @return DataResponse
	 * @throws CospendException
	 */
    public function webDeleteBill($projectid, $billid) {
        if ($this->multipleService->userHasPermission($this->userId, $projectid, 'd')) {
            $billObj = $this->billService->get($projectid, $billid);

			$this->activityManager->triggerEvent(
				ActivityManager::COSPEND_OBJECT_BILL, $billObj,
				ActivityManager::SUBJECT_BILL_DELETE,
				[]
			);

            $result = $this->billService->deleteById($projectid, $billid);
            if ($result) {
                return new DataResponse('OK');
            }
            else {
                return new DataResponse($result, 404);
            }
        }
        else {
            return new DataResponse(
                ['message'=>'You are not allowed to delete this bill']
                , 403
            );
        }
    }

	/**
	 * @NoAdminRequired
	 * @param $projectid
	 * @return DataResponse
	 * @throws CospendException
	 */
    public function webGetProjectInfo($projectid) {
        if ($this->multipleService->userCanAccessProject($this->userId, $projectid)) {
            $projectInfo = $this->multipleService->getProjectInfo($projectid);
            return new DataResponse($projectInfo);
        }
        else {
            return new DataResponse(
                ['message'=>'You are not allowed to get this project\'s info']
                , 403
            );
        }
    }

	/**
	 * @NoAdminRequired
	 * @param $projectid
	 * @param null $dateMin
	 * @param null $dateMax
	 * @param null $paymentMode
	 * @param null $category
	 * @param null $amountMin
	 * @param null $amountMax
	 * @param string $showDisabled
	 * @param null $currencyId
	 * @return DataResponse
	 * @throws DoesNotExistException
	 * @throws CospendException
	 */
    public function webGetProjectStatistics($projectid, $dateMin=null, $dateMax=null, $paymentMode=null, $category=null,
                                            $amountMin=null, $amountMax=null, $showDisabled='1', $currencyId=null) {
        if ($this->multipleService->userCanAccessProject($this->userId, $projectid)) {
            $result = $this->multipleService->getProjectStatistics(
                $projectid, 'lowername', $dateMin, $dateMax, $paymentMode,
                $category, $amountMin, $amountMax, $showDisabled, $currencyId
            );
            return new DataResponse($result);
        }
        else {
            return new DataResponse(
                ['message'=>'You are not allowed to get this project\'s statistics']
                , 403
            );
        }
    }

	/**
	 * @NoAdminRequired
	 * @param $projectid
	 * @return DataResponse
	 * @throws CospendException
	 */
    public function webGetProjectSettlement($projectid) {
        if ($this->multipleService->userCanAccessProject($this->userId, $projectid)) {
            $result = $this->multipleService->getProjectSettlement($projectid);
            return new DataResponse($result);
        }
        else {
            return new DataResponse(
                ['message'=>'You are not allowed to get this project\'s settlement']
                , 403
            );
        }
    }

    /**
     * @NoAdminRequired
     * @param $projectid
     * @return DataResponse
     * @throws Exception
     */
    public function webAutoSettlement($projectid) {
        if ($this->multipleService->userCanAccessProject($this->userId, $projectid)) {
            $result = $this->multipleService->autoSettlement($projectid);
            if ($result === 'OK') {
                return new DataResponse($result);
            }
            else {
                return new DataResponse($result, 403);
            }
        }
        else {
            return new DataResponse(
                ['message'=>'You are not allowed to settle this project automatically']
                , 403
            );
        }
    }

    /**
     * @NoAdminRequired
     * @param $projectid
     * @param $memberid
     * @param $name
     * @param $weight
     * @param $activated
     * @param null $color
     * @return DataResponse
     * @throws Exception
     */
    public function webEditMember($projectid, $memberid, $name, $weight, $activated, $color=null) {
        if ($this->multipleService->userHasPermission($this->userId, $projectid, 'e')) {
            $result = $this->memberService->editMember($projectid, $memberid, $name, $weight, $activated, $color);
            if ($result) {
                return new DataResponse($result); // TODO?
            }
            else {
                return new DataResponse($result, 400);
            }
        }
        else {
            return new DataResponse(
                ['message'=>'You are not allowed to edit this member']
                , 403
            );
        }
    }

    /**
     * @NoAdminRequired
     * @param $projectid
     * @param $billid
     * @param $date
     * @param $what
     * @param $payer
     * @param $payed_for
     * @param $amount
     * @param $repeat
     * @param null $paymentmode
     * @param null $categoryid
     * @param null $repeatallactive
     * @param null $repeatuntil
     * @param null $timestamp
     * @return DataResponse
     * @throws Exception
     */
    public function webEditBill($projectid, $billid, $date, $what, $payer, $payed_for,
                                $amount, $repeat, $paymentmode=null, $categoryid=null,
                                $repeatallactive=null, $repeatuntil=null, $timestamp=null) {
        if ($this->multipleService->userHasPermission($this->userId, $projectid, 'e')) {
            $result =  $this->billService->editBill(
                $projectid, $billid, $date, $what, $payer, $payed_for,
                $amount, $repeat, $paymentmode, $categoryid,
                $repeatallactive, $repeatuntil, $timestamp
            );
            if ($result) {
                $billObj = $this->billService->get($projectid, $billid);
                $this->activityManager->triggerEvent(
                    ActivityManager::COSPEND_OBJECT_BILL, $billObj,
                    ActivityManager::SUBJECT_BILL_UPDATE,
                    []
                );

                return new DataResponse($result); // TODO
            }
            else {
                return new DataResponse($result, 400);
            }
        }
        else {
            return new DataResponse(
                ['message'=>'You are not allowed to edit this bill']
                , 403
            );
        }
    }

    /**
     * @NoAdminRequired
     * @param $projectid
     * @param $name
     * @param $contact_email
     * @param $password
     * @param null $autoexport
     * @param null $currencyname
     * @return DataResponse
     * @throws Exception
     */
    public function webEditProject($projectid, $name, $contact_email, $password, $autoexport=null, $currencyname=null) {
        if ($this->multipleService->userHasPermission($this->userId, $projectid, 'e')) {
            $result = $this->projectService->editProject($projectid, $name, $contact_email, $password, $autoexport, $currencyname);
            if ($result) {
                return new DataResponse('UPDATED');
            }
            else {
                return new DataResponse($result, 400);
            }
        }
        else {
            return new DataResponse(
                ['message'=>'You are not allowed to edit this project']
                , 403
            );
        }
    }

    /**
     * @NoAdminRequired
     * @param $projectid
     * @param $ncurl
     * @param $password
     * @return DataResponse
     */
    public function webEditExternalProject($projectid, $ncurl, $password) {
        if ($this->multipleService->userCanAccessExternalProject($this->userId, $projectid, $ncurl)) {
            $result = $this->multipleService->editExternalProject($projectid, $ncurl, $password);
            if ($result === 'UPDATED') {
                return new DataResponse($result);
            }
            else {
                return new DataResponse($result, 403);
            }
        }
        else {
            return new DataResponse(
                ['message'=>'You are not allowed to edit this external project']
                , 400
            );
        }
    }

    /**
     * @NoAdminRequired
     * @param $projectid
     * @param $ncurl
     * @return DataResponse
     */
    public function webDeleteExternalProject($projectid, $ncurl) {
        if ($this->multipleService->userCanAccessExternalProject($this->userId, $projectid, $ncurl)) {
            $result = $this->multipleService->deleteExternalProject($projectid, $ncurl);
            if ($result === 'DELETED') {
                return new DataResponse($result);
            }
            else {
                return new DataResponse($result, 403);
            }
        }
        else {
            return new DataResponse(
                ['message'=>'You are not allowed to delete this external project']
                , 400
            );
        }
    }

    /**
     * @NoAdminRequired
     * @param $projectid
     * @param $date
     * @param $what
     * @param $payer
     * @param $payed_for
     * @param $amount
     * @param $repeat
     * @param null $paymentmode
     * @param null $categoryid
     * @param int $repeatallactive
     * @param null $repeatuntil
     * @param null $timestamp
     * @return DataResponse
     * @throws Exception
     */
    public function webAddBill($projectid, $date, $what, $payer, $payed_for, $amount,
                               $repeat, $paymentmode=null, $categoryid=null, $repeatallactive=0,
                               $repeatuntil=null, $timestamp=null) {
        if ($this->multipleService->userHasPermission($this->userId, $projectid, 'c')) {
			$billObj = $this->billService->add(
                $projectid, $date, $what, $payer, $payed_for, $amount,
                $repeat, $paymentmode, $categoryid, $repeatallactive, $repeatuntil, $timestamp
            );

			$this->activityManager->triggerEvent(
				ActivityManager::COSPEND_OBJECT_BILL, $billObj,
				ActivityManager::SUBJECT_BILL_CREATE,
				[]
			);

			return new DataResponse($billObj);
        }
        else {
            return new DataResponse(
                ['message'=>'You are not allowed to add a bill to this project']
                , 403
            );
        }
    }

    /**
     * @NoAdminRequired
     * @param $projectid
     * @param $name
     * @param int $weight
     * @param int $active
     * @param null $color
     * @return DataResponse
     * @throws Exception
     */
    public function webAddMember($projectid, $name, $weight=1, $active=1, $color=null) {
        if ($this->multipleService->userHasPermission($this->userId, $projectid, 'c')) {
            $result = $this->memberService->addMember($projectid, $name, $weight, $active, $color);
            return new DataResponse($result);
        }
        else {
            return new DataResponse(
                ['message'=>'You are not allowed to add member to this project']
                , 403
            );
        }
    }

	/**
	 * @NoAdminRequired
	 * @param $projectid
	 * @param null $lastchanged
	 * @return DataResponse
	 * @throws CospendException
	 */
    public function webGetBills($projectid, $lastchanged=null) {
        if ($this->multipleService->userCanAccessProject($this->userId, $projectid)) {
            $bills = $this->billService->getBills($projectid, null, null, null, null, null, null, $lastchanged);
            return new DataResponse($bills);
        }
        else {
            return new DataResponse(
                ['message'=>'You are not allowed to get bills of this project']
                , 403
            );
        }
    }

	/**
	 * @NoAdminRequired
	 * @NoCSRFRequired
	 *
	 * @throws CospendException
	 */
    public function webGetProjects() {
        return new DataResponse(
            $this->multipleService->getProjects($this->userId)
        );
    }

    /**
     * curl -X POST https://ihatemoney.org/api/projects \
     *   -d 'name=yay&id=yay&password=yay&contact_email=yay@notmyidea.org'
     *   "yay"
     *
     * @NoAdminRequired
     * @NoCSRFRequired
     * @PublicPage
     * @CORS
     * @param $name
     * @param $id
     * @param $password
     * @param $contact_email
     * @return DataResponse
     * @throws Exception
     */
    public function apiCreateProject($name, $id, $password, $contact_email) {
        $allow = intval($this->config->getAppValue('cospend', 'allowAnonymousCreation'));
        if ($allow) {
            $result = $this->projectService->createProject($name, $id, $password, $contact_email);
			return new DataResponse($result);
        }
        else {
            return new DataResponse(
                ['message'=>'Anonymous project creation is not allowed on this server']
                , 403
            );
        }
    }

    /**
     * @NoAdminRequired
     * @NoCSRFRequired
     * @CORS
     * @param $name
     * @param $id
     * @param $password
     * @param $contact_email
     * @return DataResponse
     * @throws Exception
     */
    public function apiPrivCreateProject($name, $id, $password, $contact_email) {
        $result = $this->projectService->createProject($name, $id, $password, $contact_email, $this->userId);
		return new DataResponse($result);
    }

	/**
	 * @NoAdminRequired
	 * @NoCSRFRequired
	 * @PublicPage
	 * @CORS
	 * @param $projectid
	 * @param $password
	 * @return DataResponse
	 * @throws CospendException
	 */
    public function apiGetProjectInfo($projectid, $password) {
        if ($this->checkLogin($projectid, $password)) {
            $projectInfo = $this->multipleService->getProjectInfo($projectid);
            if ($projectInfo !== null) {
                unset($projectInfo['userid']);
                return new DataResponse($projectInfo);
            }
            else {
                return new DataResponse(
                    ['message'=>'Project not found in the database']
                    , 404
                );
            }
        }
        else {
            return new DataResponse(
                ['message'=>'Unauthorized action']
                , 400
            );
        }
    }

	/**
	 * @NoAdminRequired
	 * @NoCSRFRequired
	 * @CORS
	 * @param $projectid
	 * @return DataResponse
	 * @throws CospendException
	 */
    public function apiPrivGetProjectInfo($projectid) {
        if ($this->multipleService->userCanAccessProject($this->userId, $projectid)) {
            $projectInfo = $this->multipleService->getProjectInfo($projectid);
            if ($projectInfo !== null) {
                unset($projectInfo['userid']);
                return new DataResponse($projectInfo);
            }
            else {
                return new DataResponse(
                    ['message'=>'Project not found in the database']
                    , 404
                );
            }
        }
        else {
            return new DataResponse(
                ['message'=>'Unauthorized action.']
                , 403
            );
        }
    }

    /**
     * @NoAdminRequired
     * @NoCSRFRequired
     * @PublicPage
     * @CORS
     * @param $projectid
     * @param $passwd
     * @param $name
     * @param $contact_email
     * @param $password
     * @param null $autoexport
     * @param null $currencyname
     * @return DataResponse
     * @throws Exception
     */
    public function apiSetProjectInfo($projectid, $passwd, $name, $contact_email, $password, $autoexport=null, $currencyname=null) {
        if ($this->checkLogin($projectid, $passwd) and $this->multipleService->guestHasPermission($projectid, 'e')) {
            $result = $this->projectService->editProject($projectid, $name, $contact_email, $password, $autoexport, $currencyname);
            if ($result) {
                return new DataResponse('UPDATED');
            }
            else {
                return new DataResponse($result, 400);
            }
        }
        else {
            return new DataResponse(
                ['message'=>'Unauthorized action']
                , 401
            );
        }
    }

    /**
     * @NoAdminRequired
     * @NoCSRFRequired
     * @CORS
     * @param $projectid
     * @param $name
     * @param $contact_email
     * @param $password
     * @param null $autoexport
     * @param null $currencyname
     * @return DataResponse
     * @throws Exception
     */
    public function apiPrivSetProjectInfo($projectid, $name, $contact_email, $password, $autoexport=null, $currencyname=null) {
        if ($this->multipleService->userHasPermission($this->userId, $projectid, 'e')) {
            $result = $this->projectService->editProject($projectid, $name, $contact_email, $password, $autoexport, $currencyname);
            if ($result) {
                return new DataResponse('UPDATED');
            }
            else {
                return new DataResponse($result, 400);
            }
        }
        else {
            return new DataResponse(
                ['message'=>'Unauthorized action.']
                , 401
            );
        }
    }

    /**
     * @NoAdminRequired
     * @NoCSRFRequired
     * @PublicPage
     * @CORS
     * @param $projectid
     * @param $password
     * @param null $lastchanged
     * @return DataResponse
     */
    public function apiGetMembers($projectid, $password, $lastchanged=null) {
        if ($this->checkLogin($projectid, $password)) {
            $members = $this->memberService->getMembers($projectid, null, $lastchanged);
            return new DataResponse($members);
        }
        else {
            return new DataResponse(
                ['message'=>'Unauthorized action']
                , 401
            );
        }
    }

	/**
	 * @NoAdminRequired
	 * @NoCSRFRequired
	 * @CORS
	 * @param $projectid
	 * @param null $lastchanged
	 * @return DataResponse
	 * @throws CospendException
	 */
    public function apiPrivGetMembers($projectid, $lastchanged=null) {
        if ($this->multipleService->userCanAccessProject($this->userId, $projectid)) {
            $members = $this->memberService->getMembers($projectid, null, $lastchanged);
            return new DataResponse($members);
        }
        else {
            return new DataResponse(
                ['message'=>'Unauthorized action']
                , 403
            );
        }
    }

	/**
	 * @NoAdminRequired
	 * @NoCSRFRequired
	 * @PublicPage
	 * @CORS
	 * @param $projectid
	 * @param $password
	 * @param null $lastchanged
	 * @return DataResponse
	 * @throws CospendException
	 */
    public function apiGetBills($projectid, $password, $lastchanged=null) {
        if ($this->checkLogin($projectid, $password)) {
            $bills = $this->billService->getBills($projectid, null, null, null, null, null, null, $lastchanged);
            return new DataResponse($bills);
        }
        else {
            return new DataResponse(
                ['message'=>'Unauthorized action']
                , 401
            );
        }
    }

    /**
     * @NoAdminRequired
     * @NoCSRFRequired
     * @CORS
     * @param $projectid
     * @param null $lastchanged
     * @return DataResponse
     * @throws Exception
     */
    public function apiPrivGetBills($projectid, $lastchanged=null) {
        if ($this->multipleService->userCanAccessProject($this->userId, $projectid)) {
            $bills = $this->billService->getBills($projectid, null, null, null, null, null, null, $lastchanged);
            $billIds = $this->billService->getAllBillIds($projectid);
            $ts = (new DateTime())->getTimestamp();
            return new DataResponse([
                'bills'=>$bills,
                'allBillIds'=>$billIds,
                'timestamp'=>$ts
            ]);
        }
        else {
            return new DataResponse(
                ['message'=>'Unauthorized action']
                , 403
            );
        }
    }

    /**
     * @NoAdminRequired
     * @NoCSRFRequired
     * @PublicPage
     * @CORS
     * @param $projectid
     * @param $password
     * @param null $lastchanged
     * @return DataResponse
     * @throws Exception
     */
    public function apiv2GetBills($projectid, $password, $lastchanged=null) {
        if ($this->checkLogin($projectid, $password)) {
            $bills = $this->billService->getBills($projectid, null, null, null, null, null, null, $lastchanged);
            $billIds = $this->billService->getAllBillIds($projectid);
            $ts = (new DateTime())->getTimestamp();
            return new DataResponse([
                'bills'=>$bills,
                'allBillIds'=>$billIds,
                'timestamp'=>$ts
            ]);
        }
        else {
            return new DataResponse(
                ['message'=>'Unauthorized action']
                , 401
            );
        }
    }

    /**
     * @NoAdminRequired
     * @NoCSRFRequired
     * @PublicPage
     * @CORS
     * @param $projectid
     * @param $password
     * @param $name
     * @param $weight
     * @param int $active
     * @param null $color
     * @return DataResponse
     * @throws Exception
     */
    public function apiAddMember($projectid, $password, $name, $weight, $active=1, $color=null) {
        if ($this->checkLogin($projectid, $password) and $this->multipleService->guestHasPermission($projectid, 'c')) {
            $result = $this->memberService->addMember($projectid, $name, $weight, $active, $color);
			return new DataResponse($result->getId());
        }
        else {
            return new DataResponse(
                ['message'=>'Unauthorized action']
                , 401
            );
        }
    }

    /**
     * @NoAdminRequired
     * @NoCSRFRequired
     * @CORS
     * @param $projectid
     * @param $name
     * @param $weight
     * @param int $active
     * @param null $color
     * @return DataResponse
     * @throws Exception
     */
    public function apiPrivAddMember($projectid, $name, $weight, $active=1, $color=null) {
        if ($this->multipleService->userHasPermission($this->userId, $projectid, 'c')) {
            $result = $this->memberService->addMember($projectid, $name, $weight, $active, $color);
			return new DataResponse($result->getId());
        }
        else {
            return new DataResponse(
                ['message'=>'Unauthorized action']
                , 403
            );
        }
    }

    /**
     * @NoAdminRequired
     * @NoCSRFRequired
     * @PublicPage
     * @CORS
     * @param $projectid
     * @param $password
     * @param $date
     * @param $what
     * @param $payer
     * @param $payed_for
     * @param $amount
     * @param string $repeat
     * @param null $paymentmode
     * @param null $categoryid
     * @param int $repeatallactive
     * @param null $repeatuntil
     * @return DataResponse
     * @throws Exception
     */
    public function apiAddBill($projectid, $password, $date, $what, $payer, $payed_for,
                               $amount, $repeat='n', $paymentmode=null, $categoryid=null,
                               $repeatallactive=0, $repeatuntil=null) {
        if ($this->checkLogin($projectid, $password) and $this->multipleService->guestHasPermission($projectid, 'c')) {
			$billObj = $this->billService->add($projectid, $date, $what, $payer, $payed_for, $amount,
                                                     $repeat, $paymentmode, $categoryid, $repeatallactive,
                                                     $repeatuntil, $timestamp);	// TODO Timestamp not initialized

			$this->activityManager->triggerEvent(
				ActivityManager::COSPEND_OBJECT_BILL, $billObj,
				ActivityManager::SUBJECT_BILL_CREATE,
				[]
			);
			return new DataResponse($billObj);
		}
        else {
            return new DataResponse(
                ['message'=>'Unauthorized action']
                , 401
            );
        }
    }

    /**
     * @NoAdminRequired
     * @NoCSRFRequired
     * @CORS
     * @param $projectid
     * @param $date
     * @param $what
     * @param $payer
     * @param $payed_for
     * @param $amount
     * @param string $repeat
     * @param null $paymentmode
     * @param null $categoryid
     * @param int $repeatallactive
     * @param null $repeatuntil
     * @param null $timestamp
     * @return DataResponse
     * @throws Exception
     */
    public function apiPrivAddBill($projectid, $date, $what, $payer, $payed_for,
                               $amount, $repeat='n', $paymentmode=null, $categoryid=null,
                               $repeatallactive=0, $repeatuntil=null, $timestamp=null) {
        if ($this->multipleService->userHasPermission($this->userId, $projectid, 'c')) {
			$billObj = $this->billService->add($projectid, $date, $what, $payer, $payed_for, $amount,
                                                     $repeat, $paymentmode, $categoryid, $repeatallactive,
                                                     $repeatuntil, $timestamp);

			$this->activityManager->triggerEvent(
				ActivityManager::COSPEND_OBJECT_BILL, $billObj,
				ActivityManager::SUBJECT_BILL_CREATE,
				[]
			);
			return new DataResponse($billObj);
        }
        else {
            return new DataResponse(
                ['message'=>'Unauthorized action']
                , 403
            );
        }
    }

    /**
     * @NoAdminRequired
     * @NoCSRFRequired
     * @PublicPage
     * @CORS
     * @param $projectid
     * @param $password
     * @param $billid
     * @param $date
     * @param $what
     * @param $payer
     * @param $payed_for
     * @param $amount
     * @param string $repeat
     * @param null $paymentmode
     * @param null $categoryid
     * @param null $repeatallactive
     * @param null $repeatuntil
     * @param null $timestamp
     * @return DataResponse
     * @throws Exception
     */
    public function apiEditBill($projectid, $password, $billid, $date, $what, $payer, $payed_for,
                                $amount, $repeat='n', $paymentmode=null, $categoryid=null,
                                $repeatallactive=null, $repeatuntil=null, $timestamp=null) {
        if ($this->checkLogin($projectid, $password) and $this->multipleService->guestHasPermission($projectid, 'e')) {
			$billObj = $this->billService->editBill($projectid, $billid, $date, $what, $payer, $payed_for,
                                                      $amount, $repeat, $paymentmode, $categoryid,
                                                      $repeatallactive, $repeatuntil, $timestamp);

			$this->activityManager->triggerEvent(
				ActivityManager::COSPEND_OBJECT_BILL, $billObj,
				ActivityManager::SUBJECT_BILL_UPDATE,
				[]
			);

			return new DataResponse($billObj);
        }
        else {
            return new DataResponse(
                ['message'=>'Unauthorized action']
                , 401
            );
        }
    }

    /**
     * @NoAdminRequired
     * @NoCSRFRequired
     * @CORS
     * @param $projectid
     * @param $billid
     * @param $date
     * @param $what
     * @param $payer
     * @param $payed_for
     * @param $amount
     * @param string $repeat
     * @param null $paymentmode
     * @param null $categoryid
     * @param null $repeatallactive
     * @param null $repeatuntil
     * @param null $timestamp
     * @return DataResponse
     * @throws Exception
     */
    public function apiPrivEditBill($projectid, $billid, $date, $what, $payer, $payed_for,
                                $amount, $repeat='n', $paymentmode=null, $categoryid=null,
                                $repeatallactive=null, $repeatuntil=null, $timestamp=null) {
        if ($this->multipleService->userHasPermission($this->userId, $projectid, 'e')) {
            $result = $this->billService->editBill($projectid, $billid, $date, $what, $payer, $payed_for,
                                                      $amount, $repeat, $paymentmode, $categoryid,
                                                      $repeatallactive, $repeatuntil, $timestamp);
            if ($result) {
				$billObj = $this->billService->get($projectid, $billid);
                $this->activityManager->triggerEvent(
                    ActivityManager::COSPEND_OBJECT_BILL, $billObj,
                    ActivityManager::SUBJECT_BILL_UPDATE,
                    []
                );

                return new DataResponse($result);
            }
            else {
                return new DataResponse($result, 400);
            }
        }
        else {
            return new DataResponse(
                ['message'=>'Unauthorized action']
                , 403
            );
        }
    }

	/**
	 * @NoAdminRequired
	 * @NoCSRFRequired
	 * @PublicPage
	 * @CORS
	 * @param $projectid
	 * @param $password
	 * @param $billid
	 * @return DataResponse
	 * @throws CospendException
	 */
    public function apiDeleteBill($projectid, $password, $billid) {
        if ($this->checkLogin($projectid, $password) and $this->multipleService->guestHasPermission($projectid, 'd')) {
            $billObj = $this->billService->get($projectid, $billid);
			$this->activityManager->triggerEvent(
				ActivityManager::COSPEND_OBJECT_BILL, $billObj,
				ActivityManager::SUBJECT_BILL_DELETE,
				[]
			);

			$result = $this->billService->deleteById($projectid, $billid);
			if ($result) {
				return new DataResponse('OK');
			}

            return new DataResponse($result, 404);
        }
        else {
            return new DataResponse(
                ['message'=>'Unauthorized action']
                , 401
            );
        }
    }

	/**
	 * @NoAdminRequired
	 * @NoCSRFRequired
	 * @CORS
	 * @param $projectid
	 * @param $billid
	 * @return DataResponse
	 * @throws CospendException
	 */
    public function apiPrivDeleteBill($projectid, $billid) {
        if ($this->multipleService->userHasPermission($this->userId, $projectid, 'd')) {
            $billObj = $this->billService->get($projectid, $billid) ;

			$this->activityManager->triggerEvent(
				ActivityManager::COSPEND_OBJECT_BILL, $billObj,
				ActivityManager::SUBJECT_BILL_DELETE,
				[]
			);

			$result = $this->billService->deleteById($projectid, $billid);
			if ($result) {
				return new DataResponse($result);
			}

            return new DataResponse($result, 404);
        }
        else {
            return new DataResponse(
                ['message'=>'Unauthorized action']
                , 403
            );
        }
    }

	/**
	 * @NoAdminRequired
	 * @NoCSRFRequired
	 * @PublicPage
	 * @CORS
	 * @param $projectid
	 * @param $password
	 * @param $memberid
	 * @return DataResponse
	 * @throws CospendException
	 */
    public function apiDeleteMember($projectid, $password, $memberid) {
        if ($this->checkLogin($projectid, $password) and $this->multipleService->guestHasPermission($projectid, 'd')) {
            $result = $this->memberService->deleteMember($projectid, $memberid);
            if ($result) {
                return new DataResponse('OK');
            }
            else {
                return new DataResponse($result, 404);
            }
        }
        else {
            return new DataResponse(
                ['message'=>'Unauthorized action']
                , 401
            );
        }
    }

	/**
	 * @NoAdminRequired
	 * @NoCSRFRequired
	 * @CORS
	 * @param $projectid
	 * @param $memberid
	 * @return DataResponse
	 * @throws CospendException
	 */
    public function apiPrivDeleteMember($projectid, $memberid) {
        if ($this->multipleService->userHasPermission($this->userId, $projectid, 'd')) {
            $result = $this->memberService->deleteMember($projectid, $memberid);
            if ($result) {
                return new DataResponse('OK');
            }
            else {
                return new DataResponse($result, 404);
            }
        }
        else {
            return new DataResponse(
                ['message'=>'Unauthorized action']
                , 403
            );
        }
    }

    /**
     * @NoAdminRequired
     * @NoCSRFRequired
     * @PublicPage
     * @CORS
     * @param $projectid
     * @param $password
     * @return DataResponse
     * @throws CospendException
     */
    public function apiDeleteProject($projectid, $password) {
        if ($this->checkLogin($projectid, $password) and $this->multipleService->guestHasPermission($projectid, 'd')) {
            $result = $this->projectService->deleteProject($projectid);
            if ($result) {
                return new DataResponse('DELETED');
            }
            else {
                return new DataResponse($result, 404);
            }
        }
        else {
            return new DataResponse(
                ['message'=>'Unauthorized action']
                , 401
            );
        }
    }

    /**
     * @NoAdminRequired
     * @NoCSRFRequired
     * @CORS
     * @param $projectid
     * @return DataResponse
     * @throws CospendException
     */
    public function apiPrivDeleteProject($projectid) {
        if ($this->multipleService->userHasPermission($this->userId, $projectid, 'd')) {
            $result = $this->projectService->deleteProject($projectid);
            if ($result) {
                return new DataResponse('DELETED');
            }
            else {
                return new DataResponse($result, 404);
            }
        }
        else {
            return new DataResponse(
                ['message'=>'Unauthorized action']
                , 403
            );
        }
    }

    /**
     * @NoAdminRequired
     * @NoCSRFRequired
     * @PublicPage
     * @CORS
     * @param $projectid
     * @param $password
     * @param $memberid
     * @param $name
     * @param $weight
     * @param $activated
     * @param null $color
     * @return DataResponse
     * @throws Exception
     */
    public function apiEditMember($projectid, $password, $memberid, $name, $weight, $activated, $color=null) {
        if ($this->checkLogin($projectid, $password) and $this->multipleService->guestHasPermission($projectid, 'e')) {
            $result = $this->memberService->editMember($projectid, $memberid, $name, $weight, $activated, $color);
            if ($result) {
                return new DataResponse($result); // TODO
            }
            else {
                return new DataResponse($result, 403);
            }
        }
        else {
            return new DataResponse(
                ['message'=>'Unauthorized action']
                , 401
            );
        }
    }

    /**
     * @NoAdminRequired
     * @NoCSRFRequired
     * @CORS
     * @param $projectid
     * @param $memberid
     * @param $name
     * @param $weight
     * @param $activated
     * @param null $color
     * @return DataResponse
     * @throws Exception
     */
    public function apiPrivEditMember($projectid, $memberid, $name, $weight, $activated, $color=null) {
        if ($this->multipleService->userHasPermission($this->userId, $projectid, 'e')) {
            $result = $this->memberService->editMember($projectid, $memberid, $name, $weight, $activated, $color);
            if ($result) {
                return new DataResponse($result); // TODO
            }
            else {
                return new DataResponse($result, 403);
            }
        }
        else {
            return new DataResponse(
                ['message'=>'Unauthorized action']
                , 403
            );
        }
    }

	/**
	 * @NoAdminRequired
	 * @NoCSRFRequired
	 * @PublicPage
	 * @CORS
	 * @param $projectid
	 * @param $password
	 * @param null $dateMin
	 * @param null $dateMax
	 * @param null $paymentMode
	 * @param null $category
	 * @param null $amountMin
	 * @param null $amountMax
	 * @param string $showDisabled
	 * @param null $currencyId
	 * @return DataResponse
	 * @throws DoesNotExistException
	 * @throws CospendException
	 */
    public function apiGetProjectStatistics($projectid, $password, $dateMin=null, $dateMax=null, $paymentMode=null,
                                            $category=null, $amountMin=null, $amountMax=null, $showDisabled='1', $currencyId=null) {
        if ($this->checkLogin($projectid, $password)) {
            $result = $this->multipleService->getProjectStatistics(
                $projectid, 'lowername', $dateMin, $dateMax, $paymentMode,
                $category, $amountMin, $amountMax, $showDisabled, $currencyId
            );
            return new DataResponse($result);
        }
        else {
            return new DataResponse(
                ['message'=>'Unauthorized action']
                , 401
            );
        }
    }

	/**
	 * @NoAdminRequired
	 * @NoCSRFRequired
	 * @CORS
	 * @param $projectid
	 * @param null $dateMin
	 * @param null $dateMax
	 * @param null $paymentMode
	 * @param null $category
	 * @param null $amountMin
	 * @param null $amountMax
	 * @param string $showDisabled
	 * @param null $currencyId
	 * @return DataResponse
	 * @throws DoesNotExistException
	 * @throws CospendException
	 */
    public function apiPrivGetProjectStatistics($projectid, $dateMin=null, $dateMax=null, $paymentMode=null,
                                            $category=null, $amountMin=null, $amountMax=null, $showDisabled='1', $currencyId=null) {
        if ($this->multipleService->userCanAccessProject($this->userId, $projectid)) {
            $result = $this->multipleService->getProjectStatistics(
                $projectid, 'lowername', $dateMin, $dateMax, $paymentMode,
                $category, $amountMin, $amountMax, $showDisabled, $currencyId
            );
            return new DataResponse($result);
        }
        else {
            return new DataResponse(
                ['message'=>'Unauthorized action']
                , 403
            );
        }
    }

	/**
	 * @NoAdminRequired
	 * @NoCSRFRequired
	 * @PublicPage
	 * @CORS
	 * @param $projectid
	 * @param $password
	 * @return DataResponse
	 * @throws CospendException
	 */
    public function apiGetProjectSettlement($projectid, $password) {
        if ($this->checkLogin($projectid, $password)) {
            $result = $this->multipleService->getProjectSettlement($projectid);
            return new DataResponse($result);
        }
        else {
            return new DataResponse(
                ['message'=>'Unauthorized action']
                , 401
            );
        }
    }

	/**
	 * @NoAdminRequired
	 * @NoCSRFRequired
	 * @CORS
	 * @param $projectid
	 * @return DataResponse
	 * @throws CospendException
	 */
    public function apiPrivGetProjectSettlement($projectid) {
        if ($this->multipleService->userCanAccessProject($this->userId, $projectid)) {
            $result = $this->multipleService->getProjectSettlement($projectid);
            return new DataResponse($result);
        }
        else {
            return new DataResponse(
                ['message'=>'Unauthorized action']
                , 403
            );
        }
    }

    /**
     * @NoAdminRequired
     * @NoCSRFRequired
     * @PublicPage
     * @CORS
     * @param $projectid
     * @param $password
     * @return DataResponse
     * @throws Exception
     */
    public function apiAutoSettlement($projectid, $password) {
        if ($this->checkLogin($projectid, $password) and $this->multipleService->guestHasPermission($projectid, 'c')) {
            $result = $this->multipleService->autoSettlement($projectid);
            if ($result === 'OK') {
                return new DataResponse($result);
            }
            else {
                return new DataResponse($result, 403);
            }
        }
        else {
            return new DataResponse(
                ['message'=>'Unauthorized action']
                , 401
            );
        }
    }

    /**
     * @NoAdminRequired
     * @NoCSRFRequired
     * @CORS
     * @param $projectid
     * @return DataResponse
     * @throws Exception
     */
    public function apiPrivAutoSettlement($projectid) {
        if ($this->multipleService->userHasPermission($this->userId, $projectid, 'c')) {
            $result = $this->multipleService->autoSettlement($projectid);
            if ($result === 'OK') {
                return new DataResponse($result);
            }
            else {
                return new DataResponse($result, 403);
            }
        }
        else {
            return new DataResponse(
                ['message'=>'Unauthorized action']
                , 403
            );
        }
    }

    /**
     * @NoAdminRequired
     * @noinspection PhpFullyQualifiedNameUsageInspection
     * @noinspection PhpUndefinedClassInspection
     * @noinspection PhpUndefinedMethodInspection
     * @noinspection PhpUndefinedNamespaceInspection
     */
    public function getUserList() {
        $userNames = [];
        foreach($this->userManager->search('') as $u) {
            if ($u->getUID() !== $this->userId && $u->isEnabled()) {
                $userNames[$u->getUID()] = $u->getDisplayName();
            }
        }
        $groupNames = [];
        foreach($this->groupManager->search('') as $g) {
            $groupNames[$g->getGID()] = $g->getDisplayName();
        }
        // circles
        $circleNames = [];
        $circlesEnabled = OC::$server->getAppManager()->isEnabledForUser('circles');
        if ($circlesEnabled) {
            $cs = \OCA\Circles\Api\v1\Circles::listCircles(\OCA\Circles\Model\Circle::CIRCLES_ALL, '', 0);
            foreach ($cs as $c) {
                $circleUniqueId = $c->getUniqueId();
                $circleName = $c->getName();
                if ($c->getOwner()->getUserId() === $this->userId) {
                    $circleNames[$circleUniqueId] = $circleName;
                    continue;
                }
                $circleDetails = \OCA\Circles\Api\v1\Circles::detailsCircle($c->getUniqueId());
                foreach ($circleDetails->getMembers() as $m) {
                    if ($m->getUserId() === $this->userId) {
                        $circleNames[$circleUniqueId] = $circleName;
                        break;
                    }
                }
            }
        }
        $response = new DataResponse(
            [
                'users'=>$userNames,
                'groups'=>$groupNames,
                'circles'=>$circleNames
            ]
        );
        $csp = new ContentSecurityPolicy();
        $csp->addAllowedImageDomain('*')
            ->addAllowedMediaDomain('*')
            ->addAllowedConnectDomain('*');
        $response->setContentSecurityPolicy($csp);
        return $response;
    }

	/**
	 * @NoAdminRequired
	 * @param $projectid
	 * @param $shid
	 * @param $permissions
	 * @return DataResponse
	 * @throws CospendException
	 */
    public function editSharePermissions($projectid, $shid, $permissions) {
        if ($this->multipleService->userHasPermission($this->userId, $projectid, 'e')) {
            $result = $this->multipleService->editSharePermissions($projectid, $shid, $permissions);
            if ($result === 'OK') {
                return new DataResponse($result);
            }
            else {
                return new DataResponse($result, 400);
            }
        }
        else {
            return new DataResponse(
                ['message'=>'You are not allowed to edit this project']
                , 403
            );
        }
    }

	/**
	 * @NoAdminRequired
	 * @param $projectid
	 * @param $permissions
	 * @return DataResponse
	 * @throws CospendException
	 */
    public function editGuestPermissions($projectid, $permissions) {
        if ($this->multipleService->userHasPermission($this->userId, $projectid, 'e')) {
            $result = $this->multipleService->editGuestPermissions($projectid, $permissions);
            if ($result === 'OK') {
                return new DataResponse($result);
            }
            else {
                return new DataResponse($result, 400);
            }
        }
        else {
            return new DataResponse(
                ['message'=>'You are not allowed to edit this project']
                , 403
            );
        }
    }

    /**
     * @NoAdminRequired
     * @param $projectid
     * @param $name
     * @param $icon
     * @param $color
     * @return DataResponse
     * @throws CospendException
     */
    public function addCategory($projectid, $name, $icon, $color) {
        if ($this->multipleService->userHasPermission($this->userId, $projectid, 'e')) {
            $result = $this->categoryService->add($projectid, $name, $icon, $color);
            if ($result !== null) {
                return new DataResponse($result);
            }
            else {
                return new DataResponse($result, 400);
            }
        }
        else {
            return new DataResponse(
                ['message'=>'You are not allowed to edit this project']
                , 403
            );
        }
    }

    /**
     * @NoAdminRequired
     * @NoCSRFRequired
     * @PublicPage
     * @CORS
     * @param $projectid
     * @param $password
     * @param $name
     * @param $icon
     * @param $color
     * @return DataResponse
     * @throws CospendException
     */
    public function apiAddCategory($projectid, $password, $name, $icon, $color) {
        if ($this->checkLogin($projectid, $password) and $this->multipleService->guestHasPermission($projectid, 'e')) {
            $result = $this->categoryService->add($projectid, $name, $icon, $color);
            if ($result !== null) {
                // inserted category id
                return new DataResponse($result);
            }
            else {
                return new DataResponse($result, 400);
            }
        }
        else {
            return new DataResponse(
                ['message'=>'Unauthorized action']
                , 401
            );
        }
    }

    /**
     * @NoAdminRequired
     * @NoCSRFRequired
     * @CORS
     * @param $projectid
     * @param $name
     * @param $icon
     * @param $color
     * @return DataResponse
     * @throws CospendException
     */
    public function apiPrivAddCategory($projectid, $name, $icon, $color) {
        if ($this->multipleService->userHasPermission($this->userId, $projectid, 'e')) {
            $result = $this->categoryService->add($projectid, $name, $icon, $color);
            if ($result !== null) {
                // inserted category id
                return new DataResponse($result);
            }
            else {
                return new DataResponse($result, 400);
            }
        }
        else {
            return new DataResponse(
                ['message'=>'Unauthorized action']
                , 403
            );
        }
    }

    /**
     * @NoAdminRequired
     * @param $projectid
     * @param $categoryid
     * @param $name
     * @param $icon
     * @param $color
     * @return DataResponse
     * @throws CospendException
     */
    public function editCategory($projectid, $categoryid, $name, $icon, $color) {
        if ($this->multipleService->userHasPermission($this->userId, $projectid, 'e')) {
            $result = $this->categoryService->update($projectid, $categoryid, $name, $icon, $color);
            if ($result) {
                return new DataResponse($result);
            }
            else {
                return new DataResponse($result, 400);
            }
        }
        else {
            return new DataResponse(
                ['message'=>'You are not allowed to edit this project']
                , 403
            );
        }
    }

    /**
     * @NoAdminRequired
     * @NoCSRFRequired
     * @PublicPage
     * @CORS
     * @param $projectid
     * @param $password
     * @param $categoryid
     * @param $name
     * @param $icon
     * @param $color
     * @return DataResponse
     * @throws CospendException
     */
    public function apiEditCategory($projectid, $password, $categoryid, $name, $icon, $color) {
        if ($this->checkLogin($projectid, $password) and $this->multipleService->guestHasPermission($projectid, 'e')) {
            $result = $this->categoryService->update($projectid, $categoryid, $name, $icon, $color);
            if ($result) {
                return new DataResponse($result);
            }
            else {
                return new DataResponse($result, 403);
            }
        }
        else {
            return new DataResponse(
                ['message'=>'Unauthorized action']
                , 401
            );
        }
    }

    /**
     * @NoAdminRequired
     * @NoCSRFRequired
     * @CORS
     * @param $projectid
     * @param $categoryid
     * @param $name
     * @param $icon
     * @param $color
     * @return DataResponse
     * @throws CospendException
     */
    public function apiPrivEditCategory($projectid, $categoryid, $name, $icon, $color) {
        if ($this->multipleService->userHasPermission($this->userId, $projectid, 'e')) {
            $result = $this->categoryService->update($projectid, $categoryid, $name, $icon, $color);
            if ($result) {
                return new DataResponse($result);
            }
            else {
                return new DataResponse($result, 403);
            }
        }
        else {
            return new DataResponse(
                ['message'=>'Unauthorized action']
                , 403
            );
        }
    }

    /**
     * @NoAdminRequired
     * @param $projectid
     * @param $categoryid
     * @return DataResponse
     * @throws CospendException
     */
    public function deleteCategory($projectid, $categoryid) {
        if ($this->multipleService->userHasPermission($this->userId, $projectid, 'e')) {
            $result = $this->categoryService->deleteById($projectid, $categoryid);
            if ($result) {
                return new DataResponse('OK');
            }
            else {
                return new DataResponse($result, 400);
            }
        }
        else {
            return new DataResponse(
                ['message'=>'You are not allowed to edit this project']
                , 403
            );
        }
    }

    /**
     * @NoAdminRequired
     * @NoCSRFRequired
     * @PublicPage
     * @CORS
     * @param $projectid
     * @param $password
     * @param $categoryid
     * @return DataResponse
     * @throws CospendException
     */
    public function apiDeleteCategory($projectid, $password, $categoryid) {
        if ($this->checkLogin($projectid, $password) and $this->multipleService->guestHasPermission($projectid, 'e')) {
            $result = $this->categoryService->deleteById($projectid, $categoryid);
            if ($result) {
                return new DataResponse('OK');
            }
            else {
                return new DataResponse($result, 400);
            }
        }
        else {
            return new DataResponse(
                ['message'=>'Unauthorized action']
                , 401
            );
        }
    }

    /**
     * @NoAdminRequired
     * @NoCSRFRequired
     * @CORS
     * @param $projectid
     * @param $categoryid
     * @return DataResponse
     * @throws CospendException
     */
    public function apiPrivDeleteCategory($projectid, $categoryid) {
        if ($this->multipleService->userHasPermission($this->userId, $projectid, 'e')) {
            $result = $this->categoryService->deleteById($projectid, $categoryid);
            if ($result) {
                return new DataResponse('OK');
            }
            else {
                return new DataResponse($result, 400);
            }
        }
        else {
            return new DataResponse(
                ['message'=>'Unauthorized action']
                , 403
            );
        }
    }

    /**
     * @NoAdminRequired
     * @param $projectid
     * @param $name
     * @param $rate
     * @return DataResponse
     * @throws CospendException
     */
    public function addCurrency($projectid, $name, $rate) {
        if ($this->multipleService->userHasPermission($this->userId, $projectid, 'e')) {
            $result = $this->currencyService->add($projectid, $name, $rate);
            if ($result !== null) {
                return new DataResponse($result);
            }
            else {
                return new DataResponse($result, 400);
            }
        }
        else {
            return new DataResponse(
                ['message'=>'You are not allowed to edit this project']
                , 403
            );
        }
    }

    /**
     * @NoAdminRequired
     * @NoCSRFRequired
     * @PublicPage
     * @CORS
     * @param string $projectid
     * @param string $password
     * @param string $name
     * @param float $exchangeRate
     * @return DataResponse
     * @throws CospendException
     */
    public function apiAddCurrency($projectid, $password, $name, $exchangeRate) {
        if ($this->checkLogin($projectid, $password) and $this->multipleService->guestHasPermission($projectid, 'e')) {
            $result = $this->currencyService->add($projectid, $name, $exchangeRate);
            if ($result !== null) {
                // inserted currency id
                return new DataResponse($result);
            }
            else {
                return new DataResponse($result, 400);
            }
        }
        else {
            return new DataResponse(
                ['message'=>'Unauthorized action']
                , 401
            );
        }
    }

    /**
     * @NoAdminRequired
     * @NoCSRFRequired
     * @CORS
     * @param $projectid
     * @param $name
     * @param $rate
     * @return DataResponse
     * @throws CospendException
     */
    public function apiPrivAddCurrency($projectid, $name, $rate) {
        if ($this->multipleService->userHasPermission($this->userId, $projectid, 'e')) {
            $result = $this->currencyService->add($projectid, $name, $rate);
            if ($result !== null) {
                // inserted bill id
                return new DataResponse($result);
            }
            else {
                return new DataResponse($result, 400);
            }
        }
        else {
            return new DataResponse(
                ['message'=>'Unauthorized action']
                , 403
            );
        }
    }

    /**
     * @NoAdminRequired
     * @param $projectid
     * @param $currencyid
     * @param $name
     * @param $rate
     * @return DataResponse
     * @throws CospendException
     */
    public function editCurrency($projectid, $currencyid, $name, $rate) {
        if ($this->multipleService->userHasPermission($this->userId, $projectid, 'e')) {
            $result = $this->currencyService->update($projectid, $currencyid, $name, $rate);
            if (is_array($result)) {
                return new DataResponse($result);
            }
            else {
                return new DataResponse($result, 400);
            }
        }
        else {
            return new DataResponse(
                ['message'=>'You are not allowed to edit this project']
                , 403
            );
        }
    }

    /**
     * @NoAdminRequired
     * @NoCSRFRequired
     * @PublicPage
     * @CORS
     * @param $projectid
     * @param $password
     * @param $currencyid
     * @param $name
     * @param $rate
     * @return DataResponse
     * @throws CospendException
     */
    public function apiEditCurrency($projectid, $password, $currencyid, $name, $rate) {
        if ($this->checkLogin($projectid, $password) and $this->multipleService->guestHasPermission($projectid, 'e')) {
            $result = $this->currencyService->update($projectid, $currencyid, $name, $rate);
            if (is_array($result)) {
                return new DataResponse($result);
            }
            else {
                return new DataResponse($result, 403);
            }
        }
        else {
            return new DataResponse(
                ['message'=>'Unauthorized action']
                , 401
            );
        }
    }

    /**
     * @NoAdminRequired
     * @NoCSRFRequired
     * @CORS
     * @param $projectid
     * @param $currencyid
     * @param $name
     * @param $rate
     * @return DataResponse
     * @throws CospendException
     */
    public function apiPrivEditCurrency($projectid, $currencyid, $name, $rate) {
        if ($this->multipleService->userHasPermission($this->userId, $projectid, 'e')) {
            $result = $this->currencyService->update($projectid, $currencyid, $name, $rate);
            if (is_array($result)) {
                return new DataResponse($result);
            }
            else {
                return new DataResponse($result, 403);
            }
        }
        else {
            return new DataResponse(
                ['message'=>'Unauthorized action']
                , 403
            );
        }
    }

    /**
     * @NoAdminRequired
     * @param $projectid
     * @param $currencyid
     * @return DataResponse
     * @throws CospendException
     */
    public function deleteCurrency($projectid, $currencyid) {
        if ($this->multipleService->userHasPermission($this->userId, $projectid, 'e')) {
            $result = $this->currencyService->deleteById($projectid, $currencyid);
            if ($result) {
                return new DataResponse('OK');
            }
            else {
                return new DataResponse($result, 400);
            }
        }
        else {
            return new DataResponse(
                ['message'=>'You are not allowed to edit this project']
                , 403
            );
        }
    }

    /**
     * @NoAdminRequired
     * @NoCSRFRequired
     * @PublicPage
     * @CORS
     * @param $projectid
     * @param $password
     * @param $currencyid
     * @return DataResponse
     * @throws CospendException
     */
    public function apiDeleteCurrency($projectid, $password, $currencyid) {
        if ($this->checkLogin($projectid, $password) and $this->multipleService->guestHasPermission($projectid, 'e')) {
            $result = $this->currencyService->deleteById($projectid, $currencyid);
            if ($result) {
                return new DataResponse('OK');
            }
            else {
                return new DataResponse($result, 400);
            }
        }
        else {
            return new DataResponse(
                ['message'=>'Unauthorized action']
                , 401
            );
        }
    }

    /**
     * @NoAdminRequired
     * @NoCSRFRequired
     * @CORS
     * @param $projectid
     * @param $currencyid
     * @return DataResponse
     * @throws CospendException
     */
    public function apiPrivDeleteCurrency($projectid, $currencyid) {
        if ($this->multipleService->userHasPermission($this->userId, $projectid, 'e')) {
            $result = $this->currencyService->deleteById($projectid, $currencyid);
            if ($result) {
                return new DataResponse('OK');
            }
            else {
                return new DataResponse($result, 400);
            }
        }
        else {
            return new DataResponse(
                ['message'=>'Unauthorized action']
                , 403
            );
        }
    }

	/**
	 * @NoAdminRequired
	 * @param $projectid
	 * @param $userid
	 * @return DataResponse
	 * @throws Exception
	 */
    public function addUserShare($projectid, $userid) {
        if ($this->multipleService->userHasPermission($this->userId, $projectid, 'e')) {
            $result = $this->multipleService->addUserShare($projectid, $userid, $this->userId);
            if (is_numeric($result)) {
                return new DataResponse($result);
            }
            else {
                return new DataResponse($result, 400);
            }
        }
        else {
            return new DataResponse(
                ['message'=>'You are not allowed to edit this project']
                , 403
            );
        }
    }

    /**
     * @NoAdminRequired
     * @param $projectid
     * @param $shid
     * @return DataResponse
     * @throws Exception
     */
    public function deleteUserShare($projectid, $shid) {
        if ($this->multipleService->userHasPermission($this->userId, $projectid, 'e')) {
            $result = $this->multipleService->deleteUserShare($projectid, $shid, $this->userId);
            if ($result === 'OK') {
                return new DataResponse($result);
            }
            else {
                return new DataResponse($result, 400);
            }
        }
        else {
            return new DataResponse(
                ['message'=>'You are not allowed to edit this project']
                , 403
            );
        }
    }

	/**
	 * @NoAdminRequired
	 * @param $projectid
	 * @param $groupid
	 * @return DataResponse
	 * @throws CospendException
	 */
    public function addGroupShare($projectid, $groupid) {
        if ($this->multipleService->userHasPermission($this->userId, $projectid, 'e')) {
            $result = $this->multipleService->addGroupShare($projectid, $groupid, $this->userId);
            if (is_numeric($result)) {
                return new DataResponse($result);
            }
            else {
                return new DataResponse($result, 400);
            }
        }
        else {
            return new DataResponse(
                ['message'=>'You are not allowed to edit this project']
                , 403
            );
        }
    }

	/**
	 * @NoAdminRequired
	 * @param $projectid
	 * @param $shid
	 * @return DataResponse
	 * @throws CospendException
	 */
    public function deleteGroupShare($projectid, $shid) {
        if ($this->multipleService->userHasPermission($this->userId, $projectid, 'e')) {
            $result = $this->multipleService->deleteGroupShare($projectid, $shid, $this->userId);
            if ($result === 'OK') {
                return new DataResponse($result);
            }
            else {
                return new DataResponse($result, 400);
            }
        }
        else {
            return new DataResponse(
                ['message'=>'You are not allowed to edit this project']
                , 403
            );
        }
    }

	/**
	 * @NoAdminRequired
	 * @param $projectid
	 * @param $circleid
	 * @return DataResponse
	 * @throws CospendException
	 */
    public function addCircleShare($projectid, $circleid) {
        if ($this->multipleService->userHasPermission($this->userId, $projectid, 'e')) {
            $result = $this->multipleService->addCircleShare($projectid, $circleid, $this->userId);
            if (is_numeric($result)) {
                return new DataResponse($result);
            }
            else {
                return new DataResponse($result, 400);
            }
        }
        else {
            return new DataResponse(
                ['message'=>'You are not allowed to edit this project']
                , 403
            );
        }
    }

	/**
	 * @NoAdminRequired
	 * @param $projectid
	 * @param $shid
	 * @return DataResponse
	 * @throws CospendException
	 */
    public function deleteCircleShare($projectid, $shid) {
        if ($this->multipleService->userHasPermission($this->userId, $projectid, 'e')) {
            $result = $this->multipleService->deleteCircleShare($projectid, $shid, $this->userId);
            if ($result === 'OK') {
                return new DataResponse($result);
            }
            else {
                return new DataResponse($result, 400);
            }
        }
        else {
            return new DataResponse(
                ['message'=>'You are not allowed to edit this project']
                , 403
            );
        }
    }

    /**
     * @NoAdminRequired
     * @param $path
     * @return DataResponse
     * @throws InvalidPathException
     * @throws NotFoundException
     * @throws Exception
     */
    public function getPublicFileShare($path) {
        $cleanPath = str_replace(array('../', '..\\'), '',  $path);
        $userFolder = OC::$server->getUserFolder();
        if ($userFolder->nodeExists($cleanPath)) {
            $file = $userFolder->get($cleanPath);
            if ($file->getType() === FileInfo::TYPE_FILE) {
                if ($file->isShareable()) {
                    $shares = $this->shareManager->getSharesBy($this->userId,
                        Share::SHARE_TYPE_LINK, $file, false, 1, 0);
                    $token = "";
                    if (count($shares) > 0){
                        foreach($shares as $share){
                            if ($share->getPassword() === null){
                                $token = $share->getToken();
                                break;
                            }
                        }
                    }
                    else {
                        $share = $this->shareManager->newShare();
                        $share->setNode($file);
                        $share->setPermissions(Constants::PERMISSION_READ);
                        $share->setShareType(Share::SHARE_TYPE_LINK);
                        $share->setSharedBy($this->userId);
                        $share = $this->shareManager->createShare($share);
                        $token = $share->getToken();
                    }
                    $response = new DataResponse(['token'=>$token]);
                }
                else {
                    $response = new DataResponse(['message'=>$this->trans->t('Access denied')], 403);
                }
            }
            else {
                $response = new DataResponse(['message'=>$this->trans->t('Access denied')], 403);
            }
        }
        else {
            $response = new DataResponse(['message'=>$this->trans->t('Access denied')], 403);
        }
        return $response;
    }

	/**
	 * @NoAdminRequired
	 * @param $projectid
	 * @return DataResponse
	 * @throws InvalidPathException
	 * @throws NotFoundException
	 * @throws NotPermittedException
	 * @throws CospendException
	 */
    public function exportCsvSettlement($projectid) {
        if ($this->multipleService->userCanAccessProject($this->userId, $projectid)) {
            $result = $this->multipleService->exportCsvSettlement($projectid, $this->userId);
            if (is_array($result) and array_key_exists('path', $result)) {
                return new DataResponse($result);
            }
            else {
                return new DataResponse($result, 400);
            }
        }
        else {
            return new DataResponse(
                ['message'=>'You are not allowed to export this project settlement']
                , 403
            );
        }
    }

	/**
	 * @NoAdminRequired
	 * @param $projectid
	 * @param null $dateMin
	 * @param null $dateMax
	 * @param null $paymentMode
	 * @param null $category
	 * @param null $amountMin
	 * @param null $amountMax
	 * @param string $showDisabled
	 * @param null $currencyId
	 * @return DataResponse
	 * @throws DoesNotExistException
	 * @throws InvalidPathException
	 * @throws LockedException
	 * @throws NotFoundException
	 * @throws NotPermittedException
	 * @throws CospendException
	 */
    public function exportCsvStatistics($projectid, $dateMin=null, $dateMax=null, $paymentMode=null, $category=null,
                                        $amountMin=null, $amountMax=null, $showDisabled='1', $currencyId=null) {
        if ($this->multipleService->userCanAccessProject($this->userId, $projectid)) {
            $result = $this->multipleService->exportCsvStatistics($projectid, $this->userId, $dateMin, $dateMax,
                                                                 $paymentMode, $category, $amountMin, $amountMax,
                                                                 $showDisabled, $currencyId);
            if (is_array($result) and array_key_exists('path', $result)) {
                return new DataResponse($result);
            }
            else {
                return new DataResponse($result, 400);
            }
        }
        else {
            return new DataResponse(
                ['message'=>'You are not allowed to export this project statistics']
                , 403
            );
        }
    }

	/**
	 * @NoAdminRequired
	 * @param $projectid
	 * @param null $name
	 * @param null $uid
	 * @return DataResponse
	 * @throws InvalidPathException
	 * @throws LockedException
	 * @throws NotFoundException
	 * @throws NotPermittedException
	 * @throws CospendException
	 */
    public function exportCsvProject($projectid, $name=null, $uid=null) {
        $userId = $uid;
        if ($this->userId) {
            $userId = $this->userId;
        }

        if ($this->multipleService->userCanAccessProject($userId, $projectid)) {
            $result = $this->multipleService->exportCsvProject($projectid, $name, $userId);
            if (is_array($result) and array_key_exists('path', $result)) {
                return new DataResponse($result);
            }
            else {
                return new DataResponse($result, 400);
            }
        }
        else {
            return new DataResponse(
                ['message'=>'You are not allowed to export this project']
                , 403
            );
        }
    }

    /**
     * @NoAdminRequired
     * @param $path
     * @return DataResponse
     * @throws NotFoundException
     */
    public function importCsvProject($path) {
        $result = $this->multipleService->importCsvProject($path, $this->userId);
        if (!is_array($result) and is_string($result)) {
            return new DataResponse($result);
        }
        else {
            return new DataResponse($result, 400);
        }
    }

    /**
     * @NoAdminRequired
     * @param $path
     * @return DataResponse
     * @throws NotFoundException
     */
    public function importSWProject($path) {
        $result = $this->multipleService->importSWProject($path, $this->userId);
        if (!is_array($result) and is_string($result)) {
            return new DataResponse($result);
        }
        else {
            return new DataResponse($result, 400);
        }
    }

    /**
     * Used by MoneyBuster to check if weblogin is valid
     * @NoAdminRequired
     * @NoCSRFRequired
     */
    public function apiPing() {
        $response = new DataResponse(
            [$this->userId]
        );
        $csp = new ContentSecurityPolicy();
        $csp->addAllowedImageDomain('*')
            ->addAllowedMediaDomain('*')
            ->addAllowedConnectDomain('*');
        $response->setContentSecurityPolicy($csp);
        return $response;
    }

}
